Quick Start
===========

In order to perform one analysis, you will have to run the ``main.py`` file located in the scripts folder.

.. code-block:: console

    python -m main.py

which will run the following piece of code

.. code-block:: python

    rawdata_path = Path(r'raw/M8/20210831')
    analysis_path = Path(r'analysis/M8/20210831')
    analyze_one_file(rawdata_path, analysis_path)

The entire workflow of the analysis is located in the ``analyze_on_file`` function.
You can have a look at this code if you open the **source** link bellow

.. autofunction:: main.analyze_one_file
