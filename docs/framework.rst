Framework / Environement
========================

The python scripts used to run the analyses is located in the ``/scripts folder``
where you will find multiples python files such as

+ main.py
+ file_tools.py
+ opencv_tools.py
+ bonsai_tools.py
+ plot_tools.py
+ behavior_analysis_tools.py
+ ephys_tools.py
+ sanity_checks.py

Most of the function use the ``params`` variable to store all their results.
So basically their is only one variable that you need to manipulate. This variable is a dictionnary,
so you can reach each field easly with this type of syntax ``params['ephys']['ADC1_pulses']['onsets']``

.. image:: params_mindmap.png
  :width: 1200
  :alt: minmap presentation of the params dictionnary
