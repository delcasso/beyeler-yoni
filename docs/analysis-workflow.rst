Analysis Workflow
=================

find_all_paths
--------------
smart way to extract paths and info on the recording session based on a well maintained organisation of the folders

.. autofunction:: file_tools.find_all_paths
    :noindex:

get_video_info
--------------
extract the framerate, the dimensions and the first frame of the video

.. autofunction:: opencv_tools.get_video_info
    :noindex:

import_bonsai_file
------------------
import video transcking data

.. autofunction:: bonsai_tools.import_bonsai_file
    :noindex:

get_video_frames_onsets
-----------------------
load from the ephys file the timestamps of the pulses send by the camera at each frame

.. autofunction:: ephys_tools.get_video_frames_onsets
    :noindex:

video_tracking_sanity_check
---------------------------
check the consistency between the videotracking from bonsai and the pulses in the ephys recording

.. autofunction:: sanity_checks.video_tracking_sanity_check
    :noindex:

behavioral_analysis
-------------------
analysis the behavior of the animal based on predifined zone in the apparatus

.. autofunction:: behavioral_analysis_tools.behavioral_analysis
    :noindex:

load_spikes
-----------
load the resutls from tridesclous

.. autofunction:: ephys_tools.load_spikes
    :noindex:

synchronise_streams
-------------------
synchronize the spike timestamps and the bonsai data

.. autofunction:: ephys_tools.synchronise_streams
    :noindex:

plot_raw_trajectory
-------------------
plot the animal trajectory

.. autofunction:: plot_tools.plot_raw_trajectory
    :noindex:

plot_cell_summary
-----------------
plot a summary fro each recorded cells

.. autofunction:: plot_tools.plot_cell_summary
    :noindex: