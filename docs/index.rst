Python Scripts For Ephys Analysis
===================================================

**The easiest way to start is to look at the quickstart section bellow.**

.. toctree::
    :maxdepth: 2

    quickstart
    framework
    analysis-workflow
    main-function


#.. autofunction:: behavior_analysis_tools.stats_in_zones

#.. literalinclude:: bonsai_tools.py
#    :pyobject: import_bonsai_file

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`