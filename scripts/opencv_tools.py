import copy
import cv2 as cv
import numpy as np
from matplotlib.path import Path
from PIL import Image as Image_Pillow
from roipoly import MultiRoi
import matplotlib.pyplot as plt


def onclick(event, x, y, flag, image):
    """
    Get the pixel coordinate of mouse click when user click on cv window
    """
    global coords
    global new_click_evt
    if event == cv.EVENT_LBUTTONUP:
        new_click_evt = True
        coords.append((x, y))
        # wait for Esc or q key and then exit
        return


def find_apparatus_center(params):
    """
    find the center of the apparatus based on given coordinates
    :param params: the main structure that hold everything and now has at new field params['apparatus']['center']
    :return: params
    """
    coords = params['apparatus']['coords']
    x = []
    y = []
    for c in coords:
        x.append(c[0])
        y.append(c[1])
    c2 = (np.mean(x), np.mean(y))
    params['apparatus']['center'] = c2
    return params


def get_rawvideo_info(params):
    """
    find the parameters of the recorded video using opencv
    :param params: the main structure that hold everything and now has at new field params['video']
    :return: params
    """
    vid_cap = cv.VideoCapture(params['paths']['video'].as_posix())
    # Get the frame rate, height and width of a video file
    params['video'] = {}
    params['video']['fr'] = vid_cap.get(cv.CAP_PROP_FPS)
    params['video']['height'] = vid_cap.get(cv.CAP_PROP_FRAME_HEIGHT)
    params['video']['width'] = vid_cap.get(cv.CAP_PROP_FRAME_WIDTH)
    success, params['video']['frame0'] = vid_cap.read()
    if not success:
        raise IOError('Video Empty')
    params['paths']['frame0'] = params['paths']['analysis'] / f'{params["paths"]["prefix"]}_frame0.png'
    im = Image_Pillow.fromarray(params['video']['frame0'])
    im.save(params['paths']['frame0'])

    return params


def get_apparatus_coordinates(params):
    """
    find the parameters of the recorded video using opencv
    :param params: the main structure that hold everything and now has at new field params['apparatus']
    :return: params
    """
    params['apparatus'] = {}
    params['paths']['userlandmarks'] = params['paths']['analysis'] / f'{params["paths"]["prefix"]}_userlandmarks.npz'
    if not params['paths']['userlandmarks'].exists():
        get_landmarks(params)

    npzfile = np.load(params['paths']['userlandmarks'])
    params['apparatus']['coords'] = npzfile['arr_0']
    params['apparatus']['center'] = npzfile['arr_1']
    params['apparatus']['path'] = create_path(params['apparatus']['coords'])
    coords = params['apparatus']['coords']
    apparatus_perimeter = 4 * 60
    px_by_cm = np.sum(np.sqrt(np.sum(np.diff(np.vstack((coords, coords[0, :])), axis=0)**2, axis=1))) / apparatus_perimeter
    params['video']['px_by_cm'] = px_by_cm

    return params



# def get_landmarks(params):
#     """
#     find the parameters of the recorded video using opencv
#     :param params: the main structure that hold everything and now has at new field params['apparatus']
#     :return: params
#     """
#     image0 = params['video']['frame0']
#     image = copy.deepcopy(image0)
#     global coords
#     coords = []
#     global new_click_evt
#     new_click_evt = False
#     cv.namedWindow('window1', cv.WINDOW_NORMAL)
#     cv.setMouseCallback('window1', onclick, image0)
#     image = cv.putText(image, 'please select the four corners', (50, 50), 0, 1, 50)
#     cv.imshow('window1', image)
#     # wait for Esc or q key and then exit
#
#     while True:
#
#         if new_click_evt:
#             new_click_evt = False
#             image = copy.deepcopy(image0)
#             cv.destroyAllWindows()
#             cv.namedWindow('window1', cv.WINDOW_NORMAL)
#             cv.imshow('window1', image)
#             cv.setMouseCallback('window1', onclick, image)
#             image = cv.putText(image, 'please select the four corners', (50, 50), 0, 1, 50)
#             for c in coords:
#                 image = cv.circle(image, (c[0], c[1]), radius=10, color=(0, 0, 255), thickness=10)
#             if len(coords) > 1:
#                 params['apparatus']['coords'] = coords
#                 params = find_apparatus_center(params)
#                 c2 = params['apparatus']['center']
#                 print(c2)
#                 image = cv.circle(image, (int(c2[0]), int(c2[1])), radius=10, color=(0, 255, 255),
#                                   thickness=10)
#             cv.imshow('window1', image)
#
#         key = cv.waitKey(25) & 0xFF
#
#         if key == 27 or key == ord("q"):
#             cv.destroyAllWindows()
#             break
#
#     np.savez(params['paths']['userlandmarks'],params['apparatus']['coords'],params['apparatus']['center'])



def get_landmarks(params):
    zones_names  = {'OFT': ['whole apparatus', ],
                   'EPM': ['Open Arm 1', 'Open Arm 2', 'Closed Arm 1', 'Closed Arm 2', 'Center'],
                   'SQ': ['whole apparatus', ]}
    task = params['session']['task']
    plt.subplots()
    plt.imshow(params['video']['frame0'])
    if task in zones_names:
        m_rois = MultiRoi(roi_names=zones_names[task])
        roi_dict = {}
        for name, roi in m_rois.rois.items():
            roi_dict[name]=(roi.x,roi.y)
        np.savez(params['paths']['userlandmarks'], **roi_dict)


def user_define_zones(params):

    params['paths']['userlandmarks'] = params['paths']['analysis'] / f'{params["paths"]["prefix"]}_userlandmarks.npz'
    if not params['paths']['userlandmarks'].exists():
        get_landmarks(params)

    if params['paths']['userlandmarks'].exists():
        rois = np.load(params['paths']['userlandmarks'])

    task = params['session']['task']

    if task == 'OFT':
        create_OFT_zones(params, rois)
    elif task == 'EPM':
        create_EPM_zones(params, rois)
    elif task == 'SQ':
        create_OFT_zones(params, rois)
    else:
        print(f'task {task} is unknown, check opencv_tools.py/user_define_zones ! ')

    return params


def create_path(coords):
    tmp = np.vstack((coords, coords[0]))
    codes = [Path.MOVETO] + [Path.LINETO] * (tmp.shape[0] - 2) + [Path.CLOSEPOLY]
    return Path(tmp, codes)


def get_apparatus_zones_perc_area(params, factor):
    """
    Only for the OFT now return two squared zone large_CT (50%), small_CT (10%)
    The zones were created as a percentage of the area of the original zone,
    A square defined by the user by clicking on the four corners of the OFT

    :param params: the usual dictionnary with all parameters

    :param factor: the proportion of the central zone

    :return: the updated version of the params dictionnary

    """
    print(params['apparatus']['coords'])
    print(params['apparatus']['center'])

    abs_coord = params['apparatus']['coords']
    center = params['apparatus']['center']
    # centered_coord = abs_coord - center

    tmp = np.vstack((abs_coord, abs_coord[0, :]))
    path_ = np.diff(tmp, axis=0)
    d = []
    for c in path_:
        d.append(np.sqrt(c[0] ** 2 + c[1] ** 2))
    avg_side = np.mean(d)
    area = avg_side ** 2

    new_zone_area = area * factor
    new_side_length = np.sqrt(new_zone_area)
    h = new_side_length / 2.0
    new_centered_coord = np.asarray([[-h, h], [h, h], [h, -h], [-h, -h]])
    new_abs_coord = new_centered_coord + center

    return new_abs_coord


def create_OFT_zones(params, rois):

    for name, coords in rois.items():
      x=coords[0]
      y=coords[1]

    params['apparatus'] = {}
    a = [[x_, y_] for x_, y_ in zip(x, y)]
    params['apparatus']['coords'] = np.asarray(a)
    params = find_apparatus_center(params)
    params['apparatus']['path'] = create_path(params['apparatus']['coords'])
    coords = params['apparatus']['coords']
    apparatus_perimeter = 4 * 60
    px_by_cm = np.sum(np.sqrt(np.sum(np.diff(np.vstack((coords, coords[0, :])), axis=0)**2, axis=1))) / apparatus_perimeter
    params['video']['px_by_cm'] = px_by_cm

    params['apparatus']['zones'] = []
    zones_names = ['small_center', 'large_center', 'external_ring', 'entire_apparatus']
    area_percentage = [10,50,100,100]

    for n,a in zip(zones_names,area_percentage):
        coords_tmp = get_apparatus_zones_perc_area(params, a / 100)
        params['apparatus']['zones'].append({'name':n, 'coords':coords_tmp, 'path': create_path(coords_tmp)})
    return params


def create_EPM_zones(params, rois):
    params['apparatus'] = {}
    params['apparatus']['zones'] = []
    all_xs = []
    all_ys = []
    for name, coords in rois.items():
        x = coords[0]
        y = coords[1]
        all_xs += x.tolist()
        all_ys += y.tolist()
        coords_ = [[x_,y_] for x_,y_ in zip(x,y)]
        params['apparatus']['zones'].append({'name': name, 'coords': coords_, 'path': create_path(coords_)})

    all_xs = np.sort(all_xs)
    all_ys = np.sort(all_ys)
    x_min = all_xs[:2].mean()
    x_max = all_xs[-1:-3:-1].mean()
    y_min = all_ys[:2].mean()
    y_max = all_ys[-1:-3:-1].mean()
    ampl_vert = y_max - y_min
    ampl_hor = x_max - x_min
    ampl_moy_pix = (ampl_vert + ampl_hor) / 2.0
    ampl_moy_cm = 75
    params['video']['px_by_cm'] = ampl_moy_pix / ampl_moy_cm
    return params


def get_video_info(params):
    """
    we extract information from the video file such as frame rate, dimension, first image, and apparatus coordinates
    :param params: the dict structure that stores all the parameters needed for this analysis
    :return: params, un uptade version of the input dict info
    """
    params = get_rawvideo_info(params)
    params = user_define_zones(params)
    return params


