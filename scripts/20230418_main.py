# sphinx-apidoc -o . ../scripts
"""
This is the main file of the program
Here in the analyze_one_file function, we can see entire workflow
"""
import pickle
from pathlib import Path

from behavior_analysis_tools import behavioral_analysis
from bonsai_tools import import_bonsai_file
from ephys_tools import get_video_frames_onsets, load_spikes, synchronise_streams, get_sq_licks_onsets, \
    get_probe_geometry, \
    measure_unit_properties
from file_tools import find_all_paths
from opencv_tools import get_video_info
from output_tools import generate_xlsx_beh_output, generate_xlsx_unit_properties_output, generate_xlsx_behspikes_output
from plot_tools import plot_raw_trajectory, plot_avg_waveform, plot_unit_properties
from sanity_checks import video_tracking_sanity_check
from scripts.behavior_and_ephys_analysis.heatmaps import heatmap_analysis
from scripts.behavior_and_ephys_analysis.psth_analysis import sq_psth_analysis
from scripts.behavior_and_ephys_analysis.zone_analysis import zone_analysis


def analyze_one_file(rawdata_path_, analysis_path_, exp_type_='ephys'):
    """
    This is the main function that is called when one session has to be analyzed

    + find_all_paths : smart way to extract paths and info on the recording session based on a well maintained
    organisation of the folders
    + get_video_info : extract the frame rate, the dimensions and the first frame of the video
    + import_bonsai_file : import video tracking data + get_video_frames_onsets : load from the ephys file the
    timestamps of the pulses send by the camera at each frame
    + video_tracking_sanity_check : check the consistency between the videotracking from bonsai and
    the pulses in the ephys recording
    + behavioral_analysis : analysis the behavior of the animal based on predifined zones in the maze
    + load_spikes : load the output file from tridesclous
    + synchronise_streams : synchronize  spike timestamps and the bonsai data
    + plot_raw_trajectory : plot the animal trajectory
    + plot_cell_summary : plot a summary from each recorded cells

    :param exp_type_:
    :param rawdata_path_: path to the raw data

    :param analysis_path_: path to the folder were we save the results of the analysis

    :return: void

    """
    params = {'exp_type': exp_type_, 'paths': {}}
    params['paths']['rawdata'] = rawdata_path_
    params['paths']['analysis'] = analysis_path_
    params = find_all_paths(params)
    params = get_video_info(params)
    params = import_bonsai_file(params)

    if params['exp_type'] == 'ephys':
        params = get_video_frames_onsets(params)
        video_tracking_sanity_check(params)
        params = load_spikes(params)
        params = synchronise_streams(params)
        params = get_probe_geometry(params)
        plot_avg_waveform(params)
        # params = measure_unit_properties(params)
        # plot_unit_properties(params)

        params['beh_analysis'] = {'bin_size_sec': 3600, 'bin_number': 1, 'periodes': {}}
        params = zone_analysis(params)
        params = behavioral_analysis(params)
        bin_size = 25
        heatmap_analysis(params, bin_size, show=False)

        # if params['session']['task'] == 'EPM':
        #     params = epm_psth_analysis(params, debug_mode=False)

        if params['session']['task'] == 'SQ':
            params = get_sq_licks_onsets(params, total_ch_number=27, ch_ids=[20, 21])
            params = sq_psth_analysis(params, debug_mode=False)

    generate_xlsx_beh_output(params)
    # generate_xlsx_unit_properties_output(params)
    generate_xlsx_behspikes_output(params)

    # choose debug_mode='on' to display debug figure, one marker per animal position over the entire video
    plot_raw_trajectory(params, debug_mode='None')

    if params['exp_type'] == 'behavior_only':
        generate_xlsx_beh_output(params)

    filename = params['paths']['analysis'] / f'{params["paths"]["prefix"]}_data.pickle'
    outfile = open(filename, 'wb')
    pickle.dump(params, outfile)
    outfile.close()

    return params

if __name__ == '__main__':

    rawdata_path = Path(r'D:\ephys\raw\F2491\20230512')
    analysis_path = Path(r'D:\ephys\analysis\F2491\20230512')
    analyze_one_file(rawdata_path, analysis_path)
