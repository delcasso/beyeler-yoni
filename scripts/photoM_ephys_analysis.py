from ephys_tools import get_one_ch_data,analog_2_events, get_multiple_ch_data
import matplotlib.pyplot as plt
import h5py
from pathlib import Path
import numpy as np
from scipy.signal import butter, lfilter
import pandas as pd

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def delete_first_min(photoM_405, photoM_470, t_ephys_dest):
    periode = np.median(np.diff(t_photoM))
    fq = 1/periode
    return photoM_405[60*fq:], photoM_470[60*fq:], t_ephys_dest[60*fq:]

def dff(photoM_405, photoM_470):
    a, b = np.polyfit(photoM_405, photoM_470, 1)
    fit_iso = a*photoM_405+b
    dff = (photoM_470-fit_iso)/fit_iso
    dff*=100.0
    return fit_iso, dff



datafolder = Path(r'Y:\Ephys in vivo\ephys_photoM\M2394\20230216')
ephys_path = datafolder / r'F23_2023-02-16_09-09-36_SignalCheck\Record Node 104\experiment1\recording1\continuous\Rhythm_FPGA-100.0\continuous.dat'
photoM_path = datafolder / 'M2394_OFT_001.mat'
f = h5py.File(photoM_path,'r')
photoM_sig = f.get('sig')
photoM_405 = np.array(photoM_sig[:,0])
photoM_470 = np.array(photoM_sig[:,1])# For converting to a NumPy array
camera_signal = get_one_ch_data(filepath=ephys_path, total_ch_number=27, ch_id=19)
camera_events = analog_2_events(camera_signal)
led_signal = get_one_ch_data(filepath=ephys_path, total_ch_number=27, ch_id=20)
led_events = analog_2_events(led_signal)
t_ephys = np.arange(0,len(camera_signal))/30000
t_photoM = np.arange(0,len(photoM_sig))/20


t_led_start_ephys = t_ephys[led_events['onsets']]
t_led_start_photoM = np.arange(0,t_photoM[-1],5)
t_synchro = t_led_start_ephys
x_ephys_origin = np.arange(0,len(t_led_start_ephys))
x_ephys_dest = np.arange(0,len(t_led_start_ephys),0.01)
t_ephys_dest = np.interp(x_ephys_dest,x_ephys_origin,t_synchro)



spikes_export_filepath = Path(r"Y:\Ephys in vivo\ephys_photoM\M2394\20230216\F23_2023-02-16_09-09-36_SignalCheck\Record Node 104\experiment1\recording1\continuous\Rhythm_FPGA-100.0\tdc_continuous\export\spikes - segNum 0 - chanGrp 0 - cell#all.csv")

df = pd.read_csv(spikes_export_filepath, header=None)

cluster_ids = df[1]
cluster_rec_num = df[0]
cluster_ids_set = set(cluster_ids)




n_clusters = len(cluster_ids_set)

binned_spikes = np.zeros((len(t_ephys_dest)-1,n_clusters))


for id in cluster_ids_set:
    spike_times = df[df[1]==id][0]/30000
    binned_spikes[:,id]=np.histogram(spike_times,bins=t_ephys_dest)[0]
    n_spikes = len(spike_times)
    plt.plot(spike_times,np.ones((n_spikes,1))*(id/n_clusters)*1000+6000,color=[0,0,0,0.1],marker='o')
    plt.plot(t_ephys_dest[0:-1]+1/15000, binned_spikes[:,id]+6000)


fig = plt.figure()
plt.plot(t_ephys_dest,photoM_405, color=[1,0,1])
plt.plot(t_ephys_dest,photoM_470, color=[0,0,1])

plt.plot(t_ephys_dest[0:-1]+1/15000, np.sum(binned_spikes, axis=1)+6000)



# fig,axs = plt.subplots(17,1, sharex=True)
# # axs[0].plot(t_ephys_dest,photoM_405)
# axs[0].plot(t_ephys_dest,photoM_470)
# axs[0].set_title('470 nm')
#
# ephys_signal = get_multiple_ch_data(filepath=ephys_path, total_ch_number=27, id_list=np.arange(0,16))
# ref = np.median(ephys_signal, axis=1)
#
# ephys_signal_ref = ephys_signal.T -ref
# ephys_signal_ref = ephys_signal_ref.T
#
# # Sample rate and desired cutoff frequencies (in Hz).
# fs = 30000.0
# lowcut = 600.0
# highcut = 6000.0
#
# ephys_signal_filtered = np.zeros(ephys_signal.shape)
#
# for i in range(16):
#     ephys_signal_filtered[:,i] = butter_bandpass_filter(ephys_signal_ref[:,i], lowcut, highcut, fs, order=4)
#
# for i in range(16):
#     axs[i+1].plot(t_ephys, ephys_signal_filtered[:, i])
#
# plt.plot(t_ephys[np.int(0*fs):np.int(0.1*fs)],ephys_signal_filtered[np.int(0*fs):np.int(0.1*fs),11])
#
# plt.show()
# plt.savefig(datafolder / 'graph photoM.png')

print('here')