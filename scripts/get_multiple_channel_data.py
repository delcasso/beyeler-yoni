import numpy as np
import matplotlib.pyplot as plt
import spikeinterface.extractors as se
import spikeinterface.preprocessing as sp
# from scalebars import add_scalebar

ephys_path = r'C:\in-vivo-ephys\data\raw\F23\20230407\F23_2023-04-07_09-57-33_EPM\Record Node 101'

i1=30000*200
i2=30000*202
recording = se.read_openephys(ephys_path)
recording_f = sp.bandpass_filter(recording)
recording_f = sp.common_reference(recording_f)
to_plot = recording_f.get_traces(start_frame=i1,end_frame=i2,channel_ids=['CH7','CH8','CH9'])

plt.plot(to_plot[:,0]+2000)
plt.plot(to_plot[:,1]+3000)
plt.plot(to_plot[:,2]+4000)
# plt.plot(to_plot[:,3]+5000)
plt.savefig(r'C:\in-vivo-ephys\data\raw\F23\20230407\graph.svg')
plt.savefig(r'C:\in-vivo-ephys\data\raw\F23\20230407\graph.png')
# fig.savefig(ephys_path/'graph.svg')
plt.ion()
plt.show()
# plt.close()