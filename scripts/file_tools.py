import numpy as np


def add_path(params, prefix, suffix, ext):
    """
    We add path to our params dictionnary
    :param params: the dictionnary before update
    :param prefix: the filename prefix such as 'M8_OFT'
    :param suffix: the filetype such as '_bonsai'
    :param ext: the extension of the filde such as '.csv'
    :return: the dictionnary after update
    """
    rp = params['paths']['rawdata']
    filepath = rp / f'{prefix}_{suffix}.{ext}'
    if not filepath.exists():
        raise FileNotFoundError(f'{filepath} file not found !')
    params['paths'][suffix] = filepath
    return params


def extract_starttime(params):
    """
    extract the time when the experimenter place the mouse in the maze and the exp actually starts
    :param params:
    :return:
    """
    startingtime = float(np.genfromtxt(params['paths']['startingtime'], dtype=float))
    params['session']['startingtime'] = startingtime
    return params


def extract_info_from_paths(params):
    """
    Using a structured storage of the data, we can extract many information about the experiment such
    as mouse name, date, task, ...
    This is what we extract here
    :param params:
    :return:
    """
    rp = params['paths']['rawdata']
    params['mouse'] = {}
    params['mouse']['name'] = rp.parent.name
    params['mouse']['sex'] = rp.parent.name[0]
    params['mouse']['id'] = rp.parent.name[1:]
    params['session'] = {}
    params['session']['date'] = rp.name
    params['session']['task'] = params['paths']['video'].stem.split('_')[-1]
    return params


def find_all_paths(params):
    """
    Using a structured storage of the data, we can extract many information about the experiment such as mouse name, date, task, ...
    We also search for video file, bonsai file, spikesorting results and experiment starting time

    :param params: the params dictionnary before update

    :return: the params dictionnary after update
    """
    rp = params['paths']['rawdata']
    print(f'Searching for data files in {rp}')
    video_path = list(rp.glob('*.avi'))[0]
    params['paths']['video'] = video_path
    params = extract_info_from_paths(params)
    prefix = f'{params["mouse"]["name"]}_{params["session"]["task"]}'
    params['paths']['prefix'] = prefix
    params = add_path(params, prefix, suffix='bonsai', ext='csv')
    params = add_path(params, prefix, suffix='startingtime', ext='txt')
    params = extract_starttime(params)

    if params['exp_type'] == 'ephys':
        params['paths']['ephys_dat'] = list(rp.glob('**/*.dat'))[0]
        print(params['paths']['ephys_dat'])
        params['paths']['tdc'] = params['paths']['ephys_dat'].parent / 'tdc_continuous'
        params = add_path(params, prefix, suffix='spikesorting', ext='csv')
    return params



