from ephys_tools import get_one_ch_data, analog_2_events, get_multiple_ch_data
import matplotlib.pyplot as plt
import h5py
from pathlib import Path
import numpy as np
from scipy.signal import butter, lfilter
import pandas as pd
from scipy.signal import find_peaks
import pickle

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y


def delete_first_min(photoM_405, photoM_470, t_ephys_dest):
    periode = np.median(np.diff(t_ephys_dest))
    fq = 1 / periode
    i1 = int(60 * fq)
    return photoM_405[i1:], photoM_470[i1:], t_ephys_dest[i1:]


def process_photometry_rawdata(rec):
    photoM_405 = rec['photoM']['raw']['405']
    photoM_470 = rec['photoM']['raw']['470']
    t_ephys_dest = rec['photoM']['raw']['ephys_time']
    photoM_405, photoM_470, t_ephys_dest = delete_first_min(photoM_405, photoM_470, t_ephys_dest)
    iso_fitted, dff = process_dff(photoM_405, photoM_470)
    # zscored = zscore_sig(dff)
    rec['photoM']['processed'] = {'405': photoM_405, '470': photoM_470, 'ephys_time': t_ephys_dest, 'dff': dff,
                                  'iso_fitted': iso_fitted}
    return rec


def process_dff(photoM_405, photoM_470):
    a, b = np.polyfit(photoM_405, photoM_470, 1)
    fit_iso = a * photoM_405 + b
    dff = (photoM_470 - fit_iso) / fit_iso
    dff *= 100.0
    return fit_iso, dff


def load_ephys_data(ephys_path, rec, sfreq=30000):
    rec['ephys']['led_signal'] = get_one_ch_data(filepath=ephys_path, total_ch_number=27, ch_id=20)
    rec['ephys']['led_events'] = analog_2_events(rec['ephys']['led_signal'])
    rec['ephys']['t_ephys'] = np.arange(0, len(rec['ephys']['led_signal'])) / sfreq
    return rec


def spikes_binning(spikes_export_filepath, rec):
    df = pd.read_csv(spikes_export_filepath, header=None)
    rec['ephys']['cluster_ids'] = df[1]
    rec['ephys']['cluster_rec_num'] = df[0]
    rec['ephys']['cluster_ids_set'] = set(rec['ephys']['cluster_ids'])

    n_clusters = len(rec['ephys']['cluster_ids_set'])
    binned_spikes = np.zeros((len(rec['photoM']['processed']['ephys_time']) - 1, n_clusters))

    for id, _ in enumerate(rec['ephys']['cluster_ids_set']):
        spike_times = df[df[1] == id][0] / 30000
        binned_spikes[:, id] = \
            np.histogram(spike_times.to_numpy(dtype=object), bins=rec['photoM']['processed']['ephys_time'])[0]
    rec['photoM']['processed']['binned_spikes'] = binned_spikes
    rec['photoM']['processed']['sum_binned_spikes'] = np.sum(rec['photoM']['processed']['binned_spikes'], axis=1)
    return rec


def load_photoM_data(photom_path, rec, sfreq=20):
    f = h5py.File(photom_path, 'r')
    photom_sig = f.get('sig')
    rec['photoM']['raw']['405'] = np.array(photom_sig[:, 0])
    rec['photoM']['raw']['470'] = np.array(photom_sig[:, 1])  # For converting to a NumPy array
    rec['photoM']['raw']['time'] = np.arange(0, len(photom_sig)) / sfreq
    rec['photoM']['raw']['path'] = photom_path
    return rec


def synchronise_photoM_to_ephys(rec):
    t_led_start_ephys = rec['ephys']['t_ephys'][rec['ephys']['led_events']['onsets']]
    t_synchro = t_led_start_ephys
    x_ephys_origin = np.arange(0, len(t_led_start_ephys))
    x_ephys_dest = np.arange(0, len(t_led_start_ephys), 0.01)
    rec['photoM']['raw']['ephys_time'] = np.interp(x_ephys_dest, x_ephys_origin, t_synchro)
    return rec



def find_dff_peaks(rec, prominence=2):
    peaks, properties = find_peaks(rec['photoM']['processed']['dff'], prominence=prominence)
    rec['photoM']['transients'] = {'peaks': peaks, 'properties': properties}
    return rec


def plot_all(rec):
    fig, axs = plt.subplots(4, 1, sharex=True)
    axs[0].plot(rec['photoM']['raw']['ephys_time'], rec['photoM']['raw']['405'], color=[1, 0, 1])
    axs[0].plot(rec['photoM']['raw']['ephys_time'], rec['photoM']['raw']['470'], color=[0, 0, 1])
    axs[1].plot(rec['photoM']['processed']['ephys_time'], rec['photoM']['processed']['405'], color=[1, 0, 1])
    axs[1].plot(rec['photoM']['processed']['ephys_time'], rec['photoM']['processed']['iso_fitted'], color=[1, 0, 1],
                linestyle=':')
    axs[1].plot(rec['photoM']['processed']['ephys_time'], rec['photoM']['processed']['470'], color=[0, 0, 1])
    axs[2].plot(rec['photoM']['processed']['ephys_time'], rec['photoM']['processed']['dff'], color=[0, 1, 0])
    peaks = rec['photoM']['transients']['peaks']
    axs[2].plot(rec['photoM']['processed']['ephys_time'][peaks], rec['photoM']['processed']['dff'][peaks], '+')
    # axs[3].plot(rec['photoM']['processed']['ephys_time'], rec['photoM']['processed']['zscored'], color=[0, 1, 0])
    axs[3].plot(rec['photoM']['processed']['ephys_time'][0:-1] + 1 / 15000,
                rec['photoM']['processed']['sum_binned_spikes'])
    return fig, axs


def zscore_snippets(rec, window_sec):
    n_clusters = rec['photoM']['processed']['binned_spikes'].shape[1]
    m = rec['photoM']['processed']['snippets_spk'][0]
    all_zscores = np.zeros((n_clusters, int(m.shape[1] / 2))) + np.nan
    for i_clu in range(n_clusters):
        m = rec['photoM']['processed']['snippets_spk'][i_clu]
        m2 = np.zeros((m.shape[0], int(m.shape[1] / 2)))
        for i in range(0, m.shape[1] - 1, 2):
            tmp = m[:, i:i + 2]
            s = np.sum(tmp, axis=1)
            m2[:, int(i / 2)] = s
        m = m2
        m_sum = np.sum(m, axis=1)
        m = m[m_sum > 0, :]
        baseline = m  # m[:,0:9]
        mean_ = np.mean(baseline, axis=1)
        std_ = np.std(baseline, axis=1)
        zscore_ = (m.T - mean_).T
        zscore_ = (zscore_.T / std_).T
        all_zscores[i_clu, :] = np.nanmean(zscore_, axis=0)
        rec['photoM']['processed']['zscored_snippets_spk'] = all_zscores

    return rec


def plot_scored_snippets(rec, window_sec):
    all_zscores = rec['photoM']['processed']['zscored_snippets_spk']
    n_clusters = rec['photoM']['processed']['binned_spikes'].shape[1]
    l = [f'{v:.2f}' for v in np.linspace(-window_sec / 2, window_sec / 2, all_zscores.shape[1])]
    fig, axs = plt.subplots(1, 2)
    for i_clu in range(n_clusters - 1):
        axs[1].plot(all_zscores[i_clu, :], color=[0.7, 0.7, 0.7])
    axs[1].plot(all_zscores[n_clusters - 1, :], color=[1, 0, 0])
    axs[1].set_xticks(np.arange(0, all_zscores.shape[1]))
    axs[1].set_xticklabels(l)
    im_handle = axs[0].imshow(all_zscores)
    axs[0].set_xticks(np.arange(0, all_zscores.shape[1]))
    axs[0].set_xticklabels(l)
    plt.colorbar(im_handle)



def refresh(ax, peaks):
    ax.clear()
    ax.plot(rec['photoM']['processed']['ephys_time'], rec['photoM']['processed']['dff'], color=[0, 1, 0])
    ax.plot(rec['photoM']['processed']['ephys_time'][peaks], rec['photoM']['processed']['dff'][peaks], '+')


def extract_snippets(rec, window_sec=1, photoM_sfreq=20):
    peaks = rec['photoM']['transients']['peaks']
    window_samples = window_sec * photoM_sfreq
    window_samples = (window_samples // 2) * 2
    h_window_samples = window_samples // 2

    n_clusters = rec['photoM']['processed']['binned_spikes'].shape[1]
    rec['photoM']['processed']['snippets_spk'] = {}

    rec['photoM']['processed']['binned_spikes'] = np.column_stack(
        (rec['photoM']['processed']['binned_spikes'], rec['photoM']['processed']['sum_binned_spikes']))

    for i_clu in range(n_clusters + 1):
        spk_matrix = np.zeros((len(peaks), window_samples))+np.nan

        binned_spikes = rec['photoM']['processed']['binned_spikes'][:, i_clu]
        i2_max = np.max(binned_spikes.shape)


        tmp2 = np.zeros((len(peaks), window_samples)) + np.nan

        for i, p in enumerate(peaks):
            i1 = p - h_window_samples
            i2 = p + h_window_samples
            if (i1>0) and i2<i2_max:
                tmp = binned_spikes[i1:i2]
                spk_matrix[i, :] = tmp
                tmp2[i,:] = rec['photoM']['processed']['dff'][i1:i2]

        rec['photoM']['processed']['snippets_dff']=tmp2

        rec['photoM']['processed']['snippets_spk'][i_clu] = spk_matrix


    return rec



def plot_multiple_mice(all_recordings, window_sec = 8):

    fig, axs = plt.subplots(1, 3)

    all_zscored_spk = None
    all_dff_snippets = None

    for rec in all_recordings:


        n_clusters = rec['photoM']['processed']['binned_spikes'].shape[1]
        tmp  =rec['photoM']['processed']['zscored_snippets_spk'][:-1,:]
        if all_zscored_spk is None:
            all_zscored_spk = tmp
        else:
            all_zscored_spk = np.vstack((all_zscored_spk,tmp))

        tmp  =rec['photoM']['processed']['snippets_dff']
        if all_dff_snippets is None:
            all_dff_snippets = tmp
        else:
            all_dff_snippets = np.vstack((all_dff_snippets,tmp))


        l = [f'{v:.2f}' for v in np.linspace(-window_sec / 2, window_sec / 2, all_zscored_spk.shape[1])]

    all_zscored_spk_sum = np.sum(all_zscored_spk, axis=1)
    all_zscored_spk = all_zscored_spk[~np.isnan(all_zscored_spk_sum), :]
    im_handle = axs[0].imshow(all_zscored_spk)
    axs[0].set_xticks(np.arange(0, all_zscored_spk.shape[1]))
    axs[0].set_xticklabels(l)
    plt.colorbar(im_handle)

    zscore_spk_mean = np.mean(all_zscored_spk, axis=0)
    zscore_spk_sem = np.std(all_zscored_spk, axis=0)/np.sqrt(all_zscored_spk.shape[0])
    axs[1].plot(zscore_spk_mean, color='k')
    axs[1].plot(zscore_spk_mean + zscore_spk_sem, color='gray')
    axs[1].plot(zscore_spk_mean - zscore_spk_sem, color='gray')

    axs[1].set_xticks(np.arange(0, all_zscored_spk.shape[1]))
    axs[1].set_xticklabels(l)

    zscore_dff_mean = np.nanmean(all_dff_snippets, axis=0)
    zscore_dff_sem = np.nanstd(all_dff_snippets, axis=0)/np.sqrt(all_dff_snippets.shape[0])
    axs[2].plot(zscore_dff_mean, color='k')
    axs[2].plot(zscore_dff_mean + zscore_dff_sem, color='gray')
    axs[2].plot(zscore_dff_mean - zscore_dff_sem, color='gray')


    return all_zscored_spk




def analyse_one_file(datafolder, photoM_sfreq=20, ephys_sampling_rate=30000):


    pickle_path = Path(datafolder / 'data.pickle')

    rec = {
        'ephys':
            {
                'led_signal': [],
                'led_events': [],
                't_ephys': []
            },
        'photoM':
            {
                'raw':
                    {
                        '405': [],
                        '470': [],
                        'time': []
                    }
            }
    }

    ephys_path = [p for p in Path(datafolder).rglob('continuous.dat')][0]
    photom_path = [p for p in Path(datafolder).rglob('*.mat')][0]

    if not pickle_path.exists():

        rec = load_ephys_data(ephys_path, rec, sfreq=ephys_sampling_rate)
        rec = load_photoM_data(photom_path, rec, sfreq=photoM_sfreq)
        rec['metadata'] = {'datafolder': datafolder, 'ephys_path': ephys_path, 'photom_path': photom_path}
        rec = synchronise_photoM_to_ephys(rec)
        rec = process_photometry_rawdata(rec)
        with open(pickle_path, 'wb') as f:
            # Pickle the 'data' dictionary using the highest protocol available.
            pickle.dump(rec, f, pickle.HIGHEST_PROTOCOL)

    with open(pickle_path, 'rb') as f:
        # The protocol version used is detected automatically, so we do not
        # have to specify it.
        rec = pickle.load(f)

    spikes_export_filepath = [p for p in Path(datafolder).rglob('spikes*.csv')][0]
    rec['metadata']['spikes_export_filepath']=spikes_export_filepath
    rec = spikes_binning(spikes_export_filepath, rec)
    rec = find_dff_peaks(rec)
    fig, axs = plot_all(rec)

    window_sec = 8
    rec = extract_snippets(rec, window_sec=window_sec, photoM_sfreq=20)
    rec = zscore_snippets(rec, window_sec=window_sec)
    plot_scored_snippets(rec, window_sec)

    # with open(pickle_path, 'wb') as f:
    #     # Pickle the 'data' dictionary using the highest protocol available.
    #     pickle.dump(rec, f, pickle.HIGHEST_PROTOCOL)

    return rec


if __name__ == '__main__':
    # mice = ['F2477', 'F2478', 'M2474', 'M2475', 'M2476']
    # mice = ['F2477', 'M2475', 'M2476']
    mice = ['M2476']
    all_recordings = []
    for mouse in mice:
        print(f'{mouse}')
        datafolder = Path(r'Y:\Ephys in vivo\ephys_photoM') / f'{mouse}' / '20230401'
        print(datafolder)
        # datafolder = Path(r'Y:\Ephys in vivo\ephys_photoM\F2477\20230401')
        all_recordings.append(analyse_one_file(datafolder, photoM_sfreq=20, ephys_sampling_rate=30000))

    plot_multiple_mice(all_recordings, window_sec=8)
    print('here')
    plt.show()