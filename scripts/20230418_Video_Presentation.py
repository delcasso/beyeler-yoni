import cv2
import numpy as np
import matplotlib.pyplot as plt

path_ = r"C:\Users\ycouderc\Desktop\test_video"
signal = r"C:\Users\ycouderc\Desktop\test_video\M2114.mat"

# Define the function to plot the signal
def plot_signal(signal):
    plt.clf()  # Clear the previous plot
    plt.plot(signal)
    plt.xlabel('Time (s)')
    plt.ylabel('Signal Amplitude')
    plt.ylim(-1, 1)  # Set the y-axis limit
    plt.pause(0.01)  # Pause for a short time to update the plot


# Load the video
cap = cv2.VideoCapture(r"C:\Users\ycouderc\Desktop\test_video\M2114.avi")

# Read the first frame to get the frame size
ret, frame = cap.read()
frame_size = (frame.shape[1], frame.shape[0])

# Define the video writer
out = cv2.VideoWriter(r"C:\Users\ycouderc\Desktop\test_video\outputvideo.avi", cv2.VideoWriter_fourcc(*'XVID'), 20.0, frame_size)

# Start the video loop
while True:
    ret, frame = cap.read()  # Read the next frame from the video
    if not ret:  # If the end of the video is reached, break out of the loop
        break

    # Process the frame here

    # Generate a random signal for demonstration purposes
    signal = np.random.uniform(-1, 1, size=1000)

    # Plot the signal
    plot_signal(signal)

    # Add the signal plot to the video frame
    signal_plot = np.frombuffer(plt.gcf().canvas.tostring_rgb(), dtype=np.uint8)
    signal_plot = signal_plot.reshape(plt.gcf().canvas.get_width_height()[::-1] + (3,))
    frame[100:2100, 100:3300] = signal_plot

    # Write the frame to the output video
    out.write(frame)

    # Display the frame
    cv2.imshow('Frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):  # Press 'q' to quit the video loop
        break

# Release the video capture and writer objects, and close all windows
cap.release()
out.release()
cv2.destroyAllWindows()

# import cv2
# import numpy as np
# import matplotlib.pyplot as plt
# from matplotlib.animation import FuncAnimation
# import imageio
#
# # Set paths
# videoPath = ""
# dataPath = ""
#
# # Load data
# data = np.load(dataPath, allow_pickle=True)
# experiment = data['experiment']
#
# # Read video
# v = cv2.VideoCapture(videoPath)
#
# # Set start and end frames
# st = 120
# videointerval = st + 400
#
# # Extract signal data
# sig = experiment['pData']['mainSig'][st:videointerval - 1]
#
# # Set up subplots
# fig, (ax1, ax2) = plt.subplots(2, 1)
#
# ax1.axis('off')
# ax2.set_xlim([0, 20])
# ax2.set_ylim([-2, 3])
# ax2.set_xlabel('Time (s)')
# ax2.set_ylabel('$\Delta F/F$ (%)', fontsize=12)
#
# # Set up animated line for signal plot
# curve1, = ax2.plot([], [], linewidth=1)
#
# # Set up time data
# t = experiment['pData']['T'][:400]
#
# # Set up video writer
# myVideo = imageio.get_writer('', fps=20)
#
#
# def update(i):
#     # Read image frame from video
#     v.set(cv2.CAP_PROP_POS_FRAMES, st + i)
#     ret, frame = v.read()
#
#     # Update image subplot
#     ax1.imshow(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
#
#     # Update signal subplot
#     curve1.set_data(t[:i], sig[:i])
#
#     # Write frame to video
#     myVideo.append_data(frame)
#
#     return ax1, curve1,
#
#
# # Set up animation
# ani = FuncAnimation(fig, update, frames=400, interval=20, blit=True)
#
# # Save video
# ani.save('', writer=myVideo)
#
# # Close video writer and release video capture
# myVideo.close()
# v.release()