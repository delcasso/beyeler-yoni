import pandas as pd
import numpy as np



def generate_xlsx_unit_properties_output(params):
    units = params['ephys']['units']

    with open(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_unit_properties_res.xls', 'w') as f:

        f.write('cluster_id\tFR\tP2V\tAUP\n')

        for k, v in units.items():
            u = units[k]
            wfs = u['wfs']['avg']
            xs = u['convolved_fr_Hz']
            ys = u['peak2valley_time_us']
            zs = u['aup']
            f.write(f'{k}\t{xs}\t{ys}\t{zs}\n')

def generate_xlsx_beh_output(params):
    d={}
    d['AnimalNumber'] = [params['mouse']['id']]
    d['Sex'] = [params['mouse']['sex']]
    b = params['beh_analysis']
    p = b['periodes']
    periodes = []
    for k,v in p.items():
        periodes.append(k)
    zones=[]
    for k,v in p[periodes[0]].items():
        zones.append(k)
    variables = []
    for k,v in p[periodes[0]][zones[0]].items():
        variables.append(k)
    for v in variables:
        for p in periodes:
            for z in zones:
                d[f'{p}-{v}-{z}'] = [params['beh_analysis']['periodes'][p][z][v]]
    df = pd.DataFrame(d)
    df.to_excel(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_behavior.xlsx')


def generate_xlsx_behspikes_output(params):
    b = params['beh_analysis']
    p = b['periodes']
    periodes = []
    for k,v in p.items():
        periodes.append(k)
    zones=[]
    for k,v in p[periodes[0]].items():
        zones.append(k)

    with open(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_spike_number.xls', 'w') as f:
        #header
        f.write('cluster_id')
        for p in periodes:
            for z in zones:
                f.write(f'\t{p}-{z}')
        f.write('\n')

        for k, v in params['ephys']['units_framed'].items():
            f.write(f'{k}')
            for p in periodes:
                for z in zones:
                    clu_str = f'cluster{k}_nspikes'
                    f.write(f'\t{params["beh_analysis"]["periodes"][p][z][clu_str]}')
            f.write('\n')


    with open(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_spike_avg.xls', 'w') as f:
        #header
        f.write('cluster_id')
        for p in periodes:
                f.write(f'\t{p}')
        f.write(f'\tAvg à partir de la 3ème colonne')
        f.write(f'\tStd à partir de la 3ème colonne')
        f.write('\n')

        for k, v in params['ephys']['units_framed'].items():
            f.write(f'{k}')
            tmp_list=[]
            for p in periodes:
                tmp = p[0:-3].split('-')
                p_duration = int(tmp[1]) - int(tmp[0])
                spike_sum = 0
                for z in zones:
                    clu_str = f'cluster{k}_nspikes'
                    spike_sum += params["beh_analysis"]["periodes"][p][z][clu_str]
                spk_avg = spike_sum/p_duration
                # spk_avg = spike_sum[1] /
                tmp_list.append(spk_avg)
                f.write(f'\t{spk_avg:.4f}')

            tmp_list.pop(0)
            f.write(f'\t{np.mean(tmp_list)}')
            f.write(f'\t{np.std(tmp_list)}')
            f.write('\n')





