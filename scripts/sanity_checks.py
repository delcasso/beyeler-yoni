


def video_tracking_sanity_check(params):
    #TODO: check the duration of the last ADC1 pulse
    videotrack_frame_num = len(params['videotrack']['x'])
    ephys_pulses_num = len(params['ephys']['ADC1_pulses']['onsets'])
    print(f'frame num diff = {videotrack_frame_num-ephys_pulses_num}')
