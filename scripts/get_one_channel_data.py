import numpy as np
import matplotlib.pyplot as plt
import spikeinterface.extractors as se
import spikeinterface.preprocessing as sp
# from scalebars import add_scalebar

# ephys_path = r'C:\Users\ycouderc\Desktop\20221105\F25_2022-11-05_16-03-20_SQ\Record Node 101\experiment1\recording1\continuous\Rhythm_FPGA-100.0\continuous.dat'
# F:\01 - RAW DATA\F10\20211111\F10_2021-11-11_17-03-54_OFT\Record Node 109\experiment1\recording1\continuous\Rhythm_FPGA-108.0
ephys_path = r'C:\in-vivo-ephys\data\raw\F23\20230407\F23_2023-04-07_09-57-33_EPM\Record Node 101'
# d = np.memmap(ephys_path, dtype=np.int16)
# d_ = np.reshape(d,(int(d.shape[0]/16),3))
# # return d[:, ch_id]
# fig, ax = plt.subplots(1, 1)
# # plt.plot(d[i1:i2])

# plt.plot(d[i1:i2])
# #d,30000*200,30000*300
# plt.show()
# plt.ion()
# plt.savefig('trace'.png)
# from neo.rawio import OpenEphysBinaryRawIO
# r = OpenEphysBinaryRawIO(dirname=ephys_path)
# r.parse_header()
# print(r)
# raw_chunk = r.get_analogsignal_chunk(block_index=0, seg_index=0,i_start=i1, i_stop=i2)
# plt.plot(raw_chunk[:,3])
# plt.show()
import spikeinterface as si

i1=30000*200
i2=30000*202
recording = se.read_openephys(ephys_path)
recording_f = sp.bandpass_filter(recording)
recording_f = sp.common_reference(recording_f)
to_plot = recording_f.get_traces(start_frame=i1,end_frame=i2,channel_ids=['CH7','CH8','CH9'])
# sb = add_scalebar(to_plot, matchx=False, matchy=False, sizex=1000, sizey=100, labelx="50ms",labely='100mV', loc=3,
#                       bbox_to_anchor=(400, 0), barwidth=bw, sep=4)
plt.plot(to_plot[:,0]+2000)
plt.plot(to_plot[:,1]+3000)
plt.plot(to_plot[:,2]+4000)
# plt.plot(to_plot[:,3]+5000)
plt.savefig(r'C:\in-vivo-ephys\data\raw\F23\20230407\graph.svg')
plt.savefig(r'C:\in-vivo-ephys\data\raw\F23\20230407\graph.png')
# fig.savefig(ephys_path/'graph.svg')
plt.ion()
plt.show()
# plt.close()
