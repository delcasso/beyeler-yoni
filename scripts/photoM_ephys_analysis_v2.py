from ephys_tools import get_one_ch_data,analog_2_events, get_multiple_ch_data
import matplotlib.pyplot as plt
import h5py
from pathlib import Path
import numpy as np
from scipy.signal import butter, lfilter
import pandas as pd
from scipy.signal import find_peaks

rec = {
    'ephys':
        {
            'led_signal':[],
            'led_events':[],
            't_ephys':[]
        },
    'photoM':
        {
            'raw':
                {
                    '405': [],
                    '470': [],
                    'time': []
                }
        }
}

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def delete_first_min(photoM_405, photoM_470, t_ephys_dest):
    periode = np.median(np.diff(t_ephys_dest))
    fq = 1/periode
    i1 = int(60*fq)
    return photoM_405[i1:], photoM_470[i1:], t_ephys_dest[i1:]

def process_photoM_rawdata(rec):
    photoM_405 = rec['photoM']['raw']['405']
    photoM_470 = rec['photoM']['raw']['470']
    t_ephys_dest = rec['photoM']['raw']['ephys_time']
    photoM_405, photoM_470, t_ephys_dest =  delete_first_min(photoM_405, photoM_470, t_ephys_dest)
    iso_fitted, dff = process_dff(photoM_405, photoM_470)
    rec['photoM']['processed'] = {'405': photoM_405, '470': photoM_470, 'ephys_time': t_ephys_dest, 'dff': dff, 'iso_fitted':iso_fitted}
    return rec

def process_dff(photoM_405, photoM_470):
    a, b = np.polyfit(photoM_405, photoM_470, 1)
    fit_iso = a*photoM_405+b
    dff = (photoM_470-fit_iso)/fit_iso
    dff*=100.0
    return fit_iso, dff

def load_ephys_data(ephys_path, rec, sfreq=30000):
    rec['ephys']['led_signal'] = get_one_ch_data(filepath=ephys_path, total_ch_number=27, ch_id=20)
    rec['ephys']['led_events'] = analog_2_events(rec['ephys']['led_signal'])
    rec['ephys']['t_ephys'] = np.arange(0, len(rec['ephys']['led_signal'])) / sfreq
    rec['ephys']['path'] = ephys_path
    return rec

def spikes_binning(spikes_export_filepath, rec):
    df = pd.read_csv(spikes_export_filepath, header=None)
    rec['ephys']['cluster_ids'] = df[1]
    rec['ephys']['cluster_rec_num'] = df[0]
    rec['ephys']['cluster_ids_set'] = set(rec['ephys']['cluster_ids'])

    n_clusters = len(rec['ephys']['cluster_ids_set'])
    binned_spikes = np.zeros((len(rec['photoM']['processed']['ephys_time']) - 1, n_clusters))

    for id in rec['ephys']['cluster_ids_set']:
        spike_times = df[df[1] == id][0] / 30000
        binned_spikes[:, id] = np.histogram(spike_times, bins=rec['photoM']['processed']['ephys_time'])[0]
    rec['photoM']['processed']['binned_spikes'] = binned_spikes
    rec['photoM']['processed']['sum_binned_spikes'] = np.sum(rec['photoM']['processed']['binned_spikes'], axis=1)
    return rec

def load_photoM_data(photom_path,rec, sfreq=20):
    f = h5py.File(photom_path, 'r')
    photom_sig = f.get('sig')
    rec['photoM']['raw']['405'] = np.array(photom_sig[:, 0])
    rec['photoM']['raw']['470'] = np.array(photom_sig[:, 1])  # For converting to a NumPy array
    rec['photoM']['raw']['time'] = np.arange(0, len(photom_sig)) / sfreq
    rec['photoM']['raw']['path'] = photom_path
    return rec

def synchronise_photoM_to_ephys(rec):
    t_led_start_ephys = rec['ephys']['t_ephys'][rec['ephys']['led_events']['onsets']]
    t_synchro = t_led_start_ephys
    x_ephys_origin = np.arange(0, len(t_led_start_ephys))
    x_ephys_dest = np.arange(0, len(t_led_start_ephys), 0.01)
    rec['photoM']['raw']['ephys_time'] = np.interp(x_ephys_dest, x_ephys_origin, t_synchro)
    return rec

def find_dff_peaks(rec, prominence=(2,10)):
    peaks, properties = find_peaks(rec['photoM']['processed']['dff'], prominence=prominence)
    rec['photoM']['transients'] = {'peaks': peaks, 'properties': properties}
    return rec

def plot_all(rec):
    fig, axs = plt.subplots(4,1, sharex=True)
    axs[0].plot(rec['photoM']['raw']['ephys_time'],rec['photoM']['raw']['405'],color=[1,0,1])
    axs[0].plot(rec['photoM']['raw']['ephys_time'],rec['photoM']['raw']['470'],color=[0,0,1])
    axs[1].plot(rec['photoM']['processed']['ephys_time'],rec['photoM']['processed']['405'],color=[1,0,1])
    axs[1].plot(rec['photoM']['processed']['ephys_time'],rec['photoM']['processed']['iso_fitted'],color=[1,0,1],linestyle=':')
    axs[1].plot(rec['photoM']['processed']['ephys_time'],rec['photoM']['processed']['470'],color=[0,0,1])
    axs[2].plot(rec['photoM']['processed']['ephys_time'],rec['photoM']['processed']['dff'],color=[0,1,0])
    peaks = rec['photoM']['transients']['peaks']
    axs[2].plot(rec['photoM']['processed']['ephys_time'][peaks],rec['photoM']['processed']['dff'][peaks],'+')
    axs[3].plot(rec['photoM']['processed']['ephys_time'][0:-1]+1/15000, rec['photoM']['processed']['sum_binned_spikes'])
    return fig, axs

def plot_snippets(rec):
    n_clusters = rec['photoM']['processed']['binned_spikes'].shape[1]
    for i_clu in range(n_clusters + 1):
        m = rec['photoM']['processed']['snippets'][i_clu]
        fig, axs = plt.subplots(1, 2, sharex=True)
        axs[0].imshow(m)
        axs[0].axis('off')
        axs[1].plot(m.T, color=[0.7, 0.7, 0.7], linewidth=0.1)
        mean_ = np.mean(m.T, axis=1)
        std_ = np.std(m.T, axis=1)
        axs[1].plot(mean_, color=[1, 0.3, 0.3], linewidth=2)
        axs[1].plot(mean_ + std_, color=[1, 0.3, 0.3], linewidth=0.5)
        axs[1].plot(mean_ - std_, color=[1, 0.3, 0.3], linewidth=0.5)


def refresh(ax,peaks):
    ax.clear()
    ax.plot(rec['photoM']['processed']['ephys_time'], rec['photoM']['processed']['dff'], color=[0, 1, 0])
    ax.plot(rec['photoM']['processed']['ephys_time'][peaks],rec['photoM']['processed']['dff'][peaks],'+')

def extract_snippets(rec, window_sec=1):

    peaks = rec['photoM']['transients']['peaks']
    window_samples = window_sec * photoM_sfreq
    window_samples = (window_samples // 2) * 2 + 1
    h_window_samples = window_samples // 2

    n_clusters = rec['photoM']['processed']['binned_spikes'].shape[1]
    rec['photoM']['processed']['snippets'] ={}

    rec['photoM']['processed']['binned_spikes'] = np.column_stack((rec['photoM']['processed']['binned_spikes'],rec['photoM']['processed']['sum_binned_spikes']))

    for i_clu in range(n_clusters+1):
        matrix_ = np.zeros((len(peaks), window_samples))
        binned_spikes = rec['photoM']['processed']['binned_spikes'][:,i_clu]
        for i, p in enumerate(peaks):
            tmp = binned_spikes[p - h_window_samples:p + h_window_samples + 1]
            matrix_[i, :] = tmp
        rec['photoM']['processed']['snippets'][i_clu]=matrix_

    return rec




datafolder = Path(r'Y:\Ephys in vivo\ephys_photoM\M2394\20230216')
ephys_path = datafolder / r'F23_2023-02-16_09-09-36_SignalCheck\Record Node 104\experiment1\recording1\continuous\Rhythm_FPGA-100.0\continuous.dat'
ephys_sampling_rate = 30000
rec = load_ephys_data(ephys_path, rec, sfreq=ephys_sampling_rate)
photom_path = datafolder / 'M2394_OFT_001.mat'
photoM_sfreq = 20
rec = load_photoM_data(photom_path,rec, sfreq=photoM_sfreq)
rec = synchronise_photoM_to_ephys(rec)
rec = process_photoM_rawdata(rec)
spikes_export_filepath = Path(r"Y:\Ephys in vivo\ephys_photoM\M2394\20230216\F23_2023-02-16_09-09-36_SignalCheck\Record Node 104\experiment1\recording1\continuous\Rhythm_FPGA-100.0\tdc_continuous\export\spikes - segNum 0 - chanGrp 0 - cell#all.csv")
rec = spikes_binning(spikes_export_filepath, rec)
rec = find_dff_peaks(rec)
fig, axs = plot_all(rec)

window_sec = 1
rec = extract_snippets(rec, window_sec=window_sec)

plot_snippets(rec)





