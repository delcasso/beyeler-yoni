# sphinx-apidoc -o . ../scripts
"""
This is the main file of the program
Here in the analyze_one_file function, we can see entire workflow
"""
from pathlib import Path
from file_tools import find_all_paths
from opencv_tools import get_video_info
from bonsai_tools import import_bonsai_file
from plot_tools import plot_raw_trajectory, plot_cell_summary, plot_avg_waveform, plot_unit_properties
from behavior_analysis_tools import behavioral_analysis
from scripts.behavior_and_ephys_analysis.zone_analysis import zone_analysis
from scripts.behavior_and_ephys_analysis.psth_analysis import sq_psth_analysis, epm_psth_analysis
from scripts.behavior_and_ephys_analysis.heatmaps import heatmap_analysis
from ephys_tools import get_video_frames_onsets, load_spikes, synchronise_streams, get_sq_licks_onsets, get_probe_geometry, \
    measure_unit_properties
from sanity_checks import video_tracking_sanity_check
from output_tools import generate_xlsx_beh_output, generate_xlsx_behspikes_output, generate_xlsx_unit_properties_output
from excel_tools import generate_xls_output
import pickle


def analyze_one_file(rawdata_path_, analysis_path_, exp_type_='behavior_only'):

    params = {'exp_type': exp_type_, 'paths': {}}
    params['paths']['rawdata'] = rawdata_path_
    params['paths']['analysis'] = analysis_path_

    params = find_all_paths(params)
    params = get_video_info(params)
    params = import_bonsai_file(params)

    params['beh_analysis'] = {'bin_size_sec': 300, 'bin_number': 12, 'periodes': {}}
    params = zone_analysis(params)
    params = behavioral_analysis(params)


    plot_raw_trajectory(params, debug_mode='on')
    generate_xlsx_beh_output(params)

    filename = params['paths']['analysis'] / f'{params["paths"]["prefix"]}_data.pickle'
    outfile = open(filename, 'wb')
    pickle.dump(params, outfile)
    outfile.close()

    return params

if __name__ == '__main__':

    rawdata_path = Path(r'D:\ephys\raw\M2502\20230512')
    analysis_path = Path(r'D:\ephys\analysis\M2502\20230512')
    analyze_one_file(rawdata_path, analysis_path)