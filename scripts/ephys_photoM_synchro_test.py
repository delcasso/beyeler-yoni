from ephys_tools import get_one_ch_data,analog_2_events
import matplotlib.pyplot as plt
import h5py
from pathlib import Path
import numpy as np

datafolder = Path(r'E:\NAS_SD\SuiviClient\Beyeler\ephys_photoM_synchro')
# datafolder = Path(r'Y:\Ephys in vivo\01 - RAW DATA\_Photometry&Ephys\M2392\20231002')
# ephys_path = datafolder / r'M2392_2023-02-10_12-22-19_SignalCheck\Record Node 104\experiment1\recording1\continuous\Rhythm_FPGA-100.0\continuous.dat'
ephys_path = datafolder / r'e2_2023-01-25_10-04-47\Record Node 101\experiment1\recording1\continuous\Rhythm_FPGA-100.0\continuous.dat'
bonsai_path = datafolder / 'v2-bonsai.csv'
photoM_path = datafolder / 'p2_000.mat'
f = h5py.File(photoM_path,'r')
_, _, _, _, _, _, optoPeriod = np.genfromtxt(bonsai_path,dtype=float,skip_header=1, unpack=True)
photoM_sig = f.get('sig')
photoM_sig = np.array(photoM_sig[:,0]) # For converting to a NumPy array
# mat = scipy.io.loadmat()
camera_signal = get_one_ch_data(filepath=ephys_path, total_ch_number=27, ch_id=19)
camera_events = analog_2_events(camera_signal)
led_signal = get_one_ch_data(filepath=ephys_path, total_ch_number=27, ch_id=20)
led_events = analog_2_events(led_signal)
t_ephys = np.arange(0,len(camera_signal))/30000
t_photoM = np.arange(0,len(photoM_sig))/20
# plt.figure()
photoM_events = analog_2_events(photoM_sig)


t_led_start_ephys = t_ephys[led_events['onsets']]
t_led_start_photoM = np.arange(0,t_photoM[-1],5)
t_synchro = t_led_start_ephys
x_ephys_origin = np.arange(0,len(t_led_start_ephys))
x_ephys_dest = np.arange(0,len(t_led_start_ephys),0.01)
t_ephys_dest = np.interp(x_ephys_dest,x_ephys_origin,t_synchro)
i1=30000*200
i2=30000*202

fig,axs = plt.subplots(4,1, sharex=True)
axs[0].plot(t_ephys,led_signal)
axs[0].set_title('LED')
# axs[1].plot(t_ephys,camera_signal)
# axs[1].set_title('Camera signal')
axs[2].plot(t_photoM,photoM_sig)
axs[2].set_title('photoM signal')
axs[3].plot(t_ephys_dest,photoM_sig)
axs[3].set_title('photoM+ephys synchro')
axs[1].plot(t_ephys[camera_events['onsets'][:-1]],optoPeriod)
axs[1].set_title('Behavioral Camera')


# plt.show()
# axs[0].plot(t_photoM,photoM_sig)
# axs[0].set_title('photoM signal')
# axs[1].plot(t_ephys_dest,photoM_sig)
# axs[1].set_title('photoM+ephys synchro')

# plt.figure()
plt.savefig(r'E:\NAS_SD\SuiviClient\Beyeler\ephys_photoM_synchro\synchro.png')
# plt.savefig(r'Y:\Ephys in vivo\01 - RAW DATA\_Photometry&Ephys\M2392\20231002\M2392_2023-02-10_12-22-19_SignalCheck\Record Node 104\graph photoM.png')

plt.figure()
d=t_led_start_ephys[1:]-t_ephys_dest[photoM_events['onsets']]
plt.hist(d*1000)

plt.show()
print('here')