# sphinx-apidoc -o . ../scripts
"""
This is the main file of the program
Here in the analyze_one_file function, we can see entire workflow
"""
import os
from pathlib import Path
from file_tools import find_all_paths
from opencv_tools import get_video_info
from bonsai_tools import import_bonsai_file
from plot_tools import plot_raw_trajectory, plot_cell_summary, plot_avg_waveform, plot_unit_properties
from behavior_analysis_tools import behavioral_analysis
from scripts.behavior_and_ephys_analysis.zone_analysis import zone_analysis
from scripts.behavior_and_ephys_analysis.psth_analysis import sq_psth_analysis, epm_psth_analysis
from scripts.behavior_and_ephys_analysis.heatmaps import heatmap_analysis
from ephys_tools import get_video_frames_onsets, load_spikes, synchronise_streams, get_sq_licks_onsets, get_probe_geometry, \
    measure_unit_properties
from sanity_checks import video_tracking_sanity_check
from output_tools import generate_xlsx_beh_output, generate_xlsx_behspikes_output, generate_xlsx_unit_properties_output
from excel_tools import generate_xls_output
import pickle


def analyze_one_file(rawdata_path_, analysis_path_, exp_type_='ephys'):
    """
    This is the main function that is called when one session has to be analyzed

    + find_all_paths : smart way to extract paths and info on the recording session based on a well maintained organisation of the folders
    + get_video_info : extract the framerate, the dimensions and the first frame of the video
    + import_bonsai_file : import video transcking data
    + get_video_frames_onsets : load from the ephys file the timestamps of the pulses send by the camera at each frame
    + video_tracking_sanity_check : check the consistency between the videotracking from bonsai and the pulses in the ephys recording
    + behavioral_analysis : analysis the behavior of the animal based on predifined zone in the apparatus
    + load_spikes : load the resutls from tridesclous
    + synchronise_streams : synchronize the spike timestamps and the bonsai data
    + plot_raw_trajectory : plot the animal trajectory
    + plot_cell_summary : plot a summary fro each recorded cells

    :param rawdata_path_: path to the raw data

    :param analysis_path_: path to the folder were we save the results of the analysis

    :return: void

    """
    params = {'exp_type': exp_type_, 'paths': {}}
    params['paths']['rawdata'] = rawdata_path_
    params['paths']['analysis'] = analysis_path_
    params = find_all_paths(params)
    params = get_video_info(params)
    params = import_bonsai_file(params)
    video_tracking_sanity_check(params)

    #depending of the recording configuration we could use in most of the cases
    # total_ch_number = 27, ch_id = 19
    # if params['mouse']['name']=='M5' and params['session']['date']=='20210127':
    #     params = get_video_frames_onsets(params, total_ch_number=24, ch_id=16)
    # else:
    #     params = get_video_frames_onsets(params)


    if params['exp_type'] == 'ephys':
        params = load_spikes(params)
        params = synchronise_streams(params)
        params = get_probe_geometry(params)
        plot_avg_waveform(params)
        params = measure_unit_properties(params)
        plot_unit_properties(params)
        params['beh_analysis'] = {'bin_size_sec': 300, 'bin_number': 4, 'periodes': {}}
        params = zone_analysis(params)
        params = behavioral_analysis(params)

        bin_size = 20
        heatmap_analysis(params, bin_size, show=False)

        if params['session']['task'] == 'SQ':
            params = get_sq_licks_onsets(params, total_ch_number=27, ch_ids=[20, 21])
            params = sq_psth_analysis(params, debug_mode=False)

        if params['session']['task'] == 'EPM':
            params = epm_psth_analysis(params)

    # generate_xlsx_unit_properties_output(params)

    #choose debug_mode='on' to display debug figure, one marker per animal position over the entire video
    plot_raw_trajectory(params, debug_mode='on')

    generate_xlsx_beh_output(params)

    if params['exp_type'] == 'behavior_only':
        params['beh_analysis'] = {'bin_size_sec': 3600, 'bin_number': 1, 'periodes': {}}
        params = zone_analysis(params)
        params = behavioral_analysis(params)
        # generate_xlsx_beh_output(params)

    filename = params['paths']['analysis'] / f'{params["paths"]["prefix"]}_data.pickle'
    outfile = open(filename, 'wb')
    pickle.dump(params, outfile)
    outfile.close()

    return

if __name__ == '__main__':
    # user entry _________
    mousename = "M3020"
    date = "20240310"
    electrodetypeidx = 1 #0: SP / 1: inPhy
    # __________________
    electrodetype = ["1_Silicone_Probes","2_In_Nphy"]
    prevpathname = r"Y:\Ephys_in_vivo"
    foldernames = ["01_RAW_DATA","02_ANALYSIS"]

    # rawdata_path = Path(r'Y:\Ephys_in_vivo\01_RAW_DATA\1_Silicone_Probes\M15\20220215')
    # analysis_path = Path(r'Y:\Ephys_in_vivo\02_ANALYSIS\2_In_Nphy\F23\20230712')
    rawdata_path =Path(prevpathname+"\\"+foldernames[0]+"\\"+electrodetype[electrodetypeidx]+"\\"+mousename+"\\"+date)
    analysis_path =Path(prevpathname+"\\"+foldernames[1]+"\\"+electrodetype[electrodetypeidx]+"\\"+mousename+"\\"+date)

    print(rawdata_path)
    print(analysis_path)
    if not os.path.exists(analysis_path):
        os.mkdir(analysis_path)
    analyze_one_file(rawdata_path, analysis_path)



