import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from matplotlib import cm
import numpy as np

def add_path(path, facecolor='#FFFFFF00', edgecolor='black', ax=None):
    if ax is None:
        fig, ax = plt.subplots(1, 1)
    ax.add_patch(PathPatch(path, edgecolor=edgecolor, facecolor=facecolor, linewidth=2, linestyle='-'))


def plot_cell_summary(params, unit_id=0, ax=None):
    plt.ion()
    framed_spikes = params['ephys']['units_framed'][unit_id]
    spikes_idx = params["ephys"]["units"][unit_id]
    n_spikes = len(spikes_idx)
    d_idx = spikes_idx[-1] - spikes_idx[0]
    ephys_sfreq = 30000
    d_sec = d_idx / ephys_sfreq
    unit_fr = n_spikes / d_sec

    if ax is None:
        fig, ax = plt.subplots(3, 3)

    fig.suptitle(f'unit {unit_id}', fontsize=16)
    ax[0,0].axis('off')
    text_kwargs = dict(ha='left', va='center', fontsize=10, color='#000000')
    ax[0,0].text(0, 1, f'{n_spikes} spikes', **text_kwargs)
    ax[0, 0].text(0, 0.85, f'{unit_fr:.2f} Hz', **text_kwargs)

    plt.show()
    return


def plot_raw_trajectory(params, debug_mode=None, ax=None):
    """
    Draw the trajectory of mouse location depending on experimental time and save this graph.
    """
    plt.ion()
    if ax is None:
        fig, ax = plt.subplots(1, 1)

    ax.imshow(params['video']['frame0'], origin='lower')

    n_zones = len(params['apparatus']['zones'])
    colors = cm.get_cmap('tab10',n_zones)
    for i,z in enumerate(params['apparatus']['zones']):
        if not (z['name'] == 'entire_apparatus'):
            p = z['path']
            add_path(p, ax=ax, edgecolor=colors(i/n_zones))
    ax.plot(params['videotrack']['x'], params['videotrack']['y'], '-', c='#222222FF', linewidth=0.5)

    if debug_mode == 'on':
        x_ = params['videotrack']['x']
        y_ = params['videotrack']['y']
        i=0
        for k,v in params['videotrack']['in_zone'].items():
            if not (k == 'entire_apparatus'):
                x__ = x_[v]
                y__ = y_[v]
                ax.plot(x__, y__, c=colors(i/n_zones), markersize=5, marker='.', linestyle='')
                i+=1

    figure_path = params['paths']['analysis'] / f'{params["paths"]["prefix"]}_trajectory.svg'
    plt.savefig(figure_path, format='svg')
    plt.show()
    return


def plot_unit_properties(params):

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    units = params['ephys']['units']

    for k, v in units.items():
        u = units[k]
        wfs = u['wfs']['avg']
        xs = u['convolved_fr_Hz']
        ys = u['peak2valley_time_us']
        zs = u['aup']
        ax.scatter(xs, ys, zs, c='r', marker='o')

    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel('Spike half-width (µs)')
    ax.set_zlabel('Area under peak (a.u)')

    plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_wfs_properties.svg')
    plt.close()


def plot_avg_waveform(params):
    units = params['ephys']['units']
    prb = params['ephys']['probe_geometry']
    ch = prb[0]['channels']
    geo = prb[0]['geometry']

    x=[]
    y=[]
    for id in ch:
        x.append(geo[id][0])
        y.append(geo[id][1])

    x = [x_*10 for x_ in x]
    y = [y_ / np.max(y) for y_ in y]
    y = [y_ * 12 for y_ in y]

    for k,v in units.items():
        u = units[k]
        wfs = u['wfs']['avg']


        amplitudes = np.max(wfs,axis=0) - np.min(wfs,axis=0)
        ch_amp_max = np.argmax(amplitudes)

        wfs = wfs - wfs.min()
        wfs = wfs / wfs.max()
        t = u['wfs']['time']
        fig = plt.figure()
        for i in range(wfs.shape[1]):
            plt.plot(np.arange(wfs.shape[0])+x[i],wfs[:,i]+y[i],color='black')
        # plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_wfs_avg_u{k}.svg')
        plt.close()

        fig = plt.figure()
        plt.plot(wfs[:, ch_amp_max], color='black')
        # plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_maxwf_u{k}.svg')
        plt.close()

