import numpy as np
import matplotlib.pyplot as plt
from tridesclous import DataIO
from typing import Tuple
from scipy.signal import butter, sosfilt

ephys_path = r'/Users/yonicouderc/Downloads/20221105/F25_2022-11-05_16-03-20_SQ/Record Node 101/experiment1/recording1/continuous/Rhythm_FPGA-100.0/continuous.dat'
d = np.memmap(ephys_path, dtype=np.int16)
d = np.reshape(d,(int(d.shape[0]/16),-1))
# return d[:, ch_id]
fig, ax = plt.subplots(1, 1)
# plt.plot(d[i1:i2])
i1=30000*200
i2=30000*220
plt.plot(d[i1:i2])
#d,30000*200,30000*300
plt.show()
plt.ion()
plt.savefig('trace'.png)