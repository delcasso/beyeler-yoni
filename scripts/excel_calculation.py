import pandas as pd

# Load the Excel file into a pandas DataFrame
df = pd.read_excel(r"Y:\Ephys in vivo\02 - ANALYZED DATA\M21\20220704\Firing_Rate_M21_Python.xlsx",header=None, skiprows=1)

# Define the row indices to use for division
numerator_row_index = 0
denominator_row_index = 31

# Loop through each row in the DataFrame
if numerator_row_index >= df.shape[0] or denominator_row_index >= df.shape[0]:
    print("Error: One or more row indices is out of bounds.")
else:
    # Loop through each row in the DataFrame
    for index, row in df.iterrows():
        # Divide each cell in the row by the corresponding cell in the denominator row
        df.iloc[index,:] = row * df.iloc[denominator_row_index,:] / df.iloc[numerator_row_index,:]
        # for index, row in df.iterrows():
        #     # Divide each cell in the row by the corresponding cell in the denominator row
        #     df.iloc[index, 32:80] = row / df.iloc[denominator_row_index, 32:80] * df.iloc[numerator_row_index, 0:80]
    # Write the updated DataFrame to a new Excel file
    df.to_excel(r"Y:\Ephys in vivo\02 - ANALYZED DATA\M21\20220704\Firing_Rate_M21_Python_output.xlsx", index=False)