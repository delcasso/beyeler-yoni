import pandas as pd
import numpy as np

def import_bonsai_file(params):
    """
    this function loads the typical bonsai file as made originally by S.Delcasso in the Beyeler Lab.
    this function only add mouseX and mouseY toh the main params dictionnary
    :param params: before update
    :return: after update
    """
    filepath = params['paths']['bonsai']
    params['videotrack'] = {}
    mouseX, mouseY, mouseAngle, mouseMajorAxisLength, mouseMinorAxisLength, mouseArea, optoPeriod = np.genfromtxt(filepath, dtype=float, skip_header=1, unpack=True)
    params['videotrack']['x'] = mouseX
    params['videotrack']['y'] = params['video']['height'] - mouseY
    dx = np.diff(params['videotrack']['x'], prepend=params['videotrack']['x'][0])
    dy = np.diff(params['videotrack']['y'], prepend=params['videotrack']['y'][0])
    params['videotrack']['dist'] = np.sqrt(dx**2 + dy**2) / params['video']['px_by_cm']
    params['videotrack']['frame_num'] = mouseX.shape
    return params