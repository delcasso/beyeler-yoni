#  Peri-event analysis in the EPM_20230721

import pickle
from scripts.ephys_tools import get_one_ch_data, butter_bandpass_filter, creates_time_vector
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.patches as patches
import numpy as np
from scripts.behavior_and_ephys_analysis.SQ_analysis import get_licks
from scripts.behavior_and_ephys_analysis.zone_analysis import zone_change
from pathlib import Path


TRACE_COLOR = (0.5, 0.5, 0.5)

def plot_rawtrace_withevents(params, unit_key, sfreq, before_sec, after_sec, ax=None):
    """
    This is a debug function to plot the raw trace (channels where the unit has the biggest waveform,
    all the spikes of the unit (gray asterisk), the event times,
    the psth time_windows and the selected spikes (the one within these time_periods)
    :param params: the main variable of the program that contains all the data
    :param unit_key: the key of the unit because info relative to units are stored in a dict
    :param sfreq: the sampling frequency of the ephys recording
    :param before_sec: the size of the window before the event
    :param after_sec: the size of the psth window after the event
    :param ax: one axes if you would like to include this plit in your own figure
    :return: void
    """

    t = params['ephys']['t']
    u = params['ephys']['units'][unit_key]
    wfs = u['wfs']['avg']
    amplitudes = np.max(wfs, axis=0) - np.min(wfs, axis=0)
    ch_amp_max = np.argmax(amplitudes)
    selected_ch = params['ephys']['probe_geometry'][0]['channels'][ch_amp_max]
    d = get_one_ch_data(filepath=params['paths']['ephys_dat'], total_ch_number=27,
                        ch_id=selected_ch)
    d2 = butter_bandpass_filter(d, lowcut=600, highcut=6000, fs=30000, order=4)
    min_d2 = np.percentile(d2, 1)
    max_d2 = np.percentile(d2, 99)
    bar_size = (max_d2 - min_d2)*2.5
    before_idx = before_sec * sfreq
    after_idx = after_sec * sfreq
    plt.ion()

    if ax is None:
        fig, ax = plt.subplots()

    rawdata_plot, = ax.plot(t, d2, color=TRACE_COLOR)
    rawdata_plot.set_label(f'ch {selected_ch} filtered [600-6000Hz]')
    spk_plot = ax.plot(t[u['idx']], np.repeat(min_d2 - (bar_size * 1.5), len(u['idx'])), '*', color=TRACE_COLOR)
    spk_plot[0].set_label(f'spikes from cluster {unit_key}')

    ax.legend()
    ax.set_ylabel('Analog signal (V)')
    ax.set_xlabel('Time (s)')

    behaviors = ('lick_ch0', 'lick_ch1')
    for beh in behaviors:
        lick_idx = params['behavior'][beh]['idx']
        for i, idx in enumerate(lick_idx):
            c = params['behavior'][beh]['display_color']
            ax.plot((t[idx], t[idx]), (min_d2 - bar_size, max_d2 + bar_size), color=c)
            ax.plot(t[idx], max_d2 + bar_size, 'v', color=c)
            ax.text(t[idx], max_d2 + bar_size * 1.1, f'trial #{i}', color=c, rotation=90)

            idx1 = idx - before_idx
            idx2 = idx + after_idx
            x = t[idx-before_idx]
            y = min_d2 - bar_size
            w = before_sec + after_sec
            h = max_d2 - min_d2 + (2 * bar_size)

            ax.add_patch(patches.Rectangle((x, y), w, h, facecolor=c, fill=True, alpha=0.2))
            spk_idx = np.where(np.logical_and(u['idx'] >= idx1, u['idx'] <= idx2))[0]
            spk_idx = u['idx'][spk_idx]

            for j in spk_idx:
                ax.plot(t[j], min_d2 - (bar_size * 1.25), '*', color=c)
                ax.plot(t[j - 45:j + 60], d2[j - 45:j + 60], color=c)

    plt.show()

    return

def build_psth(params, unit_key, event_idx, sfreq, before_sec, after_sec, bin_size_sec):

    u = params['ephys']['units'][unit_key]
    t = params['ephys']['t']

    before_idx = before_sec * sfreq
    after_idx = after_sec * sfreq

    edges = np.arange(-before_sec, after_sec, bin_size_sec)
    params['beh_analysis']['psth_Hz']['params']['edges'] = edges
    psth_histo = np.empty((len(event_idx), len(edges) - 1), dtype=float)
    psth_histo.fill(np.nan)

    raster = {}

    for i, idx in enumerate(event_idx, start=0):
        idx1 = idx - before_idx
        idx2 = idx + after_idx
        spk_idx = np.where(np.logical_and(u['idx'] >= idx1, u['idx'] <= idx2))[0]
        spk_idx = u['idx'][spk_idx]
        spk_ts = t[spk_idx] - t[idx]
        hist, bin_edges = np.histogram(spk_ts, bins=edges)
        psth_histo[i, :] = hist
        raster[i] = spk_ts
        # ax2.plot((t[idx1] - t[idx], t[idx2]- t[idx]), (i, i), ':', color=c)
        y = np.repeat(i, len(spk_ts))
        #ax_raster[k].plot(spk_ts, y, '8', color=c)
    #ax_raster[k].plot((0,0), (-1, len(event_idx) + 1), 'k')

    # x = edges[:-1]+(bin_size_sec/2)
    # y = np.mean(psth_histo, axis=0)
    # y = y/bin_size_sec
    #ax_psth[k].plot(x, y, color=c)
    #ax_graph_combo.plot(x, y, color=c)

    psth_histo = psth_histo / bin_size_sec

    return {'psth_Hz': psth_histo, 'raster': raster}

def plot_raster(params, unit_key, all_raster, raster_name, marker_color=(0.5, 0, 0), ax=None):
    if ax is None:
        fig, ax = plt.subplots()
    ax.set_title(f'raster for cluster #{unit_key} and event #{raster_name}')
    raster = all_raster[unit_key]
    for trial_id, spk_ts in raster.items():
        y = np.repeat(trial_id, len(spk_ts))
        ax.plot(spk_ts, y, '|', color=marker_color)
    ax.plot((0, 0), (-1, len(raster) + 1), 'k')
    ax.set_ylabel('Trial #')
    ax.set_xlabel('Time (s)')
    plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_raster_{raster_name}_u{unit_key}.svg')
    plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_raster_{raster_name}_u{unit_key}.png')
    plt.close()

def plot_psth(params, unit_key, all_psth, psth_name, line_color=(0.5, 0, 0), ax=None, bar_mode=True):
    if ax is None:
        fig, ax = plt.subplots()

    ax.set_title(f'psth for cluster #{unit_key} and event #{psth_name}')
    psth = all_psth[unit_key]

    edges = params['beh_analysis']['psth_Hz']['params']['edges']
    bin_size_sec = params['beh_analysis']['psth_Hz']['params']['bin_size_sec']
    x = edges[:-1]+(bin_size_sec/2)

    y_mean = np.mean(psth, axis=0)
    y_std = np.std(psth, axis=0)
    y_mean = y_mean / bin_size_sec
    y_std = y_std / bin_size_sec

    if bar_mode:
        ax.bar(x, y_mean, color=line_color)
        ax.set_ylabel('Frequency (Hz)')
        ax.set_xlabel('Time (s)')
    else:
        ax.plot(x, y_mean, color=line_color)
        ax.fill_between(x, y_mean - y_std, y_mean + y_std, facecolor=line_color, edgecolor=None, alpha=0.2)

    plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_psth_{psth_name}_u{unit_key}.svg')
    # plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_psth_{psth_name}_u{unit_key}.png')
    plt.close()

def epm_psth_analysis(params,  show_individual_plots=False, sampling_frequency=30000, alternative_rawdata_folder=None):

    p = params['paths']['ephys_dat']
    if not p.exists():
        pattern_to_find = f'{params["mouse"]["name"]}/{params["session"]["date"]}'
        #pattern_position_in_old_path
        pos = p.as_posix().find(pattern_to_find)
        new_path = alternative_rawdata_folder / p.as_posix()[pos:]
        p = new_path
    if not p.exists():
        print('p not exist, no alternative rawdata found')
        return params

    params['ephys']['t'] = creates_time_vector(filepath=p, total_ch_number=27)
    params['behavior'] = {}
    params['beh_analysis']['psth_Hz'] = {}
    params['beh_analysis']['psth_Hz']['enterzone'] = {}
    params['beh_analysis']['psth_Hz']['exitzone'] = {}
    params['beh_analysis']['raster'] = {}
    params['beh_analysis']['raster']['enterzone'] = {}
    params['beh_analysis']['raster']['exitzone'] = {}

    enterzone_color = [1, 0, 0]
    exitzone_color = [0, 0, 1]

    params['ephys']['sfreq'] = sampling_frequency
    params['beh_analysis']['psth_Hz']['params'] = {'before_sec': 10, 'after_sec': 10, 'bin_size_sec': 0.05}

    params = zone_change(params)

    for k,u in params['ephys']['units'].items():
        plt.ion()
        for z in params['apparatus']['zones']:
            params = psth_analysis(params, event_type='enterzone', event_color=enterzone_color, fig_name=f'{z["name"]}-IN', zone_name=z['name'], unit_key=k,  show_individual_plots=show_individual_plots)
            params = psth_analysis(params, event_type='exitzone', event_color=exitzone_color, fig_name=f'{z["name"]}-OUT', zone_name=z['name'], unit_key=k,  show_individual_plots=show_individual_plots)

    return params

def psth_analysis(params, event_type, event_color, fig_name, zone_name, unit_key, show_individual_plots=False):

    event_idx = params['videotrack'][f'{event_type}_ephysidx'][zone_name]
    event_ts = params['videotrack'][f'{event_type}_ephysts'][zone_name]
    event_dt = np.diff(event_ts)
    idx = np.where(event_dt <= 1)
    idx = np.array(idx) + 1
    event_idx = np.delete(event_idx, idx)
    tmp = build_psth(params=params, unit_key=unit_key, event_idx=event_idx, sfreq=params['ephys']['sfreq'],
                     before_sec=params['beh_analysis']['psth_Hz']['params']['before_sec'],
                     after_sec=params['beh_analysis']['psth_Hz']['params']['after_sec'],
                     bin_size_sec=params['beh_analysis']['psth_Hz']['params']['bin_size_sec'])
    params['beh_analysis']['psth_Hz'][event_type][unit_key] = tmp['psth_Hz']
    params['beh_analysis']['raster'][event_type][unit_key] = tmp['raster']

    if show_individual_plots:
        plot_psth(params=params, unit_key=unit_key, all_psth=params['beh_analysis']['psth_Hz'][event_type],
                  psth_name=fig_name, line_color=event_color, bar_mode=False)
        plot_raster(params=params, unit_key=unit_key, all_raster=params['beh_analysis']['raster'][event_type],
                    raster_name=fig_name, marker_color=event_color)

    return params

def sq_psth_analysis(params, debug_mode=False):

    p = params['paths']['ephys_dat']
    params['ephys']['t'] = creates_time_vector(filepath=p, total_ch_number=27)
    params['behavior'] = {}
    params['behavior']['lick_ch0'] = get_licks(params=params, ch_str='ADC2', cleaning_th=2, debug_mode=False)
    params['behavior']['lick_ch1'] = get_licks(params=params, ch_str='ADC3', cleaning_th=2, debug_mode=False)
    params['behavior']['lick_ch0']['display_color'] = np.array([47, 134, 166])/255
    params['behavior']['lick_ch1']['display_color'] = np.array([52, 190, 130])/255

    TRACE_COLOR = (0.5, 0.5, 0.5)

    params['beh_analysis']['psth'] = {}
    params['beh_analysis']['psth']['lick_ch0'] = {}
    params['beh_analysis']['psth']['lick_ch1'] = {}
    params['beh_analysis']['raster'] = {}
    params['beh_analysis']['raster']['lick_ch0'] = {}
    params['beh_analysis']['raster']['lick_ch1'] = {}

    params['ephys']['sfreq'] = 30000
    params['beh_analysis']['psth_Hz'] = {}
    params['beh_analysis']['psth_Hz']['params'] = {'before_sec': 10, 'after_sec': 10, 'bin_size_sec': 0.05}

    for k, u in params['ephys']['units'].items():

        if debug_mode:
            plot_rawtrace_withevents(params=params, unit_key=k, sfreq=params['ephys']['sfreq'],
                                     before_sec=params['beh_analysis']['psth_Hz']['params']['before_sec'],
                                     after_sec=params['beh_analysis']['psth_Hz']['params']['after_sec'],
                                     ax=None)

        event_idx = params['behavior']['lick_ch0']['idx']
        tmp = build_psth(params=params, unit_key=k, event_idx=event_idx, sfreq=params['ephys']['sfreq'],
                         before_sec=params['beh_analysis']['psth_Hz']['params']['before_sec'],
                         after_sec=params['beh_analysis']['psth_Hz']['params']['after_sec'],
                         bin_size_sec=params['beh_analysis']['psth_Hz']['params']['bin_size_sec'])
        params['beh_analysis']['psth']['lick_ch0'][k] = tmp['psth_Hz']
        params['beh_analysis']['raster']['lick_ch0'][k] = tmp['raster']
        event_idx = params['behavior']['lick_ch1']['idx']
        tmp = build_psth(params=params, unit_key=k, event_idx=event_idx, sfreq=params['ephys']['sfreq'],
                         before_sec=params['beh_analysis']['psth_Hz']['params']['before_sec'],
                         after_sec=params['beh_analysis']['psth_Hz']['params']['after_sec'],
                         bin_size_sec=params['beh_analysis']['psth_Hz']['params']['bin_size_sec'])
        params['beh_analysis']['psth']['lick_ch1'][k] = tmp['psth_Hz']
        params['beh_analysis']['raster']['lick_ch1'][k] = tmp['raster']
        all_psth = params['beh_analysis']['psth']['lick_ch0']
        all_raster = params['beh_analysis']['raster']['lick_ch0']
        plot_psth(params=params, unit_key=k, all_psth=all_psth, psth_name='quinine', line_color=params['behavior']['lick_ch0']['display_color'])
        plot_raster(params=params, unit_key=k, all_raster=all_raster, raster_name='quinine', marker_color=params['behavior']['lick_ch0']['display_color'])
        all_psth = params['beh_analysis']['psth']['lick_ch1']
        all_raster = params['beh_analysis']['raster']['lick_ch1']
        plot_psth(params=params, unit_key=k, all_psth=all_psth, psth_name='sucrose', line_color=params['behavior']['lick_ch1']['display_color'])
        plot_raster(params=params, unit_key=k, all_raster=all_raster, raster_name='sucrose', marker_color=params['behavior']['lick_ch1']['display_color'])

        #print('I am here to set a breakpoint during the debug')

    return params

def psth_population_analysis(params, use_zscore=1, pickle_name=None, show_individual_population_plots=False, peth_df=None):

    psth_db = params['beh_analysis']['psth_Hz']

    params['beh_analysis']['pop_PETH_Hz']={}

    for zone_name, all_psth in psth_db.items():

        if zone_name is not 'params':

            n_units = len(all_psth)
            n_bins = np.shape(all_psth[0])[1]

            heat_map = np.zeros((n_units, n_bins))
            for l, (unit, psth) in enumerate(all_psth.items()):
                heat_map[l,:] =np.nanmean(psth, axis=0)

            if use_zscore:
                # TODO: change baseline selection var out of the function
                baseline = heat_map[:,:9]
                mean_ = np.mean(baseline, axis=1)
                std_ = np.std(baseline, axis=1)
                zscore_ = (heat_map.T - mean_).T
                heat_map = (zscore_.T / std_).T

            if show_individual_population_plots:
                fig, axs = plt.subplots(2,1)
                fig.suptitle(zone_name)
                im_handle = axs[0].imshow(heat_map)
                plt.colorbar(im_handle)
                axs[1].plot(np.mean(heat_map, axis=0))
                plt.show()

            heat_map_max = np.max(heat_map, axis=1)
            heat_map_min = np.min(heat_map, axis=1)
            ehm = heat_map_max > 2
            ihm = heat_map_min < -2
            eohm = np.logical_and(~ihm, ehm)
            iohm = np.logical_and(ihm, ~ehm)
            eihm = np.logical_and(ihm, ehm)
            rhm = np.logical_and(~ihm, ~ehm)


            eunit = np.ones(n_units,)
            i = 0
            for unit_key, unit_values in params['ephys']['units'].items():
                if (unit_values['convolved_fr_Hz'] > 15) and (unit_values['peak2valley_time_us'] < 450):
                    eunit[i]=0
                i+=1

            vmin = np.min(heat_map_min)
            vmax = np.max(heat_map_max)

            f, axs = plt.subplots(2, 4)
            axs[0,0].set_title('e only')
            im1 = axs[0,0].imshow(heat_map[eohm, :], vmin=vmin, vmax=vmax)
            axs[0,1].set_title('i only')
            im2 = axs[0,1].imshow(heat_map[iohm, :], vmin=vmin, vmax=vmax)
            axs[0,2].set_title('e and i')
            im3 = axs[0,2].imshow(heat_map[eihm, :], vmin=vmin, vmax=vmax)
            axs[0,3].set_title('rien')
            im4 = axs[0,3].imshow(heat_map[rhm, :], vmin=vmin, vmax=vmax)


            # auc = np.sum(heat_map, axis=1)
            # ihm = heat_map[auc < 0, :]
            # ehm = heat_map[auc > 0, :]

            params['beh_analysis']['pop_PETH_Hz'][zone_name]={'iohm': heat_map[iohm, :], 'eohm': heat_map[eohm, :], 'eihm': heat_map[eihm, :],  'rhm': heat_map[rhm, :]}
            params['ephys']['excitatory_units'] = eunit
            params['beh_analysis'][zone_name] = {}
            params['beh_analysis'][zone_name]['unit_response_type'] = np.zeros((n_units))
            params['beh_analysis'][zone_name]['unit_response_type'] += np.int16(iohm)
            params['beh_analysis'][zone_name]['unit_response_type'] += np.int16(eohm)*2
            params['beh_analysis'][zone_name]['unit_response_type'] += np.int16(eihm)*3
            params['beh_analysis'][zone_name]['unit_response_type'] += np.int16(rhm)*4

            if show_individual_population_plots:
                plot_heatmap(heat_map[iohm, :], zone_name, hm_name='iohm', vmin=vmin, vmax=vmax, i1=None, i2=None, pickle_name=pickle_name)
                plot_heatmap(heat_map[eohm, :], zone_name, hm_name='eohm', vmin=vmin, vmax=vmax, i1=None, i2=None, pickle_name=pickle_name)
                plot_heatmap(heat_map[eihm, :], zone_name, hm_name='eihm', vmin=vmin, vmax=vmax, i1=None, i2=None, pickle_name=pickle_name)
                plot_heatmap(heat_map[rhm, :], zone_name, hm_name='rhm', vmin=vmin, vmax=vmax, i1=None, i2=None, pickle_name=pickle_name)

    return params

def plot_heatmap(hm, zone_name, hm_name, vmin=-8, vmax=6, i1=None, i2=None, pickle_name=None):

    if i1 is None:
        i1=0
    if i2 is None:
        i2 = hm.shape[1]

    auc = np.sum(hm[:, i1:i2], axis=1)
    idx = np.argsort(auc)
    hm = hm[idx, :]

    fig, (ax0, ax1) = plt.subplots(1, 2)
    fig.suptitle(zone_name + ' ' + hm_name)
    h0 = ax0.imshow(hm, vmin=vmin, vmax=vmax)
    plt.colorbar(h0)
    mu = np.mean(hm, axis=0)
    sigma = np.std(hm, axis=0)
    x = np.arange(hm.shape[1])
    ax1.plot(x,mu, color='grey')
    ax1.fill_between(x,mu + sigma, mu - sigma, facecolor='grey', alpha=0.1)
    plt.savefig(pickle_name.parent / f"{zone_name}-{hm_name}.svg")
    plt.close(fig)

def analyse_onefile(filename, show_all_plots=False, show_individual_population_plots=False, alternative_rawdata_folder = None, sampling_frequency=30000):
    infile = open(filename, 'rb')
    params = pickle.load(infile)
    infile.close()
    params = epm_psth_analysis(params=params, show_individual_plots=show_all_plots, sampling_frequency=sampling_frequency, alternative_rawdata_folder=alternative_rawdata_folder)
    params = psth_population_analysis(params, use_zscore=1, pickle_name=filename, show_individual_population_plots=show_individual_population_plots)
    return params

if __name__ == '__main__':

    # Script to analyse on pickle and build heatmaps of spiking activity around  entries and  exits of open arms
    # filename = Path(r"Y:\Ephys in vivo\02 - ANALYZED DATA\F2493\20230414\F2493_EPM_data.pickle")
    # show_all_plots = False
    # show_individual_population_plots = False
    # params = analyse_onefile(filename, show_all_plots=show_all_plots, show_individual_population_plots=show_individual_population_plots)
    # plt.show()

    analysis_folder = Path(r'Y:\Ephys_in_vivo\02_ANALYSIS\2_In_Nphy')
    all_PETH_pickle_path = analysis_folder / r'_OUTPUT\all_PETH.pickle2'
    rawdata_folder = Path(r'Y:\Ephys_in_vivo\01_RAW_DATA\2_In_Nphy')



    if not False: #all_PETH_pickle_path.exists():

        from pathlib import Path
        show_all_plots = False
        show_individual_population_plots = False
        data_list = analysis_folder.rglob('*.pickle')

        all_PETH = {}
        all_putative_type = [0]
        all_unit_response_type = {'enterzone':[0], 'exitzone':[0]}

        prevunits = 0
        for data_path in data_list:
            n = data_path.name

            if 'EPM' in n:
                print(n)
                try:
                    params = analyse_onefile(data_path, show_all_plots=show_all_plots,
                                             show_individual_population_plots=show_individual_population_plots,
                                             sampling_frequency=30000,
                                             alternative_rawdata_folder=rawdata_folder)

                    if len(all_PETH)==0:
                        p = params['beh_analysis']['pop_PETH_Hz']
                        for k, v in p.items():
                            iohm = params['beh_analysis']['pop_PETH_Hz'][k]['iohm']
                            n_columns = iohm.shape[1]
                            all_PETH[k]={'eohm': {'ex': np.zeros((1, n_columns)), 'inh': np.zeros((1, n_columns))}, 'iohm': {'ex': np.zeros((1, n_columns)), 'inh': np.zeros((1, n_columns))}, 'eihm':{'ex': np.zeros((1, n_columns)), 'inh': np.zeros((1, n_columns))}, 'rhm': {'ex': np.zeros((1, n_columns)), 'inh': np.zeros((1, n_columns))}}

                    p = params['beh_analysis']['pop_PETH_Hz']
                    all_putative_type = np.hstack((all_putative_type, params['ephys']['excitatory_units']))

                    unitLabel = np.loadtxt(r'Y:\Ephys in vivo\02 - ANALYZED DATA\test.txt', delimiter=',')

                    for k, v in p.items():
                        idx = len(params['ephys']['units'])

                        typeLabel = unitLabel[prevunits:prevunits+idx+1]
                        prevunits = prevunits + idx + 1
                        if k == 'enterzone':
                            c = 1
                        else:
                            c = 2

                        count2 = [0,0,0,0]
                        all_unit_response_type[k] = np.hstack(
                            (all_unit_response_type[k], params['beh_analysis'][k]['unit_response_type']))
                        for i in range(typeLabel.shape[0]):
                            num = unitLabel[i][:][:]
                            print(num)
                            type1unit = num[0]
                            type2unit = num[c]

                            if type1unit == 0:
                                typestr = 'ex'
                            else:
                                typestr='inh'

                            if type2unit == 2:
                                all_PETH[k]['eohm'][typestr] = np.vstack((all_PETH[k]['eohm'][typestr], params['beh_analysis']['pop_PETH_Hz'][k]['eohm'][count2[1]][:]))
                            elif type2unit == 1:
                                all_PETH[k]['iohm'][typestr] = np.vstack((all_PETH[k]['iohm'][typestr], params['beh_analysis']['pop_PETH_Hz'][k]['iohm'][count2[0]][:]))
                            elif type2unit == 3:
                                all_PETH[k]['eihm'][typestr] = np.vstack((all_PETH[k]['eihm'][typestr], params['beh_analysis']['pop_PETH_Hz'][k]['eihm'][count2[2]][:]))
                            else:
                                all_PETH[k]['rhm'][typestr] = np.vstack((all_PETH[k]['rhm'][typestr], params['beh_analysis']['pop_PETH_Hz'][k]['rhm'][count2[3]][:]))

                            count2[int(type2unit) - 1] = count2[int(type2unit) - 1] + 1
                            # print('done')

                        print('mouse co')



                except:
                    print(f'problem with {n}')

        with open(all_PETH_pickle_path, 'wb') as handle:
            pickle.dump({'PETH': all_PETH, 'putative_type': all_putative_type, 'unit_response_type': all_unit_response_type}, handle, protocol=pickle.HIGHEST_PROTOCOL)

    else:
        with open(all_PETH_pickle_path, 'rb') as handle:
            all_ = pickle.load(handle)
            all_PETH = all_['PETH']


        for k,v in all_.items():
            if k == 'PETH':
                for k2,v2 in v.items():
                    for k3,v3 in v2.items():
                        for k4, v4 in v3.items():
                            all_[k][k2][k3][k4] = all_[k][k2][k3][k4][1:, :]
            elif k=='putative_type':
                all_[k] = all_[k][1:]
            else:
                for k2, v2 in v.items():
                    all_[k][k2]=all_[k][k2][1:]


        m = np.vstack((all_['putative_type'],all_['unit_response_type']['enterzone'], all_['unit_response_type']['exitzone']))

        np.savetxt(r'Y:\Ephys in vivo\02 - ANALYZED DATA\test.txt',m)
        import pandas as pd
        pd.DataFrame(m.T).to_csv(r'Y:\Ephys in vivo\02 - ANALYZED DATA\test.txt', index=False)


        zone_name = 'enterzone'
        hm_name = 'eohm'

        hm =all_PETH[zone_name][hm_name]
        auc = np.sum(hm, axis=1)

        # pour classer les units en fonction de leur reponse (eg. AUC)
        # idx = np.argsort(auc)
        # hm = hm[idx, :]

        fig, (ax0, ax1) = plt.subplots(1, 2)
        fig.suptitle(zone_name + ' ' + hm_name)
        h0 = ax0.imshow(hm, aspect=.2, vmin=-8, vmax=6) #, vmin=vmin, vmax=vmax)
        plt.colorbar(h0)
        mu = np.mean(hm, axis=0)
        sigma = np.std(hm, axis=0) / np.sqrt(hm.shape[0])
        x = np.arange(hm.shape[1])
        ax1.plot(x,mu, color='grey')
        ax1.fill_between(x,mu + sigma, mu - sigma, facecolor='grey', alpha=0.1)
        plt.savefig(all_PETH_pickle_path.parent / f"{zone_name}-{hm_name}.svg")

    plt.show()


