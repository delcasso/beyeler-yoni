import pickle
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import pprint
import pickle as pkl
import os
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from scipy.ndimage import gaussian_filter1d
from scipy.stats import sem


# import scipy.stats as stats


def convertBinSize(neural, oldbinsize, newbinsize):
    ratio = int(newbinsize / oldbinsize)  # we'll sum these number of bins into 1 bin
    num_bins = int(neural.shape[1] / ratio)
    neural_new = np.zeros((neural.shape[0], num_bins, neural.shape[-1]))
    c = 0
    for i in range(neural.shape[0]):
        for j in range(num_bins):
            # plt.plot(neural[j,c:c+ratio,:])
            # plt.show()
            # plt.plot(np.sum(neural[j,c:c+ratio,:].squeeze(), axis = 0))
            # plt.show()
            neural_new[i, j, :] = np.sum(neural[i, c:c + ratio, :].squeeze(), axis=0)
            c += ratio
    plt.plot(neural_new[0, :, 0])
    return neural_new


def open_onefile(filename, show_all_plots=False, show_individual_population_plots=False):
    infile = open(filename, 'rb')
    params = pickle.load(infile)
    infile.close()
    return params


def fix_paths(params, analysis_type):
    # code to update the paths in params dictionary
    p = params['paths']
    mousename = params['mouse']['name']
    # print(mousename)

    for i in p:
        #print(i)
        #print(p[i])
        newpath = ""
        stringx = str(p[i])
        pathog = stringx.split(mousename)[0]
        if len(stringx.split(mousename))>2:
            restsplit = stringx.split(mousename)[1]+mousename+stringx.split(mousename)[-1]
        else:
            restsplit = stringx.split(mousename)[-1]
        split = os.path.join(analysis_type, mousename + restsplit)
        if 'ANALYZED' in pathog:
            newpath = Path(os.path.join("Y:\Ephys_in_vivo\\02_ANALYSIS", split))
        if 'RAW' in pathog:
            newpath = Path(os.path.join("Y:\Ephys_in_vivo\\01_RAW_DATA", split))
        else:
            newpath = p[i]
        #print(newpath)
        p[i] = newpath

    # adding DLC data path to the params
    exp_subtype = params['session']['task']
    # print(exp_subtype)
    if 'EPM' in exp_subtype:
        p['dlc'] = os.path.join(r"S:\_Tanmai\Python Scripts\DLC\ePhys_in_vivo_demo-tdhanireddy-2023-08-02\videos",
                                mousename + "_EPMDLC_resnet50_ePhys_in_vivo_demoAug2shuffle1_100000_filtered.csv")
        date = params['session']['date']
        # print(date)
        p['cebra_mousedata'] = os.path.join(r"S:\_Tanmai\Python Scripts\ePhysData_code\mouseDataNew",
                                            mousename + "_" + date + "_neuralBehdataCorrected_ALLIDX.npz")

    params['paths'] = p
    return params


def swap_licks(paramas):
    # swap licks if needed
    lick0 = params['beh_analysis']['psth']['lick_ch0'].copy()
    lick1 = params['beh_analysis']['psth']['lick_ch1'].copy()
    params['beh_analysis']['psth']['lick_ch0'] = lick1
    params['beh_analysis']['psth']['lick_ch1'] = lick0
    beh_0 = params['behavior']['lick_ch0'].copy()
    beh_1 = params['behavior']['lick_ch1'].copy()
    params['behavior']['lick_ch0'] = beh_1
    params['behavior']['lick_ch1'] = beh_0
    raster0 = params['beh_analysis']['raster']['lick_ch0'].copy() # ?
    raster1 = params['beh_analysis']['raster']['lick_ch1'].copy() # ?
    params['beh_analysis']['raster']['ch0'] = raster1
    params['beh_analysis']['raster']['ch1'] = raster0
    #print(params['beh_analysis']['raster'])
    #print(params['behavior'])
    #print(params['beh_analysis']['psth_Hz'])
    return params


def EPM_CheckHeadDips(params, savedatadir, mousename, date):
    # ONLY FOR EPM
    # head dip is defined when the mouse is outside the EPM from the open arm for more than 500ms
    binwidth = round(1 / params['video']['fr'], 6)  # 50ms, arbitrary fixed
    # print(params['video']['fr'])
    # print(binwidth)
    params['dlc_data']['binwidth'] = binwidth
    threshold_time = 0.500  # ms
    threshold = np.ceil(threshold_time / binwidth)  # 500 ms == roughly 10 data points
    l_beh = np.array(params['dlc_data']['behMatrix3D'])
    X = l_beh[:, 1]
    Y = l_beh[:, 2]
    IDX = l_beh[:, 0]
    counter = 0
    st = 0
    ed = 0
    countarr = []
    headdipidx = -1
    # print(np.unique(params['dlc_data']['behMatrix3D'][:,0]))

    for i in range(len(IDX)):
        if counter == 0:
            # if IDX[i] == headdipidx and (IDX[i-1] == 0 or IDX[i-1] == 1): # checking if the current idx is outside and the prev idx is OA
            if IDX[i] == headdipidx:
                counter = counter + 1
                st = i

        if counter > 0:
            if IDX[i] == headdipidx:  # counter = 1 happened during the change from OA to outside
                counter += 1

        if IDX[i] != headdipidx and IDX[i - 1] == headdipidx:  # point of change
            countarr.append(counter)
            if counter >= threshold:
                # print('above thresh')
                ed = i
                counter = 0
                # change the idx of these to '-2'
                params['dlc_data']['behMatrix3D'][st:ed, 0] = headdipidx - 1
                params['dlc_data']['behMatrix2D'][st:ed, 0] = headdipidx - 1
                # print('yayyy')

                # print([st, ed])
                st = 0
                ed = 0
            else:
                counter = 0
                # print('oopsie')
                # print([st, i])
                # print(params['dlc_data']['behMatrix3D'][st:i,0])

    # print(np.unique(params['dlc_data']['behMatrix3D'][:,0]))
    # print((params['dlc_data']['behMatrix3D'][17948:17953, 0]))
    # sanity check : did we even have headdips?

    # print(countarr)
    countarr = np.array(countarr) * binwidth  # get it in sec
    # print(countarr)
    # print(countarr.shape)
    binvec = np.arange(min(countarr), max(countarr) + binwidth, binwidth)

    folderpath = os.path.join(savedatadir, "HeaddipInfo")
    if not os.path.exists(folderpath):
        os.mkdir(folderpath)

    plt.hist(countarr, bins=binvec)
    plt.ylabel('event/headdip num')
    plt.xlabel('Time (s)')
    plt.savefig(folderpath + "\\" + mousename + "_" + date + "_" + "headdipdurations.png")
    # plt.show()
    plt.close()

    # boolEXISTheaddips = params['dlc_data']['behMatrix3D'][:,0] == headdipidx-1 # - 1 is headdips, -2 is headdips that we consider
    boolEXISTheaddips = countarr >= 0.5
    plt.hist(boolEXISTheaddips * 1)
    plt.title('Headdips discarded and selected')
    plt.ylabel('event/headdip num')
    plt.xlabel('Discarded // Selected')
    plt.savefig(folderpath + "\\" + mousename + "_" + date + "_" + "headdipSelection.png")
    # plt.show()
    plt.close()
    return params, binwidth


def add_behaviour(params, savedatadir, mousename, date):
    exp_subtype = params['session']['task']
    binwidth = 0
    if 'EPM' in exp_subtype:
        params['dlc_data'] = {}
        if os.path.exists(params['paths']['cebra_mousedata']):
            with np.load(params['paths']['cebra_mousedata']) as f:
                params['dlc_data']['neural'] = (f['l_data']).T  # (time,neurons)
                # print(f['l_data'].shape)
                params['dlc_data']['behMatrix3D'] = (f['l_beh']).T  # (time, (idx, x, y))
                # print(f['l_beh'].shape)
                params['dlc_data']['behMatrix2D'] = (f['l_beh_lowdim']).T
                # print(f['l_beh_lowdim'].shape)
                # print(params['dlc_data']['behMatrix2D'].shape)
                params['dlc_data']['checker_xopen'] = f['l_x_open']
                # print(len(f['l_x_open']))
                # print((f['l_x_open']))
                params['dlc_data']['checker_yopen'] = f['l_y_open']
                # print(params['dlc_data']['checker_yopen'])

            # now to define head dips
            params, binwidth = EPM_CheckHeadDips(params, savedatadir, mousename, date)
        else:
            return 0, binwidth

    return params, binwidth


def EPM_plot1type_allneur(neural, unitnums, name):
    print(neural.shape)
    neural_avg = gaussian_filter1d(np.nanmean(neural, axis=0), sigma=6)
    neural_stdd = gaussian_filter1d(sem(neural, axis=0, nan_policy='omit'), sigma=6)
    x = int(np.ceil(unitnums / 5))
    y = 5
    fig, ax = plt.subplots(x, y)
    ax = ax.flatten()
    ax = ax.flatten()
    for i in range(unitnums):
        ax[i].plot(neural_avg[:, i], c='k')
        ax[i].plot(neural_avg[:, i] - neural_stdd[:, i], c='grey')
        ax[i].plot(neural_avg[:, i] + neural_stdd[:, i], c='grey')
    plt.title(name)
    plt.savefig(
        'PSTHdata\\EPM_allneurPSTH\\' + name + "_" + params['mouse']['name'] + params['session']['date'] + '.png')
    plt.show()


def plotallunits(params):
    neural = np.array(params['dlc_data']['OA']['entry']['all_neur'])
    unitnums = neural.shape[-1]
    EPM_plot1type_allneur(neural, unitnums, name='OA_Entry')
    EPM_plot1type_allneur(params['dlc_data']['OA']['exit']['all_neur'], unitnums, name='OA_Exit')
    EPM_plot1type_allneur(params['dlc_data']['CA']['entry']['all_neur'], unitnums, name='CA_Entry')
    EPM_plot1type_allneur(params['dlc_data']['CA']['exit']['all_neur'], unitnums, name='CA_Exit')
    EPM_plot1type_allneur(params['dlc_data']['headDips']['all_neur'], unitnums, name='Headdips')


def notcentre(idx):
    if idx != 4 and idx != 5 and idx != 6 and idx != 7:
        return True
    else:
        return False


def centre(idx):
    if idx == 4 or idx == 5 or idx == 6 or idx == 7:
        return True
    else:
        return False


def selectEventIDX(idx, c, binwidth, timelength, threshold_time):
    # idx is the raw array with all idx positions of mouse
    # c is the point around which we select our code - it is the arr idx when the mouse just enters/exit
    # so we consider c and c-1
    arr_idx = np.zeros(2)  # [st ed]
    # threshold_time = 5 # sec
    threshold = int(np.ceil(threshold_time / binwidth))  # on both positive and negative timescale
    # print(threshold)
    prev_idx = idx[c - 1]

    post_idx = idx[c]
    # print(post_idx)

    # print(idx[c+threshold-1])
    # either they're equal to each other or they're all in the centre
    # print(notcentre(7))
    # print(prev_idx)
    # print(notcentre(5))
    # print(idx[c - threshold])
    # print(((not notcentre(prev_idx)) and (not notcentre(idx[c-threshold]))))
    '''
    if c-threshold>=0 and c+threshold-1<timelength:
        if (idx[c-threshold] == prev_idx or (not notcentre(prev_idx) and not notcentre(idx[c-threshold]))) and (idx[c+threshold-1] == post_idx or (not notcentre(post_idx) and not notcentre(idx[c+threshold-1]))):
            arr_idx[0] = int(c-threshold)
            arr_idx[1] = int(c+threshold-1)

    # NOT ADDING ANY EVENT THRESHOLD CONDITION BECAUSE OF THE LACK OF EVENTS OTHERWISE
    '''
    if c - threshold >= 0 and c + threshold - 1 < timelength:
        arr_idx[0] = int(c - threshold)
        arr_idx[1] = int(c + threshold - 1)

    return arr_idx


def makeHeatMap(params, timethreshold=10):
    # -10 to +10sec
    # need this for entry and exit CA, entry and exit OA, head dips - tracking the snout
    # print(params['dlc_data'])
    params['dlc_data']['OA'] = {}
    params['dlc_data']['CA'] = {}
    params['dlc_data']['headDips'] = {}
    params['dlc_data']['OA']['entry'] = {}
    params['dlc_data']['OA']['entry']['all_neur'] = []
    params['dlc_data']['OA']['entry']['beh'] = []
    params['dlc_data']['OA']['exit'] = {}
    params['dlc_data']['OA']['exit']['all_neur'] = []
    params['dlc_data']['OA']['exit']['beh'] = []
    params['dlc_data']['CA']['entry'] = {}
    params['dlc_data']['CA']['entry']['all_neur'] = []
    params['dlc_data']['CA']['entry']['beh'] = []
    params['dlc_data']['CA']['exit'] = {}
    params['dlc_data']['CA']['exit']['all_neur'] = []
    params['dlc_data']['CA']['exit']['beh'] = []
    params['dlc_data']['headDips']['all_neur'] = []
    params['dlc_data']['headDips']['beh'] = []

    binwidth = params['dlc_data']['binwidth']
    l_beh = np.array(params['dlc_data']['behMatrix3D'])
    l_data = np.array(params['dlc_data']['neural'])
    X = l_beh[:, 1]
    Y = l_beh[:, 2]
    IDX = l_beh[:, 0]
    counter = 0
    idx_prev = 0
    st = 0
    ed = 0
    timelength = int(l_data.shape[0])
    # print(np.unique(IDX))
    for i in range(len(IDX)):
        # check for entry, exit and headdips
        if i == 0:
            idx_prev = IDX[i]
        else:
            if IDX[i] != idx_prev and (notcentre(idx_prev) or notcentre(IDX[i])):
                # counter+=1
                # here it's some sort of critical point - we check what type
                # print(idx_prev)
                if centre(idx_prev):  # entry
                    if IDX[i] == 0 or IDX[i] == 1:  # OA
                        arr_idx = selectEventIDX(IDX, i, binwidth, timelength, timethreshold)
                        # print(arr_idx)
                        if np.sum(arr_idx) != 0:
                            st = int(arr_idx[0])
                            ed = int(arr_idx[1])
                            params['dlc_data']['OA']['entry']['all_neur'].append(l_data[st:ed, :])
                            params['dlc_data']['OA']['entry']['beh'].append(l_beh[st:ed, :])

                    if IDX[i] == 2 or IDX[i] == 3:  # CA
                        arr_idx = selectEventIDX(IDX, i, binwidth, timelength, timethreshold)
                        # print(l_data.shape)
                        # print(arr_idx)
                        if np.sum(arr_idx) != 0:
                            st = int(arr_idx[0])
                            ed = int(arr_idx[1])
                            params['dlc_data']['CA']['entry']['all_neur'].append(l_data[st:ed, :])
                            params['dlc_data']['CA']['entry']['beh'].append(l_beh[st:ed, :])
                elif IDX[i] == -2:  # headdips
                    # print('headdip')
                    arr_idx = selectEventIDX(IDX, i, binwidth, timelength, timethreshold)
                    # print(arr_idx)
                    if np.sum(arr_idx) != 0:
                        st = int(arr_idx[0])
                        ed = int(arr_idx[1])
                        params['dlc_data']['headDips']['all_neur'].append(l_data[st:ed, :])
                        params['dlc_data']['headDips']['beh'].append(l_beh[st:ed, :])

                else:
                    # print('exit')
                    # print(IDX[i])
                    if idx_prev == 0 or idx_prev == 1:  # OA
                        # print('oa exit')
                        arr_idx = selectEventIDX(IDX, i, binwidth, timelength, timethreshold)
                        # print(arr_idx)
                        if np.sum(arr_idx) != 0:
                            st = int(arr_idx[0])
                            ed = int(arr_idx[1])
                            params['dlc_data']['OA']['exit']['all_neur'].append(l_data[st:ed, :])
                            params['dlc_data']['OA']['exit']['beh'].append(l_beh[st:ed, :])
                    if idx_prev == 2 or idx_prev == 3:  # CA
                        # print('ca exit')
                        arr_idx = selectEventIDX(IDX, i, binwidth, timelength, timethreshold)
                        # print(arr_idx)
                        if np.sum(arr_idx) != 0:
                            st = int(arr_idx[0])
                            ed = int(arr_idx[1])
                            params['dlc_data']['CA']['exit']['all_neur'].append(l_data[st:ed, :])
                            params['dlc_data']['CA']['exit']['beh'].append(l_beh[st:ed, :])
                idx_prev = IDX[i]

    # convert bin size:
    params['dlc_data']['OA']['entry']['all_neur_50ms'] = np.array(params['dlc_data']['OA']['entry']['all_neur']).copy()
    params['dlc_data']['OA']['entry']['all_neur'] = convertBinSize(params['dlc_data']['OA']['entry']['all_neur_50ms'],
                                                                   np.round(1 / params['video']['fr'], 6),
                                                                   np.round(1 / params['video']['fr'], 6) * 10)

    params['dlc_data']['OA']['exit']['all_neur_50ms'] = np.array(params['dlc_data']['OA']['exit']['all_neur']).copy()

    params['dlc_data']['OA']['exit']['all_neur'] = convertBinSize(params['dlc_data']['OA']['exit']['all_neur_50ms'],
                                                                  np.round(1 / params['video']['fr'], 6),
                                                                  np.round(1 / params['video']['fr'], 6) * 10)
    params['dlc_data']['CA']['exit']['all_neur_50ms'] = np.array(params['dlc_data']['CA']['exit']['all_neur']).copy()
    params['dlc_data']['CA']['exit']['all_neur'] = convertBinSize(params['dlc_data']['CA']['exit']['all_neur_50ms'],
                                                                  np.round(1 / params['video']['fr'], 6),
                                                                  np.round(1 / params['video']['fr'], 6) * 10)
    params['dlc_data']['CA']['entry']['all_neur_50ms'] = np.array(params['dlc_data']['CA']['entry']['all_neur']).copy()
    params['dlc_data']['CA']['entry']['all_neur'] = convertBinSize(params['dlc_data']['CA']['entry']['all_neur_50ms'],
                                                                   np.round(1 / params['video']['fr'], 6),
                                                                   np.round(1 / params['video']['fr'], 6) * 10)
    params['dlc_data']['headDips']['all_neur_50ms'] = np.array(params['dlc_data']['headDips']['all_neur']).copy()
    params['dlc_data']['headDips']['all_neur'] = convertBinSize(params['dlc_data']['headDips']['all_neur_50ms'],
                                                                np.round(1 / params['video']['fr'], 6),
                                                                np.round(1 / params['video']['fr'], 6) * 10)
    return params


def PCA_analysis(l_data_PCA):
    k = 3  # nb of dimensions
    l_projected_data = []

    data_mat = l_data_PCA.T
    cov_mat = np.cov(data_mat)
    eigval, eigvec = np.linalg.eig(cov_mat)
    idx = eigval.argsort()[:-1]
    eigvec_sorted = eigvec[:, idx]
    PC = eigvec_sorted[:, :k]
    projected_data = np.matmul((np.transpose(data_mat)), PC)
    l_projected_data = (projected_data)
    print("Dimension of projected data:")
    print(projected_data.shape)

    return l_projected_data


def plotPCAtrialNeur(neural, timearr, ax):
    avg_plotneur = np.mean(neural, axis=0)  # (TIME, UNITS)
    avg_plotneur = StandardScaler().fit_transform(avg_plotneur)
    pca = PCA(n_components=3, whiten=True)
    # print(avg_plotneur.shape)
    pcafit = pca.fit_transform(avg_plotneur)

    # pcadata = pca.singular_values_
    centre = 0
    ax.axvline(x=centre, color='grey')
    gaussfiltpca = np.zeros(pcafit.shape)
    gaussfiltpca[:, 0] = gaussian_filter1d(pcafit[:, 0], sigma=3)
    gaussfiltpca[:, 1] = gaussian_filter1d(pcafit[:, 1], sigma=3)
    gaussfiltpca[:, 2] = gaussian_filter1d(pcafit[:, 2], sigma=3)

    pca_own = PCA_analysis(avg_plotneur)

    gaussfiltpca[:, 0] = gaussian_filter1d(pca_own[:, 0], sigma=3)
    gaussfiltpca[:, 1] = gaussian_filter1d(pca_own[:, 1], sigma=3)
    gaussfiltpca[:, 2] = gaussian_filter1d(pca_own[:, 2], sigma=3)

    ax.plot(timearr, gaussfiltpca[:, 0], color='b', label='PC1')
    ax.plot(timearr, gaussfiltpca[:, 1], color='g', label='PC2')
    ax.plot(timearr, gaussfiltpca[:, 2], color='r', label='PC3')
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05),
              ncol=3, fancybox=True)
    return avg_plotneur, gaussfiltpca


def plotavgtrialNeur(neural, timearr, ax):
    avg_plotneur = np.mean(neural, axis=0)
    avgunit_plotneur = np.mean(avg_plotneur, axis=1)
    centre = 0
    ax.axvline(x=centre, color='grey')
    ax.plot(timearr, avgunit_plotneur)
    stddev_plotneur = sem(avg_plotneur, axis=1, nan_policy='omit')
    ax.plot(timearr, avgunit_plotneur - stddev_plotneur, color='dimgrey')
    ax.plot(timearr, avgunit_plotneur + stddev_plotneur, color='dimgrey')


def plotall(neural, timearr, ax):
    avg_plotneur = np.mean(neural, axis=0)
    for i in range(avg_plotneur.shape[-1]):  # plot each unit
        ax.plot(timearr, avg_plotneur[:, i], c='lightgrey', lw=0.2)


def checkunittype(heat_map, t):
    ehm = np.zeros(heat_map.shape[-1])
    ihm = np.zeros(heat_map.shape[-1])
    rhm = np.zeros(heat_map.shape[-1])

    thr =20  # 1 sec
    print(heat_map)
    for i in range(ehm.size):
        hmm_exc = np.array(np.where(heat_map[t:t + 60, i] > 1.96)).squeeze()
        # print(hmm_exc)
        hmm_inh = np.array(np.where(heat_map[t:t + 60, i] < -1.65)).squeeze()  # asymmetric double sided z score
        # print(hmm_exc.size)
        if hmm_exc.size >= thr:
            ehm[i] = 1
        if hmm_inh.size >= thr:
            ihm[i] = 1
        if hmm_exc.size < thr and hmm_inh.size < thr:
            rhm[i] = 1

    ehm = ehm.astype(bool)
    ihm = ihm.astype(bool)
    rhm = rhm.astype(bool)
    return ehm, ihm, rhm


def EPM_plotSeparatedunits(params, timearr):
    list_things = ["eohm_OAEntry_zscore", "iohm_OAEntry_zscore", "rhm_OAEntry_zscore", "eohm_OAExit_zscore",
                   "iohm_OAExit_zscore", "rhm_OAExit_zscore", "eohm_CAEntry_zscore", "iohm_CAEntry_zscore",
                   "rhm_CAEntry_zscore", "eohm_CAExit_zscore", "iohm_CAExit_zscore", "rhm_CAExit_zscore",
                   "eohm_headdip_zscore", "iohm_headdip_zscore", "rhm_headdip_zscore"]
    fig, ax = plt.subplots(5, 3)
    c = 0
    ax = ax.flatten()
    ax = ax.flatten()
    # print(list_things)
    for i in range(len(list_things)):
        # print(i)
        print(params['beh_analysis']['psth'][list_things[i]].shape)
        meann = gaussian_filter1d(np.nanmean(params['beh_analysis']['psth'][list_things[i]], axis=1), sigma=3)
        ax[i].plot(timearr, meann, c='k')
        stdd = gaussian_filter1d(sem(params['beh_analysis']['psth'][list_things[i]], axis=1, nan_policy='omit'),
                                 sigma=3)
        ax[i].plot(timearr, meann - stdd, c='grey')
        ax[i].plot(timearr, meann + stdd, c='grey')
        if i % 3 == 2:
            c += 1
    plt.show()


def plotandcheckunittypes(params, binwidth, savedatadir, mousename, date):
    # we have an array of (time, neur) data
    # for each neuron we check the z score and everything
    # we do it for each type of behaviour

    params['beh_analysis']['psth'] = {}

    fig = plt.figure(figsize=(15, 5))
    ax1 = plt.subplot(231)
    ax2 = plt.subplot(232)
    ax3 = plt.subplot(233)
    ax4 = plt.subplot(234)
    ax5 = plt.subplot(235)

    avgplotsave = os.path.join(savedatadir, "trialunitavg_EPM")
    pcaplotsave = os.path.join(savedatadir, "PCAtrialavg_EPM")
    heatmapsplotsave = os.path.join(savedatadir, "Heatmapstrialavg_EPM")
    if not os.path.exists(avgplotsave):
        os.mkdir(avgplotsave)
    if not os.path.exists(pcaplotsave):
        os.mkdir(pcaplotsave)
    if not os.path.exists(heatmapsplotsave):
        os.mkdir(heatmapsplotsave)

    neural = np.array(params['dlc_data']['OA']['entry'][
                          'all_neur'])  # /binwidth # get fr instead of spikecounts  # all data (trials, time, units)
    # print(neural.size)
    if neural.size != 0:
        timelength = neural.shape[1] * binwidth
        timearr = np.linspace(-timelength / 2, timelength / 2, neural.shape[1])
        # plotall(neural,  timearr, ax1)
        plotavgtrialNeur(neural, timearr, ax1)
        ax1.set_title('OA Entry')
    neural = np.array(params['dlc_data']['OA']['exit']['all_neur'])  # /binwidth
    # print(neural.size)
    if neural.size != 0:
        timelength = neural.shape[1] * binwidth
        timearr = np.linspace(-timelength / 2, timelength / 2, neural.shape[1])
        # plotall(neural, timearr, ax2)
        plotavgtrialNeur(neural, timearr, ax2)
        ax2.set_title('OA Exit')
    neural = np.array(params['dlc_data']['CA']['entry']['all_neur'])  # /binwidth  # all data (trials, time, units)
    # print(neural.size)
    if neural.size != 0:
        timelength = neural.shape[1] * binwidth
        timearr = np.linspace(-timelength / 2, timelength / 2, neural.shape[1])
        # plotall(neural,  timearr, ax3)
        plotavgtrialNeur(neural, timearr, ax3)
        ax3.set_title('CA Entry')
    neural = np.array(params['dlc_data']['CA']['exit']['all_neur'])  # /binwidth  # all data (trials, time, units)
    # print(neural.shape)
    if neural.size != 0:
        timelength = neural.shape[1] * binwidth
        timearr = np.linspace(-timelength / 2, timelength / 2, neural.shape[1])
        # plotall(neural, timearr, ax4)
        plotavgtrialNeur(neural, timearr, ax4)
        ax4.set_title('CA Exit')
    neural = np.array(params['dlc_data']['headDips']['all_neur'])  # /binwidth  # all data (trials, time, units)
    # print(neural.size)
    if neural.size != 0:
        timelength = neural.shape[1] * binwidth
        timearr = np.linspace(-timelength / 2, timelength / 2, neural.shape[1])
        # plotall(neural, timearr, ax5)
        plotavgtrialNeur(neural, timearr, ax5)
        ax5.set_title('Head Dips')
    plt.savefig(avgplotsave + "\\" + mousename + "_" + date + ".png")
    # plt.show()
    plt.close()

    fig = plt.figure(figsize=(15, 10))
    ax1 = plt.subplot(231)
    ax2 = plt.subplot(232)
    ax3 = plt.subplot(233)
    ax4 = plt.subplot(234)
    ax5 = plt.subplot(235)

    neural = np.array(params['dlc_data']['OA']['entry']['all_neur']) / binwidth  # all data (trials, time, units)
    avg_plotneur_OAentry = np.array([])
    gaussfiltpca_OAentry = np.array([])
    avg_plotneur_OAexit = np.array([])
    gaussfiltpca_OAexit = np.array([])
    avg_plotneur_CAentry = np.array([])
    gaussfiltpca_CAentry = np.array([])
    avg_plotneur_CAexit = np.array([])
    gaussfiltpca_CAexit = np.array([])
    avg_plotneur_headdips = np.array([])
    gaussfiltpca_headdips = np.array([])

    # print(neural.size)
    if neural.size != 0:
        timelength = neural.shape[1] * binwidth
        timearr = np.linspace(-timelength / 2, timelength / 2, neural.shape[1])
        # plotall(neural, timearr, ax1)
        avg_plotneur_OAentry, gaussfiltpca_OAentry = plotPCAtrialNeur(neural, timearr, ax1)
        # plotstddev(neural, timearr, ax1)
        ax1.set_title('OA Entry')
    neural = np.array(params['dlc_data']['OA']['exit']['all_neur']) / binwidth
    # print(neural.size)
    if neural.size != 0:
        timelength = neural.shape[1] * binwidth
        timearr = np.linspace(-timelength / 2, timelength / 2, neural.shape[1])
        # plotall(neural, timearr, ax2)
        avg_plotneur_OAexit, gaussfiltpca_OAexit = plotPCAtrialNeur(neural, timearr, ax2)
        # plotstddev(neural, timearr, ax2)
        ax2.set_title('OA Exit')
    neural = np.array(params['dlc_data']['CA']['entry']['all_neur']) / binwidth  # all data (trials, time, units)
    # print(neural.size)
    if neural.size != 0:
        timelength = neural.shape[1] * binwidth
        timearr = np.linspace(-timelength / 2, timelength / 2, neural.shape[1])
        # plotall(neural, timearr, ax3)
        avg_plotneur_CAentry, gaussfiltpca_CAentry = plotPCAtrialNeur(neural, timearr, ax3)
        # plotstddev(neural, timearr, ax3)
        ax3.set_title('CA Entry')
    neural = np.array(params['dlc_data']['CA']['exit']['all_neur']) / binwidth  # all data (trials, time, units)
    # print(neural.shape)
    if neural.size != 0:
        timelength = neural.shape[1] * binwidth
        timearr = np.linspace(-timelength / 2, timelength / 2, neural.shape[1])
        # plotall(neural, timearr, ax4)
        avg_plotneur_CAexit, gaussfiltpca_CAexit = plotPCAtrialNeur(neural, timearr, ax4)
        # plotstddev(neural, timearr, ax4)
        ax4.set_title('CA Exit')
    neural = np.array(params['dlc_data']['headDips']['all_neur']) / binwidth  # all data (trials, time, units)
    # print(neural.size)
    if neural.size != 0:
        timelength = neural.shape[1] * binwidth
        timearr = np.linspace(-timelength / 2, timelength / 2, neural.shape[1])
        # plotall(neural, timearr, ax5)
        avg_plotneur_headdips, gaussfiltpca_headdips = plotPCAtrialNeur(neural, timearr, ax5)
        # plotstddev(neural, timearr, ax5)
        ax5.set_title('Head Dips')
    plt.savefig(pcaplotsave + "\\" + mousename + "_" + date + ".png")
    # plt.show()
    plt.close()

    # HEATMAPS !
    fig, ax = plt.subplots(2, 3, figsize=(15, 10))
    # ax.flatten()
    pcm1 = ax[0, 0].pcolormesh((np.mean(np.array(params['dlc_data']['OA']['entry']['all_neur']) / binwidth, axis=0)).T,
                               cmap='viridis')
    fig.colorbar(pcm1, ax=ax[0, 0])
    pcm2 = ax[0, 1].pcolormesh((np.mean(np.array(params['dlc_data']['OA']['exit']['all_neur']) / binwidth, axis=0)).T,
                               cmap='viridis')
    fig.colorbar(pcm2, ax=ax[0, 1])
    pcm3 = ax[0, 2].pcolormesh((np.mean(np.array(params['dlc_data']['CA']['entry']['all_neur']) / binwidth, axis=0)).T,
                               cmap='viridis')
    fig.colorbar(pcm3, ax=ax[0, 2])
    pcm4 = ax[1, 0].pcolormesh((np.mean(np.array(params['dlc_data']['CA']['exit']['all_neur']) / binwidth, axis=0)).T,
                               cmap='viridis')
    fig.colorbar(pcm4, ax=ax[1, 0])
    pcm5 = ax[1, 1].pcolormesh((np.mean(np.array(params['dlc_data']['headDips']['all_neur']) / binwidth, axis=0)).T,
                               cmap='viridis')
    fig.colorbar(pcm5, ax=ax[1, 1])

    plt.savefig(heatmapsplotsave + "\\" + mousename + "_" + date + ".png")
    plt.close()

    # sanity check
    for i in range(len(params['dlc_data']['OA']['entry']['beh'])):
        # if params['dlc_data']['OA']['entry']['beh'][i]
        plt.plot(params['dlc_data']['OA']['entry']['beh'][i][:, 1], params['dlc_data']['OA']['entry']['beh'][i][:, 2],
                 c='b')
        # print(params['dlc_data']['OA']['entry']['beh'][i][:, 1])
        # print(params['dlc_data']['OA']['entry']['beh'][i][:, 2])
    for i in range(len(params['dlc_data']['CA']['entry']['beh'])):
        plt.plot(params['dlc_data']['CA']['entry']['beh'][i][:, 1], params['dlc_data']['CA']['entry']['beh'][i][:, 2],
                 c='g')
    plt.close()
    for i in range(len(params['dlc_data']['OA']['exit']['beh'])):
        plt.plot(params['dlc_data']['OA']['exit']['beh'][i][:, 1], params['dlc_data']['OA']['exit']['beh'][i][:, 2],
                 c='b')
    for i in range(len(params['dlc_data']['CA']['exit']['beh'])):
        plt.plot(params['dlc_data']['CA']['exit']['beh'][i][:, 1], params['dlc_data']['CA']['exit']['beh'][i][:, 2],
                 c='g')

    # check unit type
    timelength = neural.shape[1] * binwidth
    timearr = np.linspace(-timelength / 2, timelength / 2, neural.shape[1])
    print(np.array(params['dlc_data']['OA']['entry']['all_neur']).shape)
    baseline = [-2, -1]
    heatmap_OA_entry, _, _ = np.array(
        calcAvgZscore(np.array(params['dlc_data']['OA']['entry']['all_neur']), timearr, baseline))
    heatmap_OA_exit, _, _ = np.array(
        calcAvgZscore(np.array(params['dlc_data']['OA']['exit']['all_neur']), timearr, baseline))
    heatmap_CA_entry, _, _ = np.array(
        calcAvgZscore(np.array(params['dlc_data']['CA']['entry']['all_neur']), timearr, baseline))
    heatmap_CA_exit, _, _ = np.array(
        calcAvgZscore(np.array(params['dlc_data']['CA']['exit']['all_neur']), timearr, baseline))
    heatmap_headdips, _, _ = np.array(
        calcAvgZscore(np.array(params['dlc_data']['headDips']['all_neur']), timearr, baseline))

    t = int(np.where(timearr >= 0)[0][0])

    params['beh_analysis']['psth']['eohm_idx_OAEntry'], params['beh_analysis']['psth']['iohm_idx_OAEntry'], \
    params['beh_analysis']['psth']['rhm_idx_OAEntry'] = checkunittype(heatmap_OA_entry, t)
    params['beh_analysis']['psth']['eohm_idx_OAExit'], params['beh_analysis']['psth']['iohm_idx_OAExit'], \
        params['beh_analysis']['psth']['rhm_idx_OAExit'] = checkunittype(heatmap_OA_exit, t)
    params['beh_analysis']['psth']['eohm_idx_CAEntry'], params['beh_analysis']['psth']['iohm_idx_CAEntry'], \
        params['beh_analysis']['psth']['rhm_idx_CAEntry'] = checkunittype(heatmap_CA_entry, t)
    params['beh_analysis']['psth']['eohm_idx_CAExit'], params['beh_analysis']['psth']['iohm_idx_CAExit'], \
        params['beh_analysis']['psth']['rhm_idx_CAExit'] = checkunittype(heatmap_CA_exit, t)
    params['beh_analysis']['psth']['eohm_idx_headdip'], params['beh_analysis']['psth']['iohm_idx_headdip'], \
        params['beh_analysis']['psth']['rhm_idx_headdip'] = checkunittype(heatmap_headdips, t)

    params['beh_analysis']['psth']['eohm_OAEntry_zscore'] = heatmap_OA_entry[:,
                                                            params['beh_analysis']['psth']['eohm_idx_OAEntry']]
    params['beh_analysis']['psth']['eohm_CAEntry_zscore'] = heatmap_CA_entry[:,
                                                            params['beh_analysis']['psth']['eohm_idx_CAEntry']]
    params['beh_analysis']['psth']['eohm_OAExit_zscore'] = heatmap_OA_exit[:,
                                                           params['beh_analysis']['psth']['eohm_idx_OAExit']]
    params['beh_analysis']['psth']['eohm_CAExit_zscore'] = heatmap_CA_exit[:,
                                                           params['beh_analysis']['psth']['eohm_idx_CAExit']]
    params['beh_analysis']['psth']['eohm_headdip_zscore'] = heatmap_headdips[:,
                                                            params['beh_analysis']['psth']['eohm_idx_headdip']]

    params['beh_analysis']['psth']['iohm_OAEntry_zscore'] = heatmap_OA_entry[:,
                                                            params['beh_analysis']['psth']['iohm_idx_OAEntry']]
    params['beh_analysis']['psth']['iohm_CAEntry_zscore'] = heatmap_CA_entry[:,
                                                            params['beh_analysis']['psth']['iohm_idx_CAEntry']]
    params['beh_analysis']['psth']['iohm_OAExit_zscore'] = heatmap_OA_exit[:,
                                                           params['beh_analysis']['psth']['iohm_idx_OAExit']]
    params['beh_analysis']['psth']['iohm_CAExit_zscore'] = heatmap_CA_exit[:,
                                                           params['beh_analysis']['psth']['iohm_idx_CAExit']]
    params['beh_analysis']['psth']['iohm_headdip_zscore'] = heatmap_headdips[:,
                                                            params['beh_analysis']['psth']['iohm_idx_headdip']]

    params['beh_analysis']['psth']['rhm_OAEntry_zscore'] = heatmap_OA_entry[:,
                                                           params['beh_analysis']['psth']['rhm_idx_OAEntry']]
    params['beh_analysis']['psth']['rhm_CAEntry_zscore'] = heatmap_CA_entry[:,
                                                           params['beh_analysis']['psth']['rhm_idx_CAEntry']]
    params['beh_analysis']['psth']['rhm_OAExit_zscore'] = heatmap_OA_exit[:,
                                                          params['beh_analysis']['psth']['rhm_idx_OAExit']]
    params['beh_analysis']['psth']['rhm_CAExit_zscore'] = heatmap_CA_exit[:,
                                                          params['beh_analysis']['psth']['rhm_idx_CAExit']]
    params['beh_analysis']['psth']['rhm_headdip_zscore'] = heatmap_headdips[:,
                                                           params['beh_analysis']['psth']['rhm_idx_headdip']]

    EPM_plotSeparatedunits(params, timearr)

    return params['dlc_data']['OA']['entry']['all_neur'], gaussfiltpca_OAentry, params['dlc_data']['OA']['exit'][
        'all_neur'], gaussfiltpca_OAexit, params['dlc_data']['CA']['exit']['all_neur'], gaussfiltpca_CAentry, \
    params['dlc_data']['CA']['exit']['all_neur'], gaussfiltpca_CAexit, np.array(
        params['dlc_data']['headDips']['all_neur']), gaussfiltpca_headdips


def SQplotdata(params, savedatadir):
    subdirsave_pca = "PCA_SQ"
    subdirsave_avg = "Avg_SQ"
    subdirsave_lickhist = "LickNum_SQ"
    if not os.path.exists(savedatadir + "\\" + subdirsave_avg):
        os.mkdir(savedatadir + "\\" + subdirsave_avg)
    if not os.path.exists(savedatadir + "\\" + subdirsave_pca):
        os.mkdir(savedatadir + "\\" + subdirsave_pca)

    if not os.path.exists(savedatadir + "\\" + subdirsave_lickhist):
        os.mkdir(savedatadir + "\\" + subdirsave_lickhist)

    unitnum = len(params['beh_analysis']['psth']['lick_ch0'])
    lick0times = params['behavior']['lick_ch0']['ts']
    lick1times = params['behavior']['lick_ch1']['ts']
    # print(lick0times)
    # print(len(lick0times))

    licktotal = np.array([lick0times.size, lick1times.size])
    binwidth = params['beh_analysis']['psth_Hz']['params']['bin_size_sec']
    timevec = params['beh_analysis']['psth_Hz']['params']['edges'][1:] - binwidth/2
    neural_S_dict = params['beh_analysis']['psth']['lick_ch0']
    neural_S = np.zeros((licktotal[0], timevec.size, unitnum))
    neural_Q = np.zeros((licktotal[1], timevec.size, unitnum))
    neural_Q_dict = params['beh_analysis']['psth']['lick_ch1']
    mousename = params['mouse']['name']
    date = params['session']['date']
    # print(timevec)
    # print(neural_S_dict[0].shape)
    for i in range(unitnum):
        if neural_S_dict.get(i) is not None:
            neural_S[:, :, i] = neural_S_dict[i]  # /binwidth
        if neural_Q_dict.get(i) is not None:
            neural_Q[:, :, i] = neural_Q_dict[i]  # /binwidth

    plt.bar([0, 1], licktotal)
    plt.xlabel('Sucrose // Quinine')
    plt.ylabel('No. of licks')
    plt.savefig(savedatadir + "\\" + subdirsave_lickhist + "\\" + mousename + "_" + date + ".png")
    plt.close()
    #print('difference in lick time:')
    #print(np.diff(lick0times))
    #print(np.diff(lick1times))
    if np.diff(lick0times).size == 0 or np.diff(lick1times).size == 0:
        print('not analysing this file')
        return 0
    # plt.show()
    plt.close()
    ax1 = plt.subplot(121)
    ax2 = plt.subplot(122)
    plotall(neural_S, timevec, ax1)
    plotall(neural_Q, timevec, ax2)
    plotavgtrialNeur(neural_S, timevec, ax1)
    plotavgtrialNeur(neural_Q, timevec, ax2)
    ax1.set_title('Sucrose')
    ax2.set_title("Quinine")
    ax1.set_xlabel('Time (s)')
    ax2.set_xlabel('Time (s)')
    ax1.set_ylabel('Firing rate (Hz)')
    plt.savefig(savedatadir + "\\" + subdirsave_avg + "\\" + mousename + "_" + date + ".png")
    # plt.show()
    plt.close()

    ax1 = plt.subplot(121)
    ax2 = plt.subplot(122)
    plotall(neural_S, timevec, ax1)
    plotall(neural_Q, timevec, ax2)
    plotPCAtrialNeur(neural_S, timevec, ax1)
    plotPCAtrialNeur(neural_Q, timevec, ax2)
    ax1.set_title('Sucrose')
    ax2.set_title("Quinine")
    ax1.set_xlabel('Time (s)')
    ax2.set_xlabel('Time (s)')
    ax1.set_ylabel('Firing rate (Hz)')
    plt.savefig(savedatadir + "\\" + subdirsave_pca + "\\" + mousename + "_" + date + ".png")
    # plt.show()
    plt.close()
    params['beh_analysis']['psth']['SucroseAnalysed_FR'] = neural_S
    params['beh_analysis']['psth']['QuinineAnalysed_FR'] = neural_Q
    return params


def calcAvgZscore(neural, timevec, baselinetime_s):
    # first average out the trials
    # then check baseline for each unit

    # print(baselinetime_s)
    timevec = np.round(timevec, decimals=2)
    # print(timevec)
    # print(np.where(baselinetime_s[0] == timevec))
    st = int(np.where(timevec <= baselinetime_s[0])[0][-1])
    # print(st)
    ed = int(np.where(timevec >= baselinetime_s[1])[0][0]) + 1
    # print(ed)

    if neural.ndim == 3:
        avg_neur = np.nanmean(neural, axis=0)  # (time, units)
    else:
        avg_neur = neural
    # added later to consider (-5, 5) time window
    # st1 = int(np.where(-9.5 == timevec)[0])#int(np.where(-5 == timevec)[0])
    # ed1 = int(np.where(9.5 == timevec)[0])#int(np.where(5 == timevec)[0])

    baseline = avg_neur[st:ed, :]  # baseline firing rate across all units
    mean_ = np.nanmean(baseline, axis=0)  # across time
    # print(baseline)
    std_ = np.nanstd(baseline, axis=0)  # across time
    # print(std_)

    # check for standard dev == 0
    check0 = np.where(std_ == 0)[0]
    #if check0.size != 0:
        #plt.imshow(neural[:,st:ed,check0])
        #plt.show()
        #std_[check0] = 1
    zscore_ = (avg_neur - mean_)  # (time, units)

    heat_map = (zscore_ / std_)
    for i in range(heat_map.shape[0]):
        heat_map[i, ~np.isfinite(heat_map[i, :])] = np.nan

    '''
    if check0.size == 0:

        zscore_ = (avg_neur - mean_)# (avg_neur[st1:ed1] - mean_)
        heat_map = (zscore_ / std_)
        return heat_map, st, ed
    else:
        if check0.size == baseline.shape[-1]: # all units are 0
            return 0,0,0
        else:
            mean_del = np.delete(mean_, check0)
            avg_neur_del = np.delete(avg_neur, check0, axis=1)
            zscore_ = (avg_neur_del - mean_del)  # (avg_neur[st1:ed1] - mean_)
            heat_map = (zscore_ / std_)
            return heat_map, st, ed
    '''
    return heat_map, st, ed


def SQ_SeparatedUnitsPlot(params, savedatadirsubdir):
    subdir = "SQ_separated"
    if not os.path.exists(savedatadirsubdir + "\\" + subdir):
        os.mkdir(savedatadirsubdir + "\\" + subdir)

    eohm = params['beh_analysis']['psth']['eohm_idx_S']
    iohm = params['beh_analysis']['psth']['iohm_idx_S']
    # eihm = params['beh_analysis']['psth']['eihm_idx_S']
    rhm = params['beh_analysis']['psth']['rhm_idx_S']
    timearr = params['beh_analysis']['psth_Hz']['params']['edges'][1:]
    fig, ax = plt.subplots(2, 3, figsize=(10, 10))
    # ax.flatten()
    # .flatten()
    neurS = params['beh_analysis']['psth']['SucroseAnalysed_FR']
    neurQ = params['beh_analysis']['psth']['QuinineAnalysed_FR']
    neurS_eohm = neurS[:, :, eohm]
    neurS_iohm = neurS[:, :, iohm]
    # neurS_eihm = neurS[:,:,eihm]
    neurS_rhm = neurS[:, :, rhm]

    # plotall(neurS_eohm, timearr, ax[0,0])
    # plotall(neurS_iohm, timearr, ax[0,1])
    # plotall(neurS_eihm, timearr, ax[1,0])
    # plotall(neurS_rhm, timearr, ax[1,1])
    plotavgtrialNeur(neurS_eohm, timearr, ax[0, 0])
    plotavgtrialNeur(neurS_iohm, timearr, ax[0, 1])
    # plotavgtrialNeur(neurS_eihm, timearr,ax[0,2])
    plotavgtrialNeur(neurS_rhm, timearr, ax[0, 2])
    ax[0, 0].set_title('Excited Only')
    ax[0, 1].set_title('Inhibited Only')
    # ax[0,2].set_title('Excited and Inhibited')
    ax[0, 2].set_title('No Change')

    eohm = params['beh_analysis']['psth']['eohm_idx_Q']
    iohm = params['beh_analysis']['psth']['iohm_idx_Q']
    # eihm = params['beh_analysis']['psth']['eihm_idx_Q']
    rhm = params['beh_analysis']['psth']['rhm_idx_Q']
    neurQ_eohm = neurQ[:, :, eohm]
    neurQ_iohm = neurQ[:, :, iohm]
    # neurQ_eihm = neurQ[:, :, eihm]
    neurQ_rhm = neurQ[:, :, rhm]
    plotavgtrialNeur(neurQ_eohm, timearr, ax[1, 0])
    plotavgtrialNeur(neurQ_iohm, timearr, ax[1, 1])
    # plotavgtrialNeur(neurQ_eihm, timearr, ax[1, 2])
    plotavgtrialNeur(neurQ_rhm, timearr, ax[1, 2])

    params['beh_analysis']['psth']['eohm_S'] = neurS_eohm
    params['beh_analysis']['psth']['iohm_S'] = neurS_iohm
    # params['beh_analysis']['psth']['eihm_S'] = neurS_eihm
    params['beh_analysis']['psth']['rhm_S'] = neurS_rhm

    params['beh_analysis']['psth']['eohm_Q'] = neurQ_eohm
    params['beh_analysis']['psth']['iohm_Q'] = neurQ_iohm
    # params['beh_analysis']['psth']['eihm_Q'] = neurQ_eihm
    params['beh_analysis']['psth']['rhm_Q'] = neurQ_rhm

    plt.suptitle('SQ')
    plt.savefig(savedatadirsubdir + "\\" + subdir + "\\" + params['mouse']['name'] + "_" + params['session'][
        'date'] + "_PSTH.png")
    plt.close()

    # heatmaps
    fig, ax = plt.subplots(2, 3, figsize=(10, 10))
    # ax.flatten()
    ax[0, 0].set_title('Excited Only')
    ax[0, 1].set_title('Inhibited Only')
    # ax[0,2].set_title('Excited and Inhibited')
    ax[0, 2].set_title('No Change')
    # print(neurS_eohm.T)
    pcm1 = ax[0, 0].pcolormesh(np.mean(neurS_eohm, axis=0).T, cmap='viridis')  # (time, unit)
    fig.colorbar(pcm1, ax=ax[0, 0])
    pcm2 = ax[0, 1].pcolormesh(np.mean(neurS_iohm, axis=0).T, cmap='viridis')
    fig.colorbar(pcm2, ax=ax[0, 1])
    # pcm3 = ax[0,2].pcolormesh(np.mean(neurS_eihm,axis=0).T, cmap='viridis')
    # fig.colorbar(pcm3, ax=ax[0,2])
    pcm4 = ax[0, 2].pcolormesh(np.mean(neurS_rhm, axis=0).T, cmap='viridis')
    fig.colorbar(pcm4, ax=ax[0, 2])

    pcm5 = ax[1, 0].pcolormesh(np.mean(neurS_eohm, axis=0).T, cmap='viridis')  # (time, unit)
    fig.colorbar(pcm5, ax=ax[1, 0])
    pcm6 = ax[1, 1].pcolormesh(np.mean(neurS_iohm, axis=0).T, cmap='viridis')
    fig.colorbar(pcm6, ax=ax[1, 1])
    # pcm7 = ax[1, 2].pcolormesh(np.mean(neurS_eihm, axis=0).T, cmap='viridis')
    # fig.colorbar(pcm7, ax=ax[1, 2])
    pcm8 = ax[1, 2].pcolormesh(np.mean(neurS_rhm, axis=0).T, cmap='viridis')
    fig.colorbar(pcm8, ax=ax[1, 2])

    plt.savefig(savedatadirsubdir + "\\" + subdir + "\\" + params['mouse']['name'] + "_" + params['session'][
        'date'] + "_heatmaps.png")
    plt.close()
    return params


def SQ_avgdata(params, eohm_total_S, iohm_total_S, rhm_total_S, eohm_total_Q, iohm_total_Q, rhm_total_Q):
    eohm = params['beh_analysis']['psth']['eohm_idx_S']
    iohm = params['beh_analysis']['psth']['iohm_idx_S']
    # eihm = params['beh_analysis']['psth']['eihm_idx_S']
    rhm = params['beh_analysis']['psth']['rhm_idx_S']
    neurS = params['beh_analysis']['psth']['SucroseAnalysed_FR']
    neurQ = params['beh_analysis']['psth']['QuinineAnalysed_FR']
    neurS_eohm = neurS[:, :, eohm]
    neurS_iohm = neurS[:, :, iohm]
    # neurS_eihm = neurS[:, :, eihm]
    neurS_rhm = neurS[:, :, rhm]

    eohm_avg = np.nanmean(neurS_eohm, axis=0)
    iohm_avg = np.nanmean(neurS_iohm, axis=0)
    # eihm_avg = np.nanmean(neurS_eihm, axis=0)
    rhm_avg = np.nanmean(neurS_rhm, axis=0)

    eohm_total_S.append(eohm_avg)
    iohm_total_S.append(iohm_avg)
    # eihm_total_S.append(eihm_avg)
    rhm_total_S.append(rhm_avg)

    eohm = params['beh_analysis']['psth']['eohm_idx_Q']
    iohm = params['beh_analysis']['psth']['iohm_idx_Q']
    # eihm = params['beh_analysis']['psth']['eihm_idx_Q']
    rhm = params['beh_analysis']['psth']['rhm_idx_Q']
    neurQ_eohm = neurQ[:, :, eohm]
    neurQ_iohm = neurQ[:, :, iohm]
    # neurQ_eihm = neurQ[:, :, eihm]
    neurQ_rhm = neurQ[:, :, rhm]

    eohm_avg = np.nanmean(neurQ_eohm, axis=0)
    iohm_avg = np.nanmean(neurQ_iohm, axis=0)
    # eihm_avg = np.nanmean(neurQ_eihm, axis=0)
    rhm_avg = np.nanmean(neurQ_rhm, axis=0)

    eohm_total_Q.append(eohm_avg)
    iohm_total_Q.append(iohm_avg)
    # eihm_total_Q.append(eihm_avg)
    rhm_total_Q.append(rhm_avg)

    return eohm_total_S, iohm_total_S, rhm_total_S, eohm_total_Q, iohm_total_Q, rhm_total_Q


def orderHM(hm, st, ed):
    avgs = np.nanmean(hm[:,st:ed], axis=-1)
    idx = np.argsort(avgs)[::-1] # reverse array after sorting it
    hm1 = hm[idx,:]
    return hm1


def SQ_plotavgSeparatedUnits(eohm_total_S, iohm_total_S, rhm_total_S, eohm_total_Q, iohm_total_Q, rhm_total_Q, timevec,
                             counts_SQ):
    eohm = []
    iohm = []
    # eihm = []
    rhm = []
    # print(timevec)
    # print(eohm_total_S)
    eohm_total_S1 = eohm_total_S.copy()
    iohm_total_S1 = iohm_total_S.copy()
    # eihm_total_S1 = eihm_total_S.copy()
    rhm_total_S1 = rhm_total_S.copy()
    eohm_total_Q1 = eohm_total_Q.copy()
    iohm_total_Q1 = iohm_total_Q.copy()
    # eihm_total_Q1 = eihm_total_Q.copy()
    rhm_total_Q1 = rhm_total_Q.copy()
    # timevec = np.linspace(-9.5, 9.5, 401)[:-1] #np.linspace(-5, 4.5, 20) #
    # timevec=timevec[:-1]
    timepoints = timevec.size
    time_min = np.min(timevec)
    time_max = np.max(timevec)
    binwidth = 0.05  # set
    timevec = np.linspace(time_min + 0.025, time_max - 0.025, timepoints - 1)

    '''
    for i in range(len(eohm_total_S)):
        # z score everything
        #print(len(eohm_total_S))
        #print(eohm_total_S[i].shape)
        if eohm_total_S[i].shape[-1] != 0:
            #if eohm_total_S[i]
            #pprint.pprint(eohm_total_S[i])
            eohm_total_S[i],_,_ = calcAvgZscore(eohm_total_S[i], timevec, baselinetime_s=[-5, -3])
            #pprint.pprint((eohm_total_S[i]))

        else:
            eohm_total_S[i] = np.array([])
        if iohm_total_S[i].shape[-1] != 0:
            iohm_total_S[i],_,_ = calcAvgZscore(iohm_total_S[i], timevec, baselinetime_s=[-5, -3])
        else:
            iohm_total_S[i] = np.array([])

        #if eihm_total_S[i].shape[-1] != 0:
        #    eihm_total_S[i],_,_ = calcAvgZscore(eihm_total_S[i], timevec, baselinetime_s=[-5, -3])
        #else:
        #    eihm_total_S[i] = np.array([])

        if rhm_total_S[i].shape[-1] != 0:
            rhm_total_S[i],_,_ = calcAvgZscore(rhm_total_S[i], timevec, baselinetime_s=[-5, -3])
        else:
            rhm_total_S[i] = np.array([])
        if eohm_total_Q[i].shape[-1] != 0:
            eohm_total_Q[i],_,_ = calcAvgZscore(eohm_total_Q[i], timevec, baselinetime_s=[-5, -3])
        else:
            eohm_total_Q[i] = np.array([])
        if iohm_total_Q[i].shape[-1] != 0:
            iohm_total_Q[i],_,_ = calcAvgZscore(iohm_total_Q[i], timevec, baselinetime_s=[-5, -3])
        else:
            iohm_total_Q[i] = np.array([])
        #if eihm_total_Q[i].shape[-1] != 0:

        #    eihm_total_Q[i],_,_ = calcAvgZscore(eihm_total_Q[i], timevec, baselinetime_s=[-5, -3])
        #else:
        #    eihm_total_Q[i] = np.array([])
        if rhm_total_Q[i].shape[-1] != 0:

            rhm_total_Q[i],_,_ = calcAvgZscore(rhm_total_Q[i], timevec, baselinetime_s=[-5, -3])
        else:
            rhm_total_Q[i] = np.array([])
    #print(eohm_total_S)
    '''
    for i in range(len(eohm_total_S)):
        # print(eohm_total_S[i])
        if eohm_total_S[i].shape[-1] != 0:
            # to ignore infinities
            eohm.append(np.nanmean(eohm_total_S[i], axis=1))
        if iohm_total_S[i].shape[-1] != 0:
            iohm.append(np.nanmean(iohm_total_S[i], axis=1))
        # if eihm_total_S[i].shape[-1] != 0:
        #    eihm.append(np.nanmean(eihm_total_S[i], axis=1))
        if rhm_total_S[i].shape[-1] != 0:
            rhm.append(np.nanmean(rhm_total_S[i], axis=1))

    eohm = np.array(eohm)
    iohm = np.array(iohm)
    # eihm = np.array(eihm)
    rhm = np.array(rhm)  # session x time
    # print(rhm)
    # print(eohm)

    eohm_totavg = np.nanmean(eohm, axis=0)
    # print(eohm_totavg)
    eohm_totsd = sem(eohm, axis=0, nan_policy='omit')
    iohm_totavg = np.nanmean(iohm, axis=0)
    iohm_totsd = sem(iohm, axis=0, nan_policy='omit')
    # eihm_totavg = np.nanmean(eihm, axis=0)
    # eihm_totsd =  sem(eihm, axis=0,nan_policy='omit')
    rhm_totavg = np.nanmean(rhm, axis=0)
    rhm_totsd = sem(rhm, axis=0, nan_policy='omit')

    fig, ax = plt.subplots(2, 3, figsize=(15, 10))
    ax[0, 0].plot(timevec, gaussian_filter1d(eohm_totavg, sigma=3), c='k')
    ax[0, 0].plot(timevec, gaussian_filter1d(eohm_totavg + eohm_totsd, sigma=3), c='grey')
    ax[0, 0].plot(timevec, gaussian_filter1d(eohm_totavg - eohm_totsd, sigma=3), c='grey')
    # print(iohm_totavg)
    # print(iohm_totavg - iohm_totsd)
    ax[0, 1].plot(timevec, gaussian_filter1d(iohm_totavg, sigma=3), c='k')
    ax[0, 1].plot(timevec, gaussian_filter1d(iohm_totavg + iohm_totsd, sigma=3), c='grey')
    ax[0, 1].plot(timevec, gaussian_filter1d(iohm_totavg - iohm_totsd, sigma=3), c='grey')
    # ax[0, 2].plot(timevec,gaussian_filter1d(eihm_totavg, sigma = 3), c='k')
    # ax[0, 2].plot(timevec,gaussian_filter1d(eihm_totavg + eihm_totsd, sigma = 3), c='grey')
    # ax[0, 2].plot(timevec,gaussian_filter1d(eihm_totavg - eihm_totsd, sigma = 3), c='grey')
    ax[0, 2].plot(timevec, gaussian_filter1d(rhm_totavg, sigma=3), c='k')
    ax[0, 2].plot(timevec, gaussian_filter1d(rhm_totavg + rhm_totsd, sigma=3), c='grey')
    ax[0, 2].plot(timevec, gaussian_filter1d(rhm_totavg - rhm_totsd, sigma=3), c='grey')

    eohm = []
    iohm = []
    # eihm = []
    rhm = []
    for i in range(len(eohm_total_Q)):
        # print(eohm_total_S[i])
        if (eohm_total_Q[i]).shape[-1] != 0:
            eohm.append(np.nanmean(eohm_total_Q[i], axis=1))
        if (iohm_total_Q[i]).shape[-1] != 0:
            # print(iohm_total_Q[i])
            iohm.append(np.nanmean(iohm_total_Q[i], axis=1))
        # if (eihm_total_S[i]).shape[-1] != 0:
        #    eihm.append(np.nanmean(eihm_total_Q[i], axis=1))
        if (rhm_total_Q[i]).shape[-1] != 0:
            rhm.append(np.nanmean(rhm_total_Q[i], axis=1))

    eohm = np.array(eohm)  # .T
    iohm = np.array(iohm)  # .T
    # eihm = np.array(eihm)#.T
    rhm = np.array(rhm)  # .T  # session x time

    # print(eohm)
    eohm_totavg = np.nanmean(eohm, axis=0)
    eohm_totsd = sem(eohm, axis=0, nan_policy='omit')
    iohm_totavg = np.nanmean(iohm, axis=0)
    iohm_totsd = sem(iohm, axis=0, nan_policy='omit')
    # eihm_totavg = np.nanmean(eihm, axis=0)
    # eihm_totsd =  sem(eihm, axis=0,nan_policy='omit')
    rhm_totavg = np.nanmean(rhm, axis=0)
    rhm_totsd = sem(rhm, axis=0, nan_policy='omit')

    ax[1, 0].plot(timevec, gaussian_filter1d(eohm_totavg, sigma=3), c='k')
    ax[1, 0].plot(timevec, gaussian_filter1d(eohm_totavg + eohm_totsd, sigma=3), c='grey')
    ax[1, 0].plot(timevec, gaussian_filter1d(eohm_totavg - eohm_totsd, sigma=3), c='grey')
    ax[1, 1].plot(timevec, gaussian_filter1d(iohm_totavg, sigma=3), c='k')
    ax[1, 1].plot(timevec, gaussian_filter1d(iohm_totavg + iohm_totsd, sigma=3), c='grey')
    ax[1, 1].plot(timevec, gaussian_filter1d(iohm_totavg - iohm_totsd, sigma=3), c='grey')
    # ax[1, 2].plot(timevec, gaussian_filter1d(eihm_totavg, sigma=3), c='k')
    # ax[1, 2].plot(timevec, gaussian_filter1d(eihm_totavg + eihm_totsd, sigma=3), c='grey')
    # ax[1, 2].plot(timevec, gaussian_filter1d(eihm_totavg - eihm_totsd, sigma=3), c='grey')
    ax[1, 2].plot(timevec, gaussian_filter1d(rhm_totavg, sigma=3), c='k')
    ax[1, 2].plot(timevec, gaussian_filter1d(rhm_totavg + rhm_totsd, sigma=3), c='grey')
    ax[1, 2].plot(timevec, gaussian_filter1d(rhm_totavg - rhm_totsd, sigma=3), c='grey')

    ax[0, 0].axvline(x=0, color='b')
    ax[0, 1].axvline(x=0, color='b')
    ax[0, 2].axvline(x=0, color='b')
    # ax[0, 3].axvline(x=0, color='b')
    ax[1, 0].axvline(x=0, color='b')
    ax[1, 1].axvline(x=0, color='b')
    ax[1, 2].axvline(x=0, color='b')
    # ax[1, 3].axvline(x=0, color='b')

    ax[0, 0].set_ylim([-4.5, 5])
    ax[0, 1].set_ylim([-4.5, 5])
    ax[0, 2].set_ylim([-4.5, 5])
    ax[1, 0].set_ylim([-4.5, 5])
    ax[1, 1].set_ylim([-4.5, 5])
    ax[1, 2].set_ylim([-4.5, 5])

    ax[0, 0].set_ylabel('Z scores - Sucrose')
    ax[1, 0].set_ylabel('Z scores - Quinine')
    ax[1, 0].set_xlabel('Time (s)')
    ax[1, 1].set_xlabel('Time (s)')
    ax[1, 2].set_xlabel('Time (s)')
    # ax[1, 3].set_xlabel('Time (s)')
    ax[0, 0].set_title('Excited')
    ax[0, 1].set_title('Inhibited')
    # ax[0, 2].set_title('Excited and Inhibited')
    ax[0, 2].set_title('No Change')

    plt.savefig('SucroseQuininePSTH.svg')
    plt.close()

    # HEATMAPS
    eohm = []
    iohm = []
    # eihm = []
    rhm = []
    S_totalunits = np.zeros(3)

    for i in range(len(eohm_total_S)):
        # print(eohm_total_S[i])
        if eohm_total_S[i].shape[-1] != 0:
            # to ignore infinities
            eohm.append(np.nanmean(eohm_total_S[i], axis=1))
        if iohm_total_S[i].shape[-1] != 0:
            iohm.append(np.nanmean(iohm_total_S[i], axis=1))
        # if eihm_total_S[i].shape[-1] != 0:
        #    eihm.append(np.nanmean(eihm_total_S[i], axis=1))
        if rhm_total_S[i].shape[-1] != 0:
            rhm.append(np.nanmean(rhm_total_S[i], axis=1))

    eohm = np.array(eohm)
    iohm = np.array(iohm)
    # eihm = np.array(eihm)
    rhm = np.array(rhm)  # session x time

    fig, ax = plt.subplots(2, 3, figsize=(15, 10))
    pcm1 = ax[0, 0].pcolormesh(eohm, cmap='viridis')  # (time, unit)
    fig.colorbar(pcm1, ax=ax[0, 0])
    pcm2 = ax[0, 1].pcolormesh(iohm, cmap='viridis')
    fig.colorbar(pcm2, ax=ax[0, 1])
    # pcm3 = ax[0, 2].pcolormesh(eihm, cmap='viridis')
    # fig.colorbar(pcm3, ax=ax[0, 2])
    pcm4 = ax[0, 2].pcolormesh(rhm, cmap='viridis')
    fig.colorbar(pcm4, ax=ax[0, 2])

    eohm = []
    iohm = []
    # eihm = []
    rhm = []
    Q_totalunits = np.zeros(3)

    for i in range(len(eohm_total_Q)):
        # print(eohm_total_S[i])
        if eohm_total_Q[i].shape[-1] != 0:
            # to ignore infinities
            eohm.append(np.nanmean(eohm_total_Q[i], axis=1))
        if iohm_total_Q[i].shape[-1] != 0:
            iohm.append(np.nanmean(iohm_total_Q[i], axis=1))
        # if eihm_total_S[i].shape[-1] != 0:
        #    eihm.append(np.nanmean(eihm_total_S[i], axis=1))
        if rhm_total_Q[i].shape[-1] != 0:
            rhm.append(np.nanmean(rhm_total_Q[i], axis=1))

    eohm = np.array(eohm)
    iohm = np.array(iohm)
    # eihm = np.array(eihm)
    rhm = np.array(rhm)  # session x time

    eohm = np.array(eohm)
    iohm = np.array(iohm)
    # eihm = np.array(eihm)
    rhm = np.array(rhm)
    # print(iohm)

    pcm5 = ax[1, 0].pcolormesh(eohm, cmap='viridis')  # (time, unit)
    fig.colorbar(pcm5, ax=ax[1, 0])
    pcm6 = ax[1, 1].pcolormesh(iohm, cmap='viridis')
    fig.colorbar(pcm6, ax=ax[1, 1])
    # pcm7 = ax[1, 2].pcolormesh(eihm, cmap='viridis')
    # fig.colorbar(pcm7, ax=ax[1, 2])
    pcm8 = ax[1, 2].pcolormesh(rhm, cmap='viridis')
    fig.colorbar(pcm8, ax=ax[1, 2])
    ax[0, 0].set_ylabel('Sessions - Sucrose')
    ax[1, 0].set_ylabel('Sessions - Quinine')
    ax[0, 0].set_title('Excited')
    ax[0, 1].set_title('Inhibited')
    # ax[0, 2].set_title('Excited and Inhibited')
    ax[0, 2].set_title('No Change')
    ax[1, 0].set_xlabel('Time (s)')
    ax[1, 1].set_xlabel('Time (s)')
    ax[1, 2].set_xlabel('Time (s)')
    # ax[1, 3].set_xlabel('Time (s)')
    plt.savefig('SucroseQuinine_Heatmap.svg')
    # plt.show()
    plt.close()

    '''
    ## plot pie graph
    labels = ['excited', 'inhibited', 'no change']
    fig, ax = plt.subplots(1,2)
    ax.flatten()
    #print(S_totalunits)
    ax[0].pie(S_totalunits, labels=labels, autopct='%1.1f%%')
    ax[1].pie(Q_totalunits, labels=labels, autopct='%1.1f%%')
    ax[0].set_title("Sucrose")
    ax[1].set_title("Quinine")
    plt.savefig("SQ_unitpercentagecount.png")
    #plt.show()
    plt.close()
    '''
    # MATRIX 3X3 - Counts
    counterrr = counts_SQ.flatten()  # concatenate along rows
    counterrr = counterrr.astype(int)
    # print(counterrr)

    labels = ['ExS', 'IbS', 'ExQ', 'InhQ', 'Exc-both', 'Inh-both', 'ExS/IbQ', 'ExQ/IbS', 'no response']
    fig = plt.figure(figsize=(15, 15), dpi=80)
    plt.pie(counterrr, autopct='%1.1f%%')
    plt.legend(labels)
    plt.savefig("SQ_unitpercentagecount_combined.svg")
    plt.close()

    # HEATMAPS - combined
    eohm = np.array([])
    iohm = np.array([])
    rhm = np.array([])
    timevecc = np.arange(-5 + 0.025, 5 - 0.025, 1)  # np.arange(-10,9.95,1) #np.linspace(-10, 9.5, 11)
    timepointnum = timevecc.size
    st5sec = int(np.where(timevec <= -5)[0][-1])
    ed5sec = int(np.where(timevec >= 5)[0][0])
    '''
    for i in range(len(eohm_total_S)):
        # print(eohm_total_S[i])
        if (eohm_total_S[i]).shape[-1] != 0 and np.all(np.all(eohm_total_S[i] == eohm_total_S[i])):
            if len(eohm) == 0:
                eohm = (eohm_total_S[i][st5sec:ed5sec].T)
            else:
                eohm = np.vstack((eohm, eohm_total_S[i][st5sec:ed5sec].T))  # eohm.append(eohm_total_S[i].T)

        if (iohm_total_S[i]).shape[-1] != 0 and np.all(np.all(iohm_total_S[i] == iohm_total_S[i])):

            if len(iohm) == 0:
                iohm = iohm_total_S[i][st5sec:ed5sec].T
            else:
                iohm = np.vstack((iohm, iohm_total_S[i][st5sec:ed5sec].T))

        if (rhm_total_S[i]).shape[-1] != 0 and not np.isnan(rhm_total_S[i]).any():
            if len(rhm) == 0:
                rhm = rhm_total_S[i][st5sec:ed5sec].T
            else:
                rhm = np.vstack((rhm, rhm_total_S[i][st5sec:ed5sec].T))

    eohm = np.array(eohm)
    iohm = np.array(iohm)
    rhm = np.array(rhm)
    #print(eohm.shape)
    '''
    eohm = np.array([])
    iohm = np.array([])
    # eihm = []
    rhm = np.array([])
    # print(rhm_total_S[0].shape)

    for i in range(len(eohm_total_S)):
        # print(eohm_total_S[i])
        if (eohm_total_S[i]).shape[-1] != 0:
            if eohm.size == 0:
                eohm = eohm_total_S[i][st5sec:ed5sec, :].T
            else:
                eohm = np.vstack((eohm, eohm_total_S[i][st5sec:ed5sec, :].T))
        if (iohm_total_S[i]).shape[-1] != 0:
            if iohm.size == 0:
                iohm = iohm_total_S[i][st5sec:ed5sec, :].T
            else:
                iohm = np.vstack((iohm, iohm_total_S[i][st5sec:ed5sec, :].T))
        # if (eihm_total_S[i]).shape[-1] != 0:
        #    eihm.append(np.nanmean(eihm_total_Q[i], axis=1))
        if (rhm_total_S[i]).shape[-1] != 0:
            if rhm.size == 0:
                rhm = rhm_total_S[i][st5sec:ed5sec, :].T
            else:
                rhm = np.vstack((rhm, rhm_total_S[i][st5sec:ed5sec, :].T))

    eohm = np.array(eohm)  # .T
    iohm = np.array(iohm)  # .T
    # eihm = np.array(eihm)#.T
    rhm = np.array(rhm)  # .T  # session x time

    #print(timevec[st5sec:ed5sec])
    st_max = np.where(timevec[st5sec:ed5sec] >= 0)[0][0].astype(int)
    ed_max = np.where(timevec[st5sec:ed5sec] >= 1)[0][0].astype(int)
    #print(eohm.shape)
    eohm = orderHM(eohm, st_max, ed_max)
    iohm = orderHM(iohm, st_max, ed_max)
    rhm = orderHM(rhm, st_max, ed_max)

    combined = np.vstack((eohm, iohm, rhm))
    line_ei = eohm.shape[0]
    line_ir = iohm.shape[0] + eohm.shape[0]
    #print(line_ir)
    #print(line_ei)
    fig, ax = plt.subplots(1, 2, figsize=(15, 10))
    ax.flatten()
    vmin = -2  # -5
    vmax = 5  # 10
    h0 = ax[0].imshow(combined, vmin=vmin, vmax=vmax)  # ax[0].pcolormesh(combined, cmap='viridis')  # (time, unit)
    ax[0].axhline(y=line_ei, color = 'k')
    ax[0].axhline(y=line_ir, color='k')
    #ax[0].colorbar(h0)
    fig.colorbar(h0, ax=ax[0],shrink=0.5)

    '''
    eohm = []
    iohm = []
    rhm = []
    for i in range(len(eohm_total_S)):
        if (eohm_total_Q[i]).shape[-1] != 0 and not np.isnan(eohm_total_Q[i]).any():
            if len(eohm) == 0:
                eohm = eohm_total_S[i][st5sec:ed5sec].T
            else:
                eohm = np.vstack((eohm, eohm_total_Q[i][st5sec:ed5sec].T))

        if (iohm_total_Q[i]).shape[-1] != 0 and not np.isnan(iohm_total_Q[i]).any():
            if len(iohm) == 0:
                iohm = iohm_total_S[i][st5sec:ed5sec].T
            else:
                iohm = np.vstack((iohm, iohm_total_Q[i][st5sec:ed5sec].T))

        if (rhm_total_Q[i]).shape[-1] != 0 and not np.isnan(rhm_total_Q[i]).any():
            if len(rhm) == 0:
                rhm = rhm_total_Q[i][st5sec:ed5sec].T
            else:
                rhm = np.vstack((rhm, rhm_total_Q[i][st5sec:ed5sec].T))
    '''
    eohm = np.array([])
    iohm = np.array([])
    # eihm = []
    rhm = np.array([])
    # print(rhm_total_Q[0].shape)

    for i in range(len(eohm_total_Q)):
        # print(eohm_total_S[i])
        if (eohm_total_Q[i]).shape[-1] != 0:
            if eohm.size == 0:
                eohm = eohm_total_Q[i][st5sec:ed5sec, :].T
            else:
                eohm = np.vstack((eohm, eohm_total_Q[i][st5sec:ed5sec, :].T))
        if (iohm_total_Q[i]).shape[-1] != 0:
            if iohm.size == 0:
                iohm = iohm_total_Q[i][st5sec:ed5sec, :].T
            else:
                iohm = np.vstack((iohm, iohm_total_Q[i][st5sec:ed5sec, :].T))
        # if (eihm_total_S[i]).shape[-1] != 0:
        #    eihm.append(np.nanmean(eihm_total_Q[i], axis=1))
        if (rhm_total_Q[i]).shape[-1] != 0:
            if rhm.size == 0:
                rhm = rhm_total_Q[i][st5sec:ed5sec, :].T
            else:
                rhm = np.vstack((rhm, rhm_total_Q[i][st5sec:ed5sec, :].T))

    eohm = np.array(eohm)  # .T
    iohm = np.array(iohm)  # .T
    # eihm = np.array(eihm)#.T
    rhm = np.array(rhm)  # .T  # session x time
    eohm = orderHM(eohm, st_max, ed_max)
    iohm = orderHM(iohm, st_max, ed_max)
    rhm = orderHM(rhm, st_max, ed_max)

    combined = np.vstack((eohm, iohm, rhm))
    line_ei = eohm.shape[0]
    line_ir = iohm.shape[0] + eohm.shape[0]

    # print(combined.shape)
    # print(iohm)
    h1 = ax[1].imshow(combined, vmin=vmin, vmax=vmax)  # ax[0].pcolormesh(combined, cmap='viridis')  # (time, unit)
    ax[1].axhline(y=line_ei, color='k')
    ax[1].axhline(y=line_ir, color='k')
    fig.colorbar(h1, ax=ax[1],shrink=0.5)

    idx = np.zeros(timepointnum)
    for i in range(timepointnum):
        idx[i] = (np.where(timevec[st5sec:ed5sec] <= timevecc[i])[0][-1])

    timevecc = np.floor(timevecc).astype(int)
    ax[0].set_xticks(idx.astype(int))
    ax[1].set_xticks(idx.astype(int))
    ax[1].set_xticklabels((timevecc))
    ax[0].set_xticklabels((timevecc))
    # print(timevec.shape)
    ax[0].set_title('Sucrose')
    ax[1].set_title('Quinine')
    ax[1].set_xlabel('Time (s)')
    ax[0].set_xlabel('Time (s)')
    plt.savefig('SucroseQuinine_Heatmap_combined.svg')
    plt.show()
    plt.close()


def SQ_separateUnits(params, savedatadir):
    # define excited, inhibited and no change units
    # also remove trials and units that have firing rate less than 1Hz - turn them into nan

    fr_thr = 1

    # p_val_cutoff = 0.01

    subdirsave = "Heatmaps_SQ_AllUnits"
    if not os.path.exists(savedatadir + "\\" + subdirsave):
        os.mkdir(savedatadir + "\\" + subdirsave)

    neural_S = params['beh_analysis']['psth']['SucroseAnalysed_FR']
    neural_Q = params['beh_analysis']['psth']['QuinineAnalysed_FR']
    baselinetime_s = [-5, -3]  # [-9.5, -2]
    timevec = params['beh_analysis']['psth_Hz']['params']['edges'][1:]

    '''
    # checking for 0 firing and removing both trial and unit
    for i in range(neural_S.shape[-1]): # per unit
        c = 0
        c1 = 0
        if i-units_discarded >= neural_S.shape[-1]:
            break
        for j in range(neural_S.shape[0]): #per trial
            if j-c<=neural_S.shape[0]:
                mean_firing = np.mean(neural_S[j-c, :, i-units_discarded]) / params['beh_analysis']['psth_Hz']['params']['bin_size_sec']
                if mean_firing < fr_thr or mean_firing != mean_firing or np.isnan(mean_firing):  # less than firing threshold or is nan
                    #neural_S[j, :, i-units_discarded] = np.nan
                    neural_S = np.delete(neural_S, j-c, axis=0)
                    c+=1

        for j in range(neural_Q.shape[0]): #per trial
            if j - c1 < neural_Q.shape[0]:
                mean_firing = np.mean(neural_Q[j-c1, :, i-units_discarded]) / params['beh_analysis']['psth_Hz']['params']['bin_size_sec']
                if mean_firing < fr_thr or mean_firing != mean_firing or np.isnan(mean_firing):  # less than firing threshold or is nan
                    #neural_Q[j, :, i-units_discarded] = np.nan
                    neural_Q = np.delete(neural_Q, j - c1, axis=0)
                    c1+=1

        if c == neural_S.shape[0] or c1 == neural_Q.shape[0]:
            # delete this unit
            print(neural_S)
            neural_S = np.delete(neural_S, obj = i-units_discarded, axis=-1) # remove i along unit axis
            print(neural_S)
            neural_Q = np.delete(neural_Q, obj = i-units_discarded, axis=-1) # remove i along unit axis
            units_discarded += 1
        '''

    # CHECK FOR ZERO FIRING AND REMOVE THAT PARTICULAR UNIT:

    lick0times = params['behavior']['lick_ch0']['ts']
    lick1times = params['behavior']['lick_ch1']['ts']
    units_discarded = 0
    diff_Slick = np.diff(lick0times)
    diff_Qlick = np.diff(lick1times)
    difftime = 10

    for i in range(neural_S.shape[-1]):  # per unit
        c = 0
        c1 = 0
        if i - units_discarded >= neural_S.shape[-1]:
            break
        for j in range(neural_S.shape[0]):  # per trial
            mean_firing = np.mean(neural_S[j, :, i - units_discarded]) / params['beh_analysis']['psth_Hz']['params']['bin_size_sec']
            baseline_firing = np.mean(neural_S[j, 100:150, i - units_discarded])/ params['beh_analysis']['psth_Hz']['params']['bin_size_sec']
            #std_ = np.nanstd(baseline_firing)
            if mean_firing < fr_thr or mean_firing != mean_firing or np.isnan(mean_firing) or baseline_firing<0.5: # less than firing threshold or is nan or has constant firing (std across time = 0)
                neural_S[j, :, i - units_discarded] = np.nan
                c += 1

        for j in range(neural_Q.shape[0]):  # per trial
            mean_firing = np.mean(neural_Q[j, :, i - units_discarded]) / params['beh_analysis']['psth_Hz']['params']['bin_size_sec']
            baseline_firing = np.mean(neural_Q[j, 100:150, i - units_discarded])/params['beh_analysis']['psth_Hz']['params']['bin_size_sec']
            #std_ = np.nanstd(baseline_firing)
            if mean_firing < fr_thr or mean_firing != mean_firing or np.isnan(mean_firing) or baseline_firing < 0.5: # less than firing threshold or is nan
                neural_Q[j, :, i - units_discarded] = np.nan
                c1 += 1

        if c == neural_S.shape[0] or c1 == neural_Q.shape[0]:
            # delete this unit
            #print(neural_S)
            neural_S = np.delete(neural_S, obj=i - units_discarded, axis=-1)  # remove i along unit axis
            #print(neural_S)
            neural_Q = np.delete(neural_Q, obj=i - units_discarded, axis=-1)  # remove i along unit axis
            units_discarded += 1

    trialsdiscardedS = 0
    trialsdiscardedQ = 0
    # DELETE TRIALS THAT ARE LESS THAN 5 SECS APART
    for i in range(1, neural_S.shape[0]):
        if diff_Slick[i - 1] < 5:
            neural_S = np.delete(neural_S, obj=i - trialsdiscardedS, axis=0)
            trialsdiscardedS += 1

    for i in range(1, neural_Q.shape[0]):
        if diff_Qlick[i - 1] < 5:
            neural_Q = np.delete(neural_Q, obj=i - trialsdiscardedQ, axis=0)
            trialsdiscardedQ += 1

    print(units_discarded)
    # DELETE UNITS THAT HAVE STD_ == 0 AVG'ED ACROSS TRIALS

    timevecx = np.round(timevec, decimals=2)
    st = int(np.where(timevecx <= baselinetime_s[0])[0][-1])
    ed = int(np.where(timevecx >= baselinetime_s[1])[0][0]) + 1
    avg_neur = np.nanmean(neural_S, axis=0)  # (time, units)
    baseline = avg_neur[st:ed, :]  # baseline firing rate across all units
    mean_ = np.nanmean(baseline, axis=0)  # across time
    std_ = np.nanstd(baseline, axis=0)  # across time
    check0 = np.where(std_ == 0)[0]
    #print(check0)
    ccc=0
    if check0.size != 0:
        #print('bros its happening')
        for i in range(len(check0)):
            if i-ccc>=len(check0):
                break

            neural_S = np.delete(neural_S, check0[i]-ccc, axis=0)
            neural_Q = np.delete(neural_Q, check0[i]-ccc, axis=0)
            #print('bros its happening')
            ccc+=1

    avg_neur = np.nanmean(neural_Q, axis=0)  # (time, units)
    baseline = avg_neur[st:ed, :]  # baseline firing rate across all units
    mean_ = np.nanmean(baseline, axis=0)  # across time
    std_ = np.nanstd(baseline, axis=0)  # across time
    check0 = np.where(std_ == 0)[0]
    ccc=0
    print(check0)
    if check0.size != 0:
        for i in range(len(check0)):
            if i-ccc>=len(check0):
                break
            print(check0[i])
            print(neural_S[check0[i], :, :])
            print(neural_S[check0[i]+1, :, :])
            print(check0[i]-ccc)
            neural_S = np.delete(neural_S, check0[i]-ccc, axis=-1)
            neural_Q = np.delete(neural_Q, check0[i]-ccc, axis=-1)
            print(neural_S[check0[i], :, :])
            print('bros its happening')
            ccc+=1
    '''
    cc=0
    for i in range(neural_S.shape[-1]):
        if i-cc>=neural_S.shape[-1]:
            break
        baselineS = np.nanmean(neural_S[:,100:150,i-cc],axis=-1) # across time
        std_ = np.nanstd(baselineS)
        baselineQ = np.nanmean(neural_Q[:, 100:150, i-cc], axis=-1)  # across time
        std_Q = np.nanstd(baselineQ)
        if std_ == 0 or std_Q == 0:
            neural_S = np.delete(neural_S, i-cc, axis = -1)
            units_discarded += 1
            cc+=1
    print(units_discarded)
    '''
    # sanity check for nans
    # neuralS_avg_sanitycheck = np.nanmean(neural_S, axis=0)
    # plt.imshow(neuralS_avg_sanitycheck)
    # plt.show()
    # neuralQ_avg_sanitycheck = np.nanmean(neural_Q, axis=0)
    # plt.imshow(neuralQ_avg_sanitycheck)
    # plt.show()

    params['beh_analysis']['psth_Hz']['units_discarded'] = units_discarded
    heat_map_S, st, ed = calcAvgZscore(neural_S, timevec, baselinetime_s)
    heat_map_Q, st, ed = calcAvgZscore(neural_Q, timevec, baselinetime_s)

    fig, ax = plt.subplots(1, 2, figsize=(15, 5))
    ax.flatten()
    pcm1 = ax[0].pcolormesh(heat_map_S.T, cmap='viridis')
    ax[0].set_title('Sucrose')
    fig.colorbar(pcm1, ax=ax[0])
    pcm2 = ax[1].pcolormesh(heat_map_Q.T, cmap='viridis')
    ax[1].set_title('Quinine')
    fig.colorbar(pcm2, ax=ax[1])
    plt.savefig(
        savedatadir + "\\" + subdirsave + "\\" + params['mouse']['name'] + "_" + params['session']['date'] + ".png")
    # 3plt.show()
    plt.close()
    t = int(np.floor(heat_map_S.shape[0] / 2))
    # print(t)
    # heat_map_max = np.max(heat_map_S[t:,:], axis=0)
    # heat_map_min = np.min(heat_map_S[t:,:], axis=0)

    # ehm = heat_map_max > 2
    # ihm = heat_map_min < -2
    # baseline_S = heat_map_S[st:ed,:]
    # baseline_Q = heat_map_Q[st:ed,:]

    # testres  = stats.wilcoxon(baseline_S, heat_map_S[t:,:])
    # if testres[1]<p_val_cutoff:
    #    ehm = 1

    heat_map_count_S = 0
    heat_map_count_Q = 0

    ehm = np.zeros(heat_map_S.shape[-1])
    ihm = np.zeros(heat_map_S.shape[-1])
    rhm = np.zeros(heat_map_S.shape[-1])
    idx_S = [None]*3
    for i in range(3):
        idx_S[i] = []
    # hmm_exc = np.where(heat_map_S[t:,:]>1.96)
    # hmm_inh = np.where(heat_map_S[t:,:]<-1.65) # asymmetric double sided z score
    thr = 10 # 200 ms
    for i in range(ehm.size):
        hmm_exc = np.array(np.where(heat_map_S[t:t + 60, i] > 1.96)).squeeze()
        # print(hmm_exc)
        hmm_inh = np.array(np.where(heat_map_S[t:t + 60, i] < -1.65)).squeeze()  # asymmetric double sided z score
        # print(hmm_exc.size)
        if hmm_exc.size >= thr:
            ehm[i] = 1
            idx_S[0].append(i)
        if hmm_inh.size >= thr:
            ihm[i] = 1
            idx_S[1].append(i)
        if hmm_exc.size < thr and hmm_inh.size < thr:
            rhm[i] = 1
            idx_S[2].append(i)

    ehm = ehm.astype(bool)
    ihm = ihm.astype(bool)
    rhm = rhm.astype(bool)
    # print(ehm)
    # print(ihm)
    # eohm = np.logical_and(~ihm, ehm) # units segregated
    # iohm = np.logical_and(ihm, ~ehm)
    # eihm = np.logical_and(ihm, ehm)
    # rhm = np.logical_and(~ihm, ~ehm)
    params['beh_analysis']['psth']['eohm_idx_S'] = ehm
    params['beh_analysis']['psth']['iohm_idx_S'] = ihm
    # params['beh_analysis']['psth']['eihm_idx_S'] = eihm
    params['beh_analysis']['psth']['rhm_idx_S'] = rhm

    params['beh_analysis']['psth']['eohm_S_zscore'] = heat_map_S[:, ehm]
    params['beh_analysis']['psth']['iohm_S_zscore'] = heat_map_S[:, ihm]
    # params['beh_analysis']['psth']['eihm_S_zscore'] = heat_map_S[:, eihm]
    params['beh_analysis']['psth']['rhm_S_zscore'] = heat_map_S[:, rhm]
    params['beh_analysis']['psth']['SucroseAnalysed_FR'] = neural_S

    # Quinine

    # heat_map_max = np.max(heat_map_Q, axis=0)
    # heat_map_min = np.min(heat_map_Q, axis=0)

    # ehm = heat_map_max > 2
    # ihm = heat_map_min < -2

    ehm = np.zeros(heat_map_Q.shape[-1])
    ihm = np.zeros(heat_map_Q.shape[-1])
    rhm = np.zeros(heat_map_Q.shape[-1])
    idx_Q = [None] * 3
    for i in range(3):
        idx_Q[i] = []

    # hmm_exc = np.where(heat_map_Q[t:, :] > 1.96)
    # hmm_inh = np.where(heat_map_Q[t:, :] < -1.65)  # asymmetric double sided z score

    for i in range(ehm.size):
        hmm_exc = np.array(np.where(heat_map_Q[t:t+60, i] > 1.96)).squeeze()
        # print(hmm_exc)
        hmm_inh = np.array(np.where(heat_map_Q[t:t+60, i] < -1.65)).squeeze()  # asymmetric double sided z score
        if hmm_exc.size >= thr:
            ehm[i] = 1
            idx_Q[0].append(i)
        if hmm_inh.size >= thr:
            ihm[i] = 1
            idx_Q[1].append(i)
        if hmm_exc.size < thr and hmm_inh.size < thr:
            rhm[i] = 1
            idx_Q[2].append(i)

    ehm = ehm.astype(bool)
    ihm = ihm.astype(bool)
    rhm = rhm.astype(bool)
    # eohm = np.logical_and(~ihm, ehm)  # units segregated
    # iohm = np.logical_and(ihm, ~ehm)
    # eihm = np.logical_and(ihm, ehm)
    # rhm = np.logical_and(~ihm, ~ehm)
    params['beh_analysis']['psth']['eohm_idx_Q'] = ehm
    params['beh_analysis']['psth']['iohm_idx_Q'] = ihm
    # params['beh_analysis']['psth']['eihm_idx_Q'] = eihm
    params['beh_analysis']['psth']['rhm_idx_Q'] = rhm

    params['beh_analysis']['psth']['eohm_Q_zscore'] = heat_map_Q[:, ehm]
    params['beh_analysis']['psth']['iohm_Q_zscore'] = heat_map_Q[:, ihm]
    # params['beh_analysis']['psth']['eihm_Q_zscore'] = heat_map_Q[:, eihm]
    params['beh_analysis']['psth']['rhm_Q_zscore'] = heat_map_Q[:, rhm]
    params['beh_analysis']['psth']['QuinineAnalysed_FR'] = neural_Q

    params = SQ_SeparatedUnitsPlot(params, savedatadir + "\\" + subdirsave)
    # save 3D data per mouse (trials, time, units) and idx that's eohm, iohm and rhm for both S and Q
    np.savez(
        savedatadir + "\\" + subdirsave + "\\" + params['mouse']['name'] + "_" + params['session']['date'] + ".npz",
        neural_S, neural_Q, np.array(idx_S), np.array(idx_Q))

    return params


def unitcountSQ(params):
    ehmQ = params['beh_analysis']['psth']['eohm_idx_Q']
    ihmQ = params['beh_analysis']['psth']['iohm_idx_Q']
    rhmQ = params['beh_analysis']['psth']['rhm_idx_Q']
    ehmS = params['beh_analysis']['psth']['eohm_idx_S']
    ihmS = params['beh_analysis']['psth']['iohm_idx_S']
    rhmS = params['beh_analysis']['psth']['rhm_idx_S']
    matrixcount = np.zeros((3, 3))

    matrixcount[0, 0] = np.sum(np.logical_and(ehmS, rhmQ) * 1)  # excS only
    matrixcount[0, 1] = np.sum(np.logical_and(ihmS, rhmQ) * 1)  # inhS only
    matrixcount[0, 2] = np.sum(np.logical_and(rhmS, ehmQ) * 1)  # excQ only
    matrixcount[1, 0] = np.sum(np.logical_and(ihmQ, rhmS) * 1)  # inhQ only
    matrixcount[1, 1] = np.sum(np.logical_and(ehmS, ehmS) * 1)  # exc both
    matrixcount[1, 2] = np.sum(np.logical_and(ihmS, ihmQ) * 1)  # inh both
    matrixcount[2, 0] = np.sum(np.logical_and(ehmS, ihmQ) * 1)  # excS - inhQ
    matrixcount[2, 1] = np.sum(np.logical_and(ihmS, ehmQ) * 1)  # inhS - excQ
    matrixcount[-1, -1] = np.sum(np.logical_and(rhmS, rhmQ) * 1)  # no change

    return matrixcount


if __name__ == '__main__':

    analysis_type = "2_In_Nphy"  # "1_Silicone_Probes" #
    # Script to analyse on pickle and build heatmaps of spiking activity around  entries and  exits of open arms
    all_PETH_pickle_path = Path('Allmice_Separatedunits_SQ.pkl')
    thresholdtime = 10  # sec
    savedatadir = "PSTHdata"
    if not os.path.exists(savedatadir):
        os.mkdir(savedatadir)

    if not False:#all_PETH_pickle_path.exists():

        from pathlib import Path

        show_all_plots = False
        show_individual_population_plots = False

        #data_folder = Path(os.path.join(r'Y:\Ephys_in_vivo\02_ANALYSIS', analysis_type))
        data_folder = Path(r'Y:\Ephys_in_vivo\02_ANALYSIS')
        data_list = data_folder.rglob('*.pickle')  # lists all the pickle files

        # all_PETH = {}
        # all_putative_type = [0]
        # all_unit_response_type = {'enterzone': [0], 'exitzone': [0]}
        '''
        for data_path  in data_list:
            print(data_path.name)

        '''
        eohm_total_S = []
        iohm_total_S = []
        # eihm_total_S = []
        rhm_total_S = []
        eohm_total_Q = []
        iohm_total_Q = []
        # eihm_total_Q= []
        rhm_total_Q = []
        timevec = 0
        unitsdiscardedSQ = []
        unitorder = []
        counts_SQ = np.zeros((3, 3))
        mouse_consider_sq = ["F23", "M30", "M31", "F2492", "F2493", "F2495", "F2496", "M2497", "M2498", "M2499", "M2502"]
        mouse_swaplickarr = ["F2492", "F2493", "F2495", "F2496", "M2497", "M2498", "M2499", "M2502"] #["M15","M2497", "F2495", "F2496", "M2498", "M2499", "M2500", "M2501", "M2502"]

        for data_path in data_list:
            n = data_path.name  # each pickle file

            if 'SQ_data_2' in n:
                print(n)
                # if 'M29_SQ' in n:
                params = open_onefile(data_path, show_all_plots=show_all_plots,
                                      show_individual_population_plots=show_individual_population_plots)
                params = fix_paths(params, analysis_type)  # new path names updated

                if params['mouse']['name'] in mouse_swaplickarr:
                    params = swap_licks(params)


                params = SQplotdata(params, savedatadir)
                if params == 0:
                    continue
                params = SQ_separateUnits(params, savedatadir)
                # eohm_total_S, iohm_total_S,rhm_total_S, eohm_total_Q,iohm_total_Q, rhm_total_Q = SQ_avgdata(params, eohm_total_S, iohm_total_S, rhm_total_S,eohm_total_Q, iohm_total_Q, rhm_total_Q)
                timevec = params['beh_analysis']['psth_Hz']['params']['edges']
                eohm_total_S.append(params['beh_analysis']['psth']['eohm_S_zscore'])
                iohm_total_S.append(params['beh_analysis']['psth']['iohm_S_zscore'])
                # eihm_total_S.append(params['beh_analysis']['psth']['eihm_S_zscore'])
                rhm_total_S.append(params['beh_analysis']['psth']['rhm_S_zscore'])
                eohm_total_Q.append(params['beh_analysis']['psth']['eohm_Q_zscore'])
                iohm_total_Q.append(params['beh_analysis']['psth']['iohm_Q_zscore'])
                # eihm_total_Q.append(params['beh_analysis']['psth']['eihm_Q_zscore'])
                rhm_total_Q.append(params['beh_analysis']['psth']['rhm_Q_zscore'])

                counts_SQ += unitcountSQ(params)

                unitsdiscardedSQ.append(params['beh_analysis']['psth_Hz']['units_discarded'])

            elif 'EPM' in n:
                print(n)
                '''
                params = open_onefile(data_path, show_all_plots=show_all_plots,
                                         show_individual_population_plots=show_individual_population_plots)

                #neural_dat = params['ephys']['units_framed']
                #print(params['beh_analysis']['bin_size_sec'])
                #pprint.pprint(neural_dat)
                #plt.plot(neural_dat[1]/0.5)
                #plt.show()
                #break

                if params['mouse']['name'] in ["F2491","F2492","F2493", "F2494", "F2495","F2496", "M2497","M2498","M2499", "M2500", "M2501", "M2502"]:
                    params = fix_paths(params, analysis_type) # new paths updated
                    params, binwidth = add_behaviour(params,savedatadir, params['mouse']['name'], params['session']['date'])  # data extraction

                    if params==0:
                        continue
                    else:
                        params = makeHeatMap(params, thresholdtime)
                        plotallunits(params)
                        avg_plotneur_OAentry, gaussfiltpca_OAentry, avg_plotneur_OAexit, gaussfiltpca_OAexit, avg_plotneur_CAentry, gaussfiltpca_CAentry, avg_plotneur_CAexit, gaussfiltpca_CAexit, avg_plotneur_headdips, gaussfiltpca_headdips = plotandcheckunittypes(params, binwidth, savedatadir, params['mouse']['name'], params['session']['date'])  # check unit type
                        # save PCA data :)

                        if not os.path.exists(savedatadir+"\\data"):
                            os.mkdir(savedatadir+"\\data")
                        filename_pkl = savedatadir+"\\data\\"+ params['mouse']['name'] + "_" + params['session']['date']+"_"+str(thresholdtime)+"secEventSeparationAndPCA.npz"
                        with open(filename_pkl, 'wb') as f:
                            pkl.dump([avg_plotneur_OAentry, gaussfiltpca_OAentry, avg_plotneur_OAexit, gaussfiltpca_OAexit, avg_plotneur_CAentry, gaussfiltpca_CAentry, avg_plotneur_CAexit, gaussfiltpca_CAexit, avg_plotneur_headdips, gaussfiltpca_headdips, timevec], f)

                #print('out')



                # code for calling a function to check unit type
                # code for calling a function to separate units
                # code for generating the plot

                '''
            else:
                continue
        with open('Allmice_Separatedunits_SQ.pkl', 'wb') as f:
            pickle.dump(
                [eohm_total_S, iohm_total_S, rhm_total_S, eohm_total_Q, iohm_total_Q, rhm_total_Q, timevec, counts_SQ,
                 unitsdiscardedSQ], f)
        #print(unitsdiscardedSQ)
        plt.plot(np.array(unitsdiscardedSQ))
        plt.savefig('unitsdiscardedSQ_persession.svg')
        plt.close()
        SQ_plotavgSeparatedUnits(eohm_total_S, iohm_total_S, rhm_total_S, eohm_total_Q, iohm_total_Q, rhm_total_Q,
                                 timevec, counts_SQ)

    else:
        with open('Allmice_Separatedunits_SQ.pkl', 'rb') as f:
            eohm_total_S, iohm_total_S, rhm_total_S, eohm_total_Q, iohm_total_Q, rhm_total_Q, timevec, counts_SQ, unitsdiscardedSQ = pickle.load(
                f)
        #print(unitsdiscardedSQ)
        plt.plot(np.array(unitsdiscardedSQ))
        plt.savefig('unitsdiscardedSQ_persession.svg')
        plt.close()
        SQ_plotavgSeparatedUnits(eohm_total_S, iohm_total_S, rhm_total_S, eohm_total_Q, iohm_total_Q,
                                 rhm_total_Q, timevec, counts_SQ)

