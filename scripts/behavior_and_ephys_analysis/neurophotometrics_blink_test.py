import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path



p = Path(r'S:\_Yoni\Neurophotometrics\rec_blink')

photo_frame, photo_time = np.genfromtxt(p / 'photometry_fluo_timestamps.csv', unpack=True)
beh_frame, beh_time = np.genfromtxt(p / 'photometry_behaviorcamera_timestamps.csv', unpack=True)
beh_value = np.genfromtxt(p / 'roi_activiy.csv', unpack=True, usecols =10)

FrameCounter,Timestamp,LedState,Stimulation,Output0,Output1,Input0,Input1,Region0R,Region1G =  np.genfromtxt(p / 'photometry_fluo_values.csv', delimiter=',', unpack=True, skip_header=1)


Region1G = Region1G - np.min(Region1G)
Region1G = Region1G / np.max(Region1G)

beh_value = beh_value - np.min(beh_value)
beh_value = beh_value / np.max(beh_value)

plt.figure()
plt.plot(photo_time, Region1G)
plt.plot(beh_time, beh_value)
plt.show()