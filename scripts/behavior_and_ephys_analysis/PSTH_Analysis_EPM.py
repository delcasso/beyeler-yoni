import pickle
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import pprint
import pickle as pkl
import os
from PSTH_analysis_Separated import open_onefile, fix_paths
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from scipy.ndimage import gaussian_filter1d
from scipy.stats import sem

def add_behaviour(params, savedatadir, mousename, date):
    params['dlc_data'] = {}

    if os.path.exists(params['paths']['cebra_mousedata']):
        with np.load(params['paths']['cebra_mousedata']) as f:
            params['dlc_data']['neural'] = (f['l_data']).T  # (time,neurons)
            print(params['dlc_data']['neural'].shape)
            params['dlc_data']['behMatrix3D'] = (f['l_beh']).T  # (time, (idx, x, y))
            plt.plot(params['dlc_data']['behMatrix3D'][:,0])
            plt.show()
            params['dlc_data']['behMatrix2D'] = (f['l_beh_lowdim']).T
            params['dlc_data']['checker_xopen'] = f['l_x_open']
            params['dlc_data']['checker_yopen'] = f['l_y_open']
    else:
        return 0

    return params


def selectTrials(params, idx_pre, idx_post, timethr):
    l_data = params['dlc_data']['neural']
    l_beh = params['dlc_data']['behMatrix3D']
    beh_arr = []
    neur_arr = []
    binwidth = round(1 / params['video']['fr'], 6)  # 50ms, arbitrary fixed
    thr = np.ceil(timethr/binwidth).astype(int)

    for i in range(l_data.shape[0]): # time
        if i == 0:
            continue
        if np.where(l_beh[i,0] == idx_post)[0].size !=0 and np.where(l_beh[i-1,0] == idx_pre)[0].size !=0:
            if i-thr-1 >=0 and i+thr < l_data.shape[0]:
                neur_arr.append(l_data[i-thr-1:i+thr,:])
                beh_arr.append(l_beh[i-thr-1:i+thr,:])

    return neur_arr, beh_arr

def separateBehavior(params, thresholdtime):
    # params will be ordered in this fashion:
    # params['dlc_data'][0] -> OA
    # params['dlc_data'][1] -> CA
    # params['dlc_data'][2] -> headDips
    # params['dlc_data'][0][0] -> entry (idx 4 to 0/1), params['dlc_data'][0][1] -> exit (idx 0/1 to 4)
    # params['dlc_data'][1][0] -> entry (idx 4 to 2/3), params['dlc_data'][1][1] -> exit (idx 2/3 to 4)

    # initialise dicts
    for i in range(3):
        params['dlc_data'][i] ={}
        if i != 2:
            params['dlc_data'][i][0] = {}
            params['dlc_data'][i][1] = {}

    # add behavior now
    # binwidth = round(1 / params['video']['fr'], 6)  # 50ms, arbitrary fixed
    idx_pre_arr = [4,4,[0,1]]
    idx_post_arr = [[0,1],[2,3],-1]
    for i in range(3):
        idx_pre = idx_pre_arr[i]
        idx_post = idx_post_arr[i]
        if i != 2:
            for j in range(2):
                neur_arr, beh_arr = selectTrials(params, idx_pre, idx_post, timethr=thresholdtime)
                params['dlc_data'][i][j][0] = neur_arr
                print(np.array(neur_arr).shape)
                params['dlc_data'][i][j][1] = beh_arr
                print(np.array(beh_arr)[:,:,0])
                idx_post = idx_pre_arr[i]
                idx_pre = idx_post_arr[i]
        else:
            neur_arr, beh_arr = selectTrials(params, idx_pre, idx_post, timethr=thresholdtime)
            params['dlc_data'][i][0] = neur_arr
            print(np.array(neur_arr).shape)
            params['dlc_data'][i][1] = beh_arr

    return params


def EPM_plot1type_allneur(neural, unitnums):

    neural_avg = gaussian_filter1d(np.nanmean(neural, axis=0), sigma=6)
    neural_stdd = gaussian_filter1d(sem(neural, axis=0, nan_policy='omit'), sigma=6)
    x = int(np.ceil(unitnums / 5))
    y = 5
    fig, ax = plt.subplots(x, y)
    ax = ax.flatten()
    ax = ax.flatten()
    for i in range(unitnums):
        ax[i].plot(neural_avg[:, i], c='k')
        ax[i].plot(neural_avg[:, i] - neural_stdd[:, i], c='grey')
        ax[i].plot(neural_avg[:, i] + neural_stdd[:, i], c='grey')
    plt.show()


def plotSeparateUnits(params):
    unitnums = params['dlc_data']['neural'].shape[-1]
    for i in range(3):
        if i != 2:
            for j in range(2):
                EPM_plot1type_allneur(params['dlc_data'][i][j][0], unitnums)
        else:
            EPM_plot1type_allneur(params['dlc_data'][i][0], unitnums)


if __name__ == '__main__':

    analysis_type = "2_In_Nphy"  # "1_Silicone_Probes" #
    # Script to analyse on pickle and build heatmaps of spiking activity around  entries and  exits of open arms
    all_PETH_pickle_path = Path('Allmice_Separatedunits_EPM.pkl')
    thresholdtime = 2  # sec
    savedatadir = "PSTHdata"
    if not os.path.exists(savedatadir):
        os.mkdir(savedatadir)

    if not all_PETH_pickle_path.exists():

        from pathlib import Path

        show_all_plots = False
        show_individual_population_plots = False

        #data_folder = Path(os.path.join(r'Y:\Ephys_in_vivo\02_ANALYSIS', analysis_type))
        data_folder = Path(r'Y:\Ephys_in_vivo\02_ANALYSIS')
        data_list = data_folder.rglob('*.pickle')  # lists all the pickle files


        for data_path in data_list:
            n = data_path.name  # each pickle file

            if 'EPM' in n:
                print(n)
                params = open_onefile(data_path, show_all_plots=show_all_plots,
                                         show_individual_population_plots=show_individual_population_plots) # read params
                params = fix_paths(params, analysis_type) # new paths updated
                params = add_behaviour(params,savedatadir, params['mouse']['name'], params['session']['date'])  # data extraction - cebramousedata
                if params==0:
                    continue
                params = separateBehavior(params, thresholdtime)
                plotSeparateUnits(params)

        # with open('Allmice_Separatedunits_EPM.pkl', 'wb') as f:
        #     #PICKLE DUMP
        #     pickle.dump([],f)

    else:
        with open('Allmice_Separatedunits_EPM.pkl', 'rb') as f:
            []=pickle.load(f)