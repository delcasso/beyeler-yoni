import os
from pathlib import Path
import dill
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pntools as pn
from pntools import sampled
import beyelerlab as ab

%matplotlib widget

# fdir = r'C:\Dropbox (MIT)\Photometry\PhotoM groupe 1'
# fdir = r'C:\Dropbox (MIT)\Photometry\Compulsive1G1'
# fdir = r'C:\Dropbox (MIT)\Photometry\BingeB1'
fdir = r'S:\_Claudia\PhD\EtOH photométrie\Manip_pIC photoM DID comp\Photometry Analysis\pIC_Compulsive_recordings\20220407-12_pIC_CompulsiveRecording_batch2\Compulsive2_2M_analysis\Group1_1M2M1F2F'
# fdir = r'C:\Dropbox (MIT)\Photometry\2M'

fdir_group = r'S:\_Claudia\PhD\EtOH photométrie\Manip_pIC photoM DID comp\Photometry Analysis\pIC_Binge_recordings\20220211_pIC_BingeRecording_batch1\GOOD_Recording2' # Folder to save this mouse's data

default_params = {
    'file_types'        : ('lick_times', 'isos', 'GCaMP'), # add RCaMP (or similar) at the end if you have that
    'use_names_from'    : 'GCaMP', # source file for using mouse names
    'col_selector'      : 'Mouse', # I manually renamed your isos file to be consistent with columns in the GCaMP file
    'mouse_col_offset'  : 11,
    'bout_th'           : 10, # s
    'target_sr'         : 30, # Hz
    'discard_initial_s' : 600, # seconds
    'detrend_lambda'    : 1e6,
    'f_bandpass'        : (0.2, 6),
}
mouse_id = 'Mouse21G'

d = ab.Dataset(fdir, **default_params)
p = d.params

_, ax = plt.subplots(2, 1, sharex=True, sharey=True)
ax[0].eventplot([x() for x in d.lick_times.values()], linelengths=0.8)
ax[0].set_ylabel('Mouse #')
ax[0].set_title('Raw licks')

for m_count, this_licktimes in enumerate(d.lick_times.values()):
    bout_classes = this_licktimes.bout_classes()
    for bout_class in np.unique(bout_classes):
        ax[1].eventplot(np.asarray(this_licktimes())[bout_classes == bout_class], color=f'C{bout_class-1}', lineoffsets=m_count, linelengths=0.8)
ax[1].set_xlabel('Time (s)')
ax[1].set_title('Bout analysis')

for this_ax in ax:
    this_ax.set_yticks((0, 1, 2, 3))
    this_ax.set_yticklabels([x.removeprefix('Mouse') for x in d.mouse_names])


## Make a save a raster plot with each bout on one line
bout_classes = np.array(d.lick_times[mouse_id].bout_classes())
lick_times = np.array(d.lick_times[mouse_id]())
f, ax = plt.subplots(1, 1)
ax.eventplot([lick_times[bout_classes==bout_count] for bout_count in range(d.lick_times[mouse_id].n_bouts(),0,-1)], linelengths=0.8)
ax.set_title(mouse_id)
ax.set_xlabel('Time(s)')
f.savefig(os.path.join(fdir, f'{mouse_id}_bout_raster.pdf'))

# number of bouts
d.lick_times[mouse_id].n_bouts()
print(len(np.unique(bout_classes))) #number of bouts
print(len(bout_classes)) # number of licks
bout_classes # actual array having bout class indices

m = d(mouse_id).GCaMP
m_ref = d(mouse_id).isos[m._t0 + p.discard_initial_s:].detrend_airPLS(p.detrend_lambda)
m_proc = m[m._t0 + p.discard_initial_s:].detrend_airPLS(p.detrend_lambda).regress(m_ref).bandpass(*p.f_bandpass)
m_trend = m[m._t0 + p.discard_initial_s:].get_trend_airPLS(p.detrend_lambda) # F for df/F

_, ax = plt.subplots(2, 2, sharex=True)

ax[0, 0].plot(*m(''))
ax[0, 0].plot(*m[m._t0 + p.discard_initial_s:](''))
ax[0, 0].set_title('Step 1: Discard initial time')

ax[0, 1].plot(*m[m._t0 + p.discard_initial_s:](''))
ax[0, 1].plot(*m[m._t0 + p.discard_initial_s:].get_trend_airPLS(p.detrend_lambda)(''))
ax[0, 1].set_title('Step 2: Compute trend')

ax[1, 0].plot(*m[m._t0 + p.discard_initial_s:].detrend_airPLS(p.detrend_lambda)(''))
ax[1, 0].plot(*m[m._t0 + p.discard_initial_s:].detrend_airPLS(p.detrend_lambda).regress(m_ref)(''))
ax[1, 0].set_title('Step 3: Regress isosbectic signal')

ax[1, 1].plot(*m[m._t0 + p.discard_initial_s:].detrend_airPLS(p.detrend_lambda).regress(m_ref)(''))
ax[1, 1].plot(*m[m._t0 + p.discard_initial_s:].detrend_airPLS(p.detrend_lambda).regress(m_ref).bandpass(*p.f_bandpass)(''))
ax[1, 1].set_title('Step 4: bandpass')

m_lickstart = d(mouse_id).lick_times
events = tuple(m_lickstart.bout_start())

discard_intervals = [(m._t0, m._t0+p.discard_initial_s), (6000, 7600)] #, (2050., 2075.), (3550., 3553.)] # add intervals in time that you wish to disregard for event related analysis
event_discard_sel = np.zeros(len(events), dtype=bool)
for ev_count, ev_time in enumerate(events):
   for dt_start, dt_end in discard_intervals:
       if dt_start <= ev_time <= dt_end:
           event_discard_sel[ev_count] = True
events = [ev for ev, discard in zip(events, event_discard_sel) if not discard]

event_signals = sampled.Siglets(m_proc, events, window=(-10., 10.)) # set event window here (including baseline)
baseline_signals = sampled.Siglets(m_proc, events, window=(-10., -8.)) # set baseline window here
baseline_mean = baseline_signals(np.mean, axis='time')
baseline_std = baseline_signals(np.std, axis='time')
event_signals_z = (event_signals() - baseline_mean)/baseline_std

_, ax = plt.subplots(3, 1)
ax[0].plot(event_signals.t, event_signals()) # after bandpass
ax[0].set_title('Event-adjacent signals after bandpass filtering')
ax[0].get_xaxis().set_visible(False)
ax[1].plot(event_signals.t, event_signals_z) # z-scored signals
ax[1].get_xaxis().set_visible(False)
ax[1].set_title('After z-scoring each trial')

mu = np.mean(event_signals_z, axis=1)
sem = np.std(event_signals_z, axis=1)/np.sqrt(event_signals.n)
ax[2].plot(event_signals.t, mu) # mean of z-scored signals
ax[2].fill_between(event_signals.t, mu-sem, mu+sem, alpha=0.3) # standard error of the mean
ax[2].set_title('After averaging')
ax[2].set_ylabel('z-score')
ax[2].set_xlabel('Time (s)')


## Plotting the photometry signal from the first and the last lick bouts
f, ax = plt.subplots(2, 1, sharex=True, sharey=True)
ax[0].plot(event_signals.t, event_signals_z[:, 0])
ax[0].set_title('First lick bout')
ax[1].plot(event_signals.t, event_signals_z[:, -1])
ax[1].set_title('Last lick bout')
ax[1].set_xlabel('Time (s)')
f.suptitle(mouse_id)

df = pd.DataFrame(np.asarray([event_signals_z[:, 0], event_signals_z[:, -1]]).T, index=event_signals.t, columns=['z_score_first_bout', 'z_score_last_bout'])
df.to_csv(os.path.join(fdir, f'{mouse_id}_zscore_firstlast.csv'))

# Save this mouse's z score in the correct group folder
sig = sampled.Data(mu, sr=event_signals.sr, t0=event_signals.t[0])
import dill
with open(os.path.join(fdir_group, f'{Path(fdir).stem}_{mouse_id}.pkl'), 'wb') as f:
    dill.dump(sig, f)


baseline_f = m_trend[events]
event_signals_dff = event_signals()/baseline_f # if you assume detrending is already an approximate df operation, then this can be considered df/f

_, ax = plt.subplots(3, 1)
ax[0].plot(event_signals.t, event_signals()) # after bandpass
ax[0].set_title('Event-adjacent signals after bandpass filtering')
ax[0].get_xaxis().set_visible(False)
ax[1].plot(event_signals.t, event_signals_dff) # z-scored signals
ax[1].get_xaxis().set_visible(False)
ax[1].set_title('Relative fluorescence (df/f) on each trial')

mu = np.mean(event_signals_dff, axis=1)
sem = np.std(event_signals_dff, axis=1)/np.sqrt(event_signals.n)
ax[2].plot(event_signals.t, mu) # mean of z-scored signals
ax[2].fill_between(event_signals.t, mu-sem, mu+sem, alpha=0.3) # standard error of the mean
ax[2].set_title('After averaging')
ax[2].set_ylabel('dF/F')
ax[2].set_xlabel('Time (s)')

mu_z = np.mean(event_signals_z, axis=1)
mu_dff = np.mean(event_signals_dff, axis=1)
df = pd.DataFrame(np.asarray([mu_z, mu_dff]).T, index=event_signals.t, columns=['z_score', 'dff'])
df.to_csv(os.path.join(fdir, f'{mouse_id}_zscore_dff.csv'))

# min / max analysis
# make sure to execute the previous block which computes the mean z score for the animal
min_max_lim = (0, +3) # window in s to search for min/max
t_vec = np.array(event_signals.t)
sel = np.logical_and(t_vec < min_max_lim[1], t_vec > min_max_lim[0])
mu_z_piece = mu_z[sel]
t_piece = t_vec[sel]
max_pos = np.argmax(mu_z_piece)
max_t = t_piece[max_pos]
max_val = mu_z_piece[max_pos]

min_pos = np.argmin(mu_z_piece)
min_t = t_piece[min_pos]
min_val = mu_z_piece[min_pos]

f, ax = plt.subplots(1, 1)
ax.plot(t_vec, mu_z)
ax.plot(t_piece, mu_z_piece)
ax.plot(max_t, max_val, 'o')
ax.plot(min_t, min_val, 'o')
ax.set_title(mouse_id)

print(f'min: ({min_t} s, {min_val})')
print(f'max: ({max_t} s, {max_val})')

f.savefig(os.path.join(fdir, f'{mouse_id}_min_max.pdf'))

# Plot group average
group_dir = r'S:\_Claudia\PhD\EtOH photométrie\Manip_pIC photoM DID comp\Photometry Analysis\pIC_Binge_recordings\Nouveau dossier'
all_files = pn.FileManager(group_dir).add('event_signals', '*.pkl')['event_signals']
all_sig = [dill.load(open(file, 'rb')) for file in all_files]
assert len(set([s.sr for s in all_sig])) == 1 # check if all data has the same sampling rate
assert len(set([s._t0 for s in all_sig])) == 1 # check if the event signal window was the same for all mice

sig_mat = np.array([s() for s in all_sig]).T
t_vec = all_sig[0].t
n_mice = sig_mat.shape[-1]

f, ax = plt.subplots(1, 1)

mu = np.mean(sig_mat, axis=1)
sem = np.std(sig_mat, axis=1)/np.sqrt(n_mice)
ax.plot(t_vec, mu) # mean of z-scored signals
ax.fill_between(t_vec, mu-sem, mu+sem, alpha=0.3) # standard error of the mean
ax.set_title('Group average')
ax.set_ylabel('z-score')
ax.set_xlabel('Time (s)')

df = pd.DataFrame(np.asarray([mu, sem]).T, index=t_vec, columns=['group_average_mean', 'group_average_sem'])
df.to_csv(os.path.join(group_dir, f'group_average.csv'))
f.savefig(os.path.join(group_dir, f'group_average.pdf'))