#  Peri-event analysis in the EPM_20230721

import pickle
from pathlib import Path

import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from scripts.behavior_and_ephys_analysis.SQ_analysis import get_licks
from scripts.behavior_and_ephys_analysis.zone_analysis import zone_change
from scripts.ephys_tools import get_one_ch_data, butter_bandpass_filter, creates_time_vector

TRACE_COLOR = (0.5, 0.5, 0.5)


def plot_rawtrace_withevents(params, unit_key, sfreq, before_sec, after_sec, ax=None):
    """
    This is a debug function to plot the raw trace (channels where the unit has the biggest waveform,
    all the spikes of the unit (gray asterisk), the event times,
    the psth time_windows and the selected spikes (the one within these time_periods)
    :param params: the main variable of the program that contains all the data
    :param unit_key: the key of the unit because info relative to units are stored in a dict
    :param sfreq: the sampling frequency of the ephys recording
    :param before_sec: the size of the window before the event
    :param after_sec: the size of the psth window after the event
    :param ax: one axes if you would like to include this plit in your own figure
    :return: void
    """

    t = params['ephys']['t']
    u = params['ephys']['units'][unit_key]
    wfs = u['wfs']['avg']
    amplitudes = np.max(wfs, axis=0) - np.min(wfs, axis=0)
    ch_amp_max = np.argmax(amplitudes)
    selected_ch = params['ephys']['probe_geometry'][0]['channels'][ch_amp_max]
    d = get_one_ch_data(filepath=params['paths']['ephys_dat'], total_ch_number=27,
                        ch_id=selected_ch)
    d2 = butter_bandpass_filter(d, lowcut=600, highcut=6000, fs=30000, order=4)
    min_d2 = np.percentile(d2, 1)
    max_d2 = np.percentile(d2, 99)
    bar_size = (max_d2 - min_d2) * 2.5
    before_idx = before_sec * sfreq
    after_idx = after_sec * sfreq
    plt.ion()

    if ax is None:
        fig, ax = plt.subplots()

    rawdata_plot, = ax.plot(t, d2, color=TRACE_COLOR)
    rawdata_plot.set_label(f'ch {selected_ch} filtered [600-6000Hz]')
    spk_plot = ax.plot(t[u['idx']], np.repeat(min_d2 - (bar_size * 1.5), len(u['idx'])), '*', color=TRACE_COLOR)
    spk_plot[0].set_label(f'spikes from cluster {unit_key}')

    ax.legend()
    ax.set_ylabel('Analog signal (V)')
    ax.set_xlabel('Time (s)')

    behaviors = ('lick_ch0', 'lick_ch1')
    for beh in behaviors:
        lick_idx = params['behavior'][beh]['idx']
        for i, idx in enumerate(lick_idx):
            c = params['behavior'][beh]['display_color']
            ax.plot((t[idx], t[idx]), (min_d2 - bar_size, max_d2 + bar_size), color=c)
            ax.plot(t[idx], max_d2 + bar_size, 'v', color=c)
            ax.text(t[idx], max_d2 + bar_size * 1.1, f'trial #{i}', color=c, rotation=90)

            idx1 = idx - before_idx
            idx2 = idx + after_idx
            x = t[idx - before_idx]
            y = min_d2 - bar_size
            w = before_sec + after_sec
            h = max_d2 - min_d2 + (2 * bar_size)

            ax.add_patch(patches.Rectangle((x, y), w, h, facecolor=c, fill=True, alpha=0.2))
            spk_idx = np.where(np.logical_and(u['idx'] >= idx1, u['idx'] <= idx2))[0]
            spk_idx = u['idx'][spk_idx]

            for j in spk_idx:
                ax.plot(t[j], min_d2 - (bar_size * 1.25), '*', color=c)
                ax.plot(t[j - 45:j + 60], d2[j - 45:j + 60], color=c)

    plt.show()

    return


def build_psth(params, unit_key, event_idx, sfreq, before_sec, after_sec, bin_size_sec):
    u = params['ephys']['units'][unit_key]
    t = params['ephys']['t']

    before_idx = before_sec * sfreq
    after_idx = after_sec * sfreq

    edges = np.arange(-before_sec, after_sec, bin_size_sec)
    params['beh_analysis']['psth_params']['edges'] = edges
    psth_histo = np.empty((len(event_idx), len(edges) - 1), dtype=float)
    psth_histo.fill(np.nan)

    raster = {}

    for i, idx in enumerate(event_idx, start=0):
        idx1 = idx - before_idx
        idx2 = idx + after_idx
        spk_idx = np.where(np.logical_and(u['idx'] >= idx1, u['idx'] <= idx2))[0]
        spk_idx = u['idx'][spk_idx]
        spk_ts = t[spk_idx] - t[idx]
        hist, bin_edges = np.histogram(spk_ts, bins=edges)
        psth_histo[i, :] = hist
        raster[i] = spk_ts
        y = np.repeat(i, len(spk_ts))

    psth_histo = psth_histo / bin_size_sec

    return {'psth_Hz': psth_histo, 'raster': raster, 'psth_params': params['beh_analysis']['psth_params']}


def plot_raster(params, unit_key, all_raster, raster_name, marker_color=(0.5, 0, 0), ax=None):
    if ax is None:
        fig, ax = plt.subplots()
    ax.set_title(f'raster for cluster #{unit_key} and event #{raster_name}')
    raster = all_raster[unit_key]
    for trial_id, spk_ts in raster.items():
        y = np.repeat(trial_id, len(spk_ts))
        ax.plot(spk_ts, y, '|', color=marker_color)
    ax.plot((0, 0), (-1, len(raster) + 1), 'k')
    ax.set_ylabel('Trial #')
    ax.set_xlabel('Time (s)')
    plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_raster_{raster_name}_u{unit_key}.svg')
    plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_raster_{raster_name}_u{unit_key}.png')
    plt.close()


def plot_psth(params, unit_key, all_psth, psth_name, line_color=(0.5, 0, 0), ax=None, bar_mode=True):
    if ax is None:
        fig, ax = plt.subplots()

    ax.set_title(f'psth for cluster #{unit_key} and event #{psth_name}')
    psth = all_psth[unit_key]
    edges = params['beh_analysis']['psth_params']['edges']
    bin_size_sec = params['beh_analysis']['psth_params']['bin_size_sec']
    x = edges[:-1] + (bin_size_sec / 2)

    y_mean = np.mean(psth, axis=0)
    y_std = np.std(psth, axis=0)
    y_mean = y_mean / bin_size_sec
    y_std = y_std / bin_size_sec

    if bar_mode:
        ax.bar(x, y_mean, color=line_color)
        ax.set_ylabel('Frequency (Hz)')
        ax.set_xlabel('Time (s)')
    else:
        ax.plot(x, y_mean, color=line_color)
        ax.fill_between(x, y_mean - y_std, y_mean + y_std, facecolor=line_color, edgecolor=None, alpha=0.2)

    plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_psth_{psth_name}_u{unit_key}.svg')
    # plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_psth_{psth_name}_u{unit_key}.png')
    plt.close()


def find_rawdata(params, alternative_rawdata_folder):
    p = params['paths']['ephys_dat']
    if not p.exists():
        pattern_to_find = f'{params["mouse"]["name"]}/{params["session"]["date"]}'
        # pattern_position_in_old_path
        pos = p.as_posix().find(pattern_to_find)
        new_path = alternative_rawdata_folder / p.as_posix()[pos:]
        if new_path.exists():
            params['paths']['ephys_dat'] = new_path
    return params


def epm_psth_analysis(params, show_individual_plots=False, sampling_frequency=30000):
    p = params['paths']['ephys_dat']
    if not p.exists():
        print('p not exist, no alternative rawdata found')
        return params

    params['ephys']['t'] = creates_time_vector(filepath=p, total_ch_number=27)
    params['behavior'] = {}
    params['beh_analysis']['psth_Hz'] = {}
    params['beh_analysis']['raster'] = {}

    enterzone_color = [1, 0, 0]
    exitzone_color = [0, 0, 1]

    params['ephys']['sfreq'] = sampling_frequency
    params['beh_analysis']['psth_params'] = {'before_sec': 10, 'after_sec': 10, 'bin_size_sec': 0.5}

    params = zone_change(params)

    for z in params['apparatus']['zones']:
        params['beh_analysis']['psth_Hz'][z['name']] = {}
        params['beh_analysis']['psth_Hz'][z['name']]['enterzone'] = {}
        params['beh_analysis']['psth_Hz'][z['name']]['exitzone'] = {}
        params['beh_analysis']['raster'][z['name']] = {}
        params['beh_analysis']['raster'][z['name']]['enterzone'] = {}
        params['beh_analysis']['raster'][z['name']]['exitzone'] = {}

    for k, u in params['ephys']['units'].items():
        plt.ion()
        for z in params['apparatus']['zones']:
            params = psth_analysis(params, event_type='enterzone', event_color=enterzone_color,
                                   fig_name=f'{z["name"]}-IN', zone_name=z['name'], unit_key=k,
                                   show_individual_plots=show_individual_plots)
            params = psth_analysis(params, event_type='exitzone', event_color=exitzone_color,
                                   fig_name=f'{z["name"]}-OUT', zone_name=z['name'], unit_key=k,
                                   show_individual_plots=show_individual_plots)

    return params


def psth_analysis(params, event_type, event_color, fig_name, zone_name, unit_key, show_individual_plots=False):
    event_idx = params['videotrack'][f'{event_type}_ephysidx'][zone_name]
    event_ts = params['videotrack'][f'{event_type}_ephysts'][zone_name]
    event_dt = np.diff(event_ts)
    idx = np.where(event_dt <= 1)
    idx = np.array(idx) + 1
    event_idx = np.delete(event_idx, idx)
    tmp = build_psth(params=params, unit_key=unit_key, event_idx=event_idx, sfreq=params['ephys']['sfreq'],
                     before_sec=params['beh_analysis']['psth_params']['before_sec'],
                     after_sec=params['beh_analysis']['psth_params']['after_sec'],
                     bin_size_sec=params['beh_analysis']['psth_params']['bin_size_sec'])
    params['beh_analysis']['psth_Hz'][zone_name][event_type][unit_key] = tmp['psth_Hz']
    params['beh_analysis']['raster'][zone_name][event_type][unit_key] = tmp['raster']
    params['beh_analysis']['psth_params'] = tmp['psth_params']

    if show_individual_plots:
        plot_psth(params=params, unit_key=unit_key, all_psth=params['beh_analysis']['psth_Hz'][event_type],
                  psth_name=fig_name, line_color=event_color, bar_mode=False)
        plot_raster(params=params, unit_key=unit_key, all_raster=params['beh_analysis']['raster'][event_type],
                    raster_name=fig_name, marker_color=event_color)

    return params


def sq_psth_analysis(params, debug_mode=False):
    p = params['paths']['ephys_dat']
    params['ephys']['t'] = creates_time_vector(filepath=p, total_ch_number=27)
    params['behavior'] = {}
    params['behavior']['lick_ch0'] = get_licks(params=params, ch_str='ADC2', cleaning_th=2, debug_mode=False)
    params['behavior']['lick_ch1'] = get_licks(params=params, ch_str='ADC3', cleaning_th=2, debug_mode=False)
    params['behavior']['lick_ch0']['display_color'] = np.array([47, 134, 166]) / 255
    params['behavior']['lick_ch1']['display_color'] = np.array([52, 190, 130]) / 255

    TRACE_COLOR = (0.5, 0.5, 0.5)

    params['beh_analysis']['psth'] = {}
    params['beh_analysis']['psth']['lick_ch0'] = {}
    params['beh_analysis']['psth']['lick_ch1'] = {}
    params['beh_analysis']['raster'] = {}
    params['beh_analysis']['raster']['lick_ch0'] = {}
    params['beh_analysis']['raster']['lick_ch1'] = {}

    params['ephys']['sfreq'] = 30000
    params['beh_analysis']['psth']['params'] = {'before_sec': 10, 'after_sec': 10, 'bin_size_sec': 0.5}

    for k, u in params['ephys']['units'].items():

        if debug_mode:
            plot_rawtrace_withevents(params=params, unit_key=k, sfreq=params['ephys']['sfreq'],
                                     before_sec=params['beh_analysis']['psth']['params']['before_sec'],
                                     after_sec=params['beh_analysis']['psth']['params']['after_sec'],
                                     ax=None)

        event_idx = params['behavior']['lick_ch0']['idx']
        tmp = build_psth(params=params, unit_key=k, event_idx=event_idx, sfreq=params['ephys']['sfreq'],
                         before_sec=params['beh_analysis']['psth']['params']['before_sec'],
                         after_sec=params['beh_analysis']['psth']['params']['after_sec'],
                         bin_size_sec=params['beh_analysis']['psth']['params']['bin_size_sec'])
        params['beh_analysis']['psth']['lick_ch0'][k] = tmp['psth']
        params['beh_analysis']['raster']['lick_ch0'][k] = tmp['raster']
        event_idx = params['behavior']['lick_ch1']['idx']
        tmp = build_psth(params=params, unit_key=k, event_idx=event_idx, sfreq=params['ephys']['sfreq'],
                         before_sec=params['beh_analysis']['psth']['params']['before_sec'],
                         after_sec=params['beh_analysis']['psth']['params']['after_sec'],
                         bin_size_sec=params['beh_analysis']['psth']['params']['bin_size_sec'])
        params['beh_analysis']['psth']['lick_ch1'][k] = tmp['psth']
        params['beh_analysis']['raster']['lick_ch1'][k] = tmp['raster']
        all_psth = params['beh_analysis']['psth']['lick_ch0']
        all_raster = params['beh_analysis']['raster']['lick_ch0']
        plot_psth(params=params, unit_key=k, all_psth=all_psth, psth_name='quinine',
                  line_color=params['behavior']['lick_ch0']['display_color'])
        plot_raster(params=params, unit_key=k, all_raster=all_raster, raster_name='quinine',
                    marker_color=params['behavior']['lick_ch0']['display_color'])
        all_psth = params['beh_analysis']['psth']['lick_ch1']
        all_raster = params['beh_analysis']['raster']['lick_ch1']
        plot_psth(params=params, unit_key=k, all_psth=all_psth, psth_name='sucrose',
                  line_color=params['behavior']['lick_ch1']['display_color'])
        plot_raster(params=params, unit_key=k, all_raster=all_raster, raster_name='sucrose',
                    marker_color=params['behavior']['lick_ch1']['display_color'])

    return params


def psth_population_analysis(params, use_zscore=1, pickle_name=None, show_individual_population_plots=False,
                             peth_df=None, peth_matrix=None):

    if not 'psth_Hz' in params['beh_analysis'].keys():
        return params, peth_matrix, peth_df

    psth_db = params['beh_analysis']['psth_Hz']


    params['beh_analysis']['pop_PETH_Hz'] = {}

    for zone_name, data in psth_db.items():
        for event_type, data2 in data.items():
            for unit, psth in data2.items():
                baseline = psth[:, :9]
                mean_ = np.mean(baseline, axis=1)
                std_ = np.std(baseline, axis=1)
                zscore_ = (psth.T - mean_).T
                zscored_psth = (zscore_.T / std_).T
                mean_zscored_psth = np.squeeze(np.nanmean(zscored_psth, axis=0))

                if peth_matrix is None:
                    peth_matrix = mean_zscored_psth
                    peth_matrix_line_num = 0
                else:
                    peth_matrix = np.vstack((peth_matrix, mean_zscored_psth))
                    peth_matrix_line_num =peth_matrix.shape[0]-1


                mean_zscored_psth_max = np.max(mean_zscored_psth)
                mean_zscored_psth_min = np.min(mean_zscored_psth)
                ehm = mean_zscored_psth_max > 2
                ihm = mean_zscored_psth_min < -2

                response_type = 'unknown'

                if ~ihm and ehm:
                    response_type = 'excited only'
                if ihm and ~ehm:
                    response_type = 'inhibited only'
                if ihm and ehm:
                    response_type = 'excited & inhibited'
                if ~ihm and ~ehm:
                    response_type = 'not responsive'

                # # for unit_key, unit_values in params['ephys']['units'].items():
                # #     if (unit_values['convolved_fr_Hz'] > 15) and (unit_values['peak2valley_time_us'] < 450):
                # #         eunit[i] = 0

                if 'convolved_fr_Hz'  in params['ephys']['units'][unit].keys():
                    fr = params['ephys']['units'][unit]['convolved_fr_Hz']
                else:
                    fr = -1
                if 'peak2valley_time_us'  in params['ephys']['units'][unit].keys():
                    p2v = params['ephys']['units'][unit]['peak2valley_time_us']
                else:
                    p2v = -1
                data = {'unit_number': unit, 'FR': fr, 'peak2valley': p2v, 'response': response_type, 'zone': zone_name,
                        'direction': event_type}

                tmp_df = pd.DataFrame(data, index=[peth_matrix_line_num])
                peth_df = pd.concat([peth_df, tmp_df])


    return params, peth_matrix, peth_df


def plot_heatmap(hm, zone_name, hm_name, vmin=-8, vmax=6, i1=None, i2=None, pickle_name=None):
    if i1 is None:
        i1 = 0
    if i2 is None:
        i2 = hm.shape[1]

    auc = np.sum(hm[:, i1:i2], axis=1)
    idx = np.argsort(auc)
    hm = hm[idx, :]

    fig, (ax0, ax1) = plt.subplots(1, 2)
    fig.suptitle(zone_name + ' ' + hm_name)
    h0 = ax0.imshow(hm, vmin=vmin, vmax=vmax)
    plt.colorbar(h0)
    mu = np.mean(hm, axis=0)
    sigma = np.std(hm, axis=0)
    x = np.arange(hm.shape[1])
    ax1.plot(x, mu, color='grey')
    ax1.fill_between(x, mu + sigma, mu - sigma, facecolor='grey', alpha=0.1)
    plt.savefig(pickle_name.parent / f"{zone_name}-{hm_name}.svg")
    plt.close(fig)


def analyse_onefile(filename, show_all_plots=False, show_individual_population_plots=False, sampling_frequency=30000,
                    peth_df=None, peth_matrix=None, alternative_rawdata_folder=None):
    infile = open(filename, 'rb')
    params = pickle.load(infile)
    infile.close()

    params = find_rawdata(params, alternative_rawdata_folder=alternative_rawdata_folder)

    params = epm_psth_analysis(params=params, show_individual_plots=show_all_plots,
                               sampling_frequency=sampling_frequency)

    params, peth_matrix, peth_df = psth_population_analysis(params, use_zscore=1, pickle_name=filename,
                                      show_individual_population_plots=show_individual_population_plots,
                                      peth_df=peth_df, peth_matrix=peth_matrix)
    return params, peth_matrix, peth_df


if __name__ == '__main__':

    # Script to analyse on pickle and build heatmaps of spiking activity around  entries and  exits of open arms
    # filename = Path(r"Y:\Ephys in vivo\02 - ANALYZED DATA\F2493\20230414\F2493_EPM_data.pickle")
    # show_all_plots = False
    # show_individual_population_plots = False
    # params = analyse_onefile(filename, show_all_plots=show_all_plots, show_individual_population_plots=show_individual_population_plots)
    # plt.show()

    analysis_folder = Path(r'Y:\Ephys_in_vivo\02_ANALYSIS\2_In_Nphy')
    all_PETH_pickle_path = analysis_folder / r'_OUTPUT\all_PETH.pickle2'
    rawdata_folder = Path(r'Y:\Ephys_in_vivo\01_RAW_DATA\2_In_Nphy')

    peth_df = pd.DataFrame([], columns=['unit_number', 'FR', 'peak2valley', 'response', 'zone', 'direction'])
    peth_matrix = None
    peth_matrix.to_excel(r'Y:\Ephys_in_vivo\01_RAW_DATA\2_In_Nphy\df_2.xlsx')
    
    if not False:  # all_PETH_pickle_path.exists():

        from pathlib import Path

        show_all_plots = False
        show_individual_population_plots = False
        data_list = analysis_folder.rglob('*.pickle')

        all_PETH = {}
        all_putative_type = [0]
        all_unit_response_type = {'enterzone': [0], 'exitzone': [0]}

        prevunits = 0
        for data_path in data_list:
            n = data_path.name

            if 'EPM' in n:
                print(n)

                params, peth_matrix, peth_df = analyse_onefile(data_path, show_all_plots=show_all_plots,
                                         show_individual_population_plots=show_individual_population_plots,
                                         sampling_frequency=30000, peth_df=peth_df, peth_matrix=peth_matrix,
                                         alternative_rawdata_folder=rawdata_folder)

        with open(all_PETH_pickle_path, 'wb') as handle:
            pickle.dump(
                {'peth_matrix':peth_matrix, 'peth_df':peth_df},
                handle, protocol=pickle.HIGHEST_PROTOCOL)

    plt.show()
