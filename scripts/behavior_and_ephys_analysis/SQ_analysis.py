import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.patches as patches
import numpy as np


def clean_licks(time_vector, lick_idx, th_sec, debug_mode=False, debug_fig_title=''):
    """
    This function will clean TTL signals associated with both lickometers
    Basically it calculates the time between each TTL pulses if this time is inferior at th_sec,
    the following TTL will be removed

    :param time_vector: the time as recorded with raw ephys data
    :param lick_idx: the indices of the TTL-onset events linked to the licks
    :param th_sec: the minimum duration accepted bteween two licks
    :param debug_mode: if you want to print the debug figre
    :param debug_fig_title: the title of the debug figure
    :return:
    """
    lick_t = time_vector[lick_idx]
    lick_dt = np.diff(lick_t)
    idx = np.where(lick_dt <= 2)
    idx = np.array(idx) + 1
    lick_idx = np.delete(lick_idx, idx)
    new_lick_dt = np.diff(time_vector[lick_idx])

    if debug_mode:
        plt.ion()
        fig, axs = plt.subplots(1,2)
        fig.suptitle(debug_fig_title)
        axs[0].set_title('licks (dt)')
        axs[0].hist(lick_dt, bins=np.arange(0, 20, .05), density=True, facecolor='k', alpha=0.75)
        axs[1].set_title('cleaned licks (dt)')
        axs[1].hist(new_lick_dt, bins=np.arange(0, 20, .05), density=True, facecolor='k', alpha=0.75)
        plt.show()

    return lick_idx


def get_licks(params, ch_str, cleaning_th=2, debug_mode=False):
    """
    giving an adc channel ('lick channel')  a time vectore (time) and a clening th in sec,
    to clean the licks as described in the 'clean_licks' function
    :param lick_channel: 0 for ADC2 and 1 for ADC3
    :param time: the time as recorded with raw ephys data
    :param cleaning_th:the minimum duration accepted bteween two licks
    :return: a dict containing cleand indices and timestamps of the licks
    """
    licks_idx = []
    licks_idx = params['ephys'][f'{ch_str}_pulses']['onsets']
    t = params['ephys']['t']
    licks_idx = clean_licks(time_vector=t, lick_idx=licks_idx, th_sec=cleaning_th, debug_mode=debug_mode, debug_fig_title=f'lick from detector #{ch_str}')
    licks_t = t[licks_idx]

    return {'idx': licks_idx, 'ts': licks_t}