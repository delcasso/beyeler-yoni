import pickle
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.patches as patches
import matplotlib as mpl
import numpy as np
from scipy.ndimage import gaussian_filter

def heatmap_analysis(params, bin_size, show=False):
    plt.ion()
    edges = np.arange(0, 1300, bin_size)
    x_ = params['videotrack']['x']
    y_ = params['videotrack']['y']
    s_ = int(1300 / bin_size)

    # e = np.arange(0, 1300, 1)
    # H, xedges, yedges = np.histogram2d(x_, y_, bins=(e, e))
    #
    # L = np.reshape(H,(1,1299*1299))
    # plt.hist(L)

    clusters = params['ephys']['units_framed']
    cluster_num = len(clusters)

    size_1 = len(x_)
    size_2 = len(params['ephys']['units_framed'][0])

    min_size = min(size_1, size_2)

    params['beh_analysis']['occupancy_map'] = {}
    params['beh_analysis']['occupancy_map_sec'] = {}
    params['beh_analysis']['spikecount_map'] = {}
    params['beh_analysis']['activity_map'] = {}

    for k, v in clusters.items():

        spikecount_ = np.zeros((s_, s_))
        occupancy_ = np.zeros((s_, s_))

        for i in range(min_size):
            if not np.isnan(x_[i]) and not np.isnan(y_[i]):
                x__ = int(x_[i] / bin_size)
                y__ = int(y_[i] / bin_size)
                v__ = v[i]
                spikecount_[x__, y__] = spikecount_[x__, y__] + v__
                occupancy_[x__, y__] = occupancy_[x__, y__] + 1

        occupancy_sec = occupancy_ / params['video']['fr']
        params['beh_analysis']['occupancy_map'] = {'data': occupancy_, 'edges': edges, 'unit': "frames"}
        params['beh_analysis']['occupancy_map_sec'] = {'data': occupancy_sec, 'edges': edges, 'unit': "frames"}

        if k == 0:
            plot_map(params=params, data=params['beh_analysis']['occupancy_map_sec']['data'],
                     edges=params['beh_analysis']['occupancy_map']['edges'], colorbar_lb='Time (s)', unit_key=k,
                     map_name='occupancy_map_rawcb_filter', smart_colorbar=False, filter_data=True, sigma_=1)
            plot_map(params=params, data=params['beh_analysis']['occupancy_map_sec']['data'],
                     edges=params['beh_analysis']['occupancy_map']['edges'], colorbar_lb='Time (s)', unit_key=k,
                     map_name='occupancy_map_rawcb_nofilter', smart_colorbar=False, filter_data=False, sigma_=1)
            plot_map(params=params, data=params['beh_analysis']['occupancy_map_sec']['data'],
                     edges=params['beh_analysis']['occupancy_map']['edges'], colorbar_lb='Time (s)', unit_key=k,
                     map_name='occupancy_map_smartcb_nofilter', smart_colorbar=True, filter_data=False, sigma_=1)
            plot_map(params=params, data=params['beh_analysis']['occupancy_map_sec']['data'],
                     edges=params['beh_analysis']['occupancy_map']['edges'], colorbar_lb='Time (s)', unit_key=k,
                     map_name='occupancy_map_smartcb_filter', smart_colorbar=True, filter_data=True, sigma_=1)

        activity_ = spikecount_ / occupancy_sec

        params['beh_analysis']['spikecount_map'][k] = {'data': spikecount_, 'edges': edges, 'unit': "spikes"}
        params['beh_analysis']['activity_map'][k] = {'data': activity_, 'edges': edges, 'unit': "hertz"}
        plot_map(params=params, data=params['beh_analysis']['activity_map'][k]['data'],
                 edges=params['beh_analysis']['activity_map'][k]['edges'], colorbar_lb='Frequency (Hz)',
                 unit_key=k,  map_name='activity_map_rawcb_filter', smart_colorbar=False, filter_data=True,
                 sigma_=1)
        plot_map(params=params, data=params['beh_analysis']['activity_map'][k]['data'],
                 edges=params['beh_analysis']['activity_map'][k]['edges'], colorbar_lb='Frequency (Hz)',
                 unit_key=k,  map_name='activity_mapp_rawcb_nofilter', smart_colorbar=False, filter_data=False,
                 sigma_=1)
        plot_map(params=params, data=params['beh_analysis']['activity_map'][k]['data'],
                 edges=params['beh_analysis']['activity_map'][k]['edges'], colorbar_lb='Frequency (Hz)',
                 unit_key=k,  map_name='activity_map_smartcb_nofilter', smart_colorbar=True, filter_data=False,
                 sigma_=1)
        plot_map(params=params, data=params['beh_analysis']['activity_map'][k]['data'],
                 edges=params['beh_analysis']['activity_map'][k]['edges'], colorbar_lb='Frequency (Hz)',
                 unit_key=k,  map_name='activity_map_smartcb_filter', smart_colorbar=True, filter_data=True,
                 sigma_=1)
    return params


def plot_map(params, data, edges, colorbar_lb,  unit_key, map_name, mask_zero=True, smart_colorbar=False, colorbar_limits=None, filter_data=False, sigma_=1):

    cmap = cm.jet


    fig = plt.figure()
    plt.set_cmap(cmap)
    ax = fig.add_subplot(111, title='Heatmap of the firing activity')
    e = edges

    data0 = data
    if filter_data:
        data = np.nan_to_num(data)
        data = gaussian_filter(data, sigma=sigma_)
        data[np.isnan(data0)] = np.nan

    if mask_zero:
        data[data0==0]=np.nan
        current_cmap = mpl.cm.get_cmap()
        current_cmap.set_bad(color='white')

    map_ = plt.imshow(data.T, interpolation='nearest', origin='lower',
                      extent=[e[0], e[-1], e[0], e[-1]])

    c = fig.colorbar(map_, ax=ax)
    c.set_label(colorbar_lb, rotation=270)


    if smart_colorbar:
        l1 = np.nanpercentile(data, 5)
        l2 = np.nanpercentile(data, 95)
        map_.set_clim(l1, l2)

    if not colorbar_limits is None:
        map_.set_clim(colorbar_limits[0], colorbar_limits[1])

    plt.show()
    # plt.style.use('classic')
    plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_{map_name}_u{unit_key}.png')
    plt.savefig(params['paths']['analysis'] / f'{params["paths"]["prefix"]}_{map_name}_u{unit_key}.svg')
    plt.close()


if __name__ == '__main__':
    filename = r"Y:\Ephys in vivo\02 - ANALYZED DATA\F2496\20230414\F2496_EPM_data.pickle"
    infile = open(filename, 'rb')
    params = pickle.load(infile)
    infile.close()

    params = heatmap_analysis(params=params, bin_size=20)

    plt.show()

    print('here')
