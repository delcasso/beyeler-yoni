import cv2
from matplotlib.path import Path
import cv2 as cv
import numpy as np

def stats_in_zones(params,idx_start=None,idx_stop=None,period_name='full'):
    """
    Process statistics in each zone, time spent in the zone, distance travelled in the zone and spike number
    :param params: the main structure containing all the parameters
    :param idx_start: beginning of the time period to analyze (index)
    :param idx_stop: end of the time period to analyze (index)
    :param period_name: string that will tag the output data
    :return: the main structure containing all the parameters (params)
    """

    params['beh_analysis']["periodes"][period_name]={}
    for zone_name, in_zone in params['videotrack']['in_zone'].items():
        n_spikes = {}
        params['beh_analysis']["periodes"][period_name][zone_name] = {}
        if (idx_start is not None) and (idx_stop is not None):
            idx_stop = min([idx_stop,len(in_zone)])
            in_zone = in_zone[idx_start:idx_stop]
            tmp = params['videotrack']['dist'][idx_start:idx_stop]
            if params['exp_type'] == 'ephys':
                for k,v in params['ephys']['units_framed'].items():
                    tmp2 = v[idx_start:idx_stop]
                    tmp2 = tmp2[np.where(in_zone==True)]
                    n_spikes[k]=tmp2.sum()

        #we don't want to define a sub-period of the recording but have stats on the entire recording
        else:
            tmp = params['videotrack']['dist']
            if params['exp_type'] == 'ephys':
                n_spikes = {}
                for k,v in params['ephys']['units_framed'].items():
                    n_spikes[k]=v.sum()

        time_ = np.sum(in_zone) / params['video']['fr']
        travel_ = np.nansum(tmp[in_zone])
        params['beh_analysis']["periodes"][period_name][zone_name]['time'] = time_
        params['beh_analysis']["periodes"][period_name][zone_name]['travel'] = travel_
        if params['exp_type'] == 'ephys':
            for k, v in n_spikes.items():
                params['beh_analysis']["periodes"][period_name][zone_name][f'cluster{k}_nspikes'] = v
    return params



def in_zones(params):
    x = params['videotrack']['x']
    y = params['videotrack']['y']
    in_zone = {}
    for zone in params['apparatus']['zones']:
        coords = zone['coords']
        p = zone['path']
        pos = np.vstack((x, y)).T
        c_in = p.contains_points(pos)
        in_zone[zone['name']] = c_in

    task = params['session']['task']

    if task == 'OFT':
        in_zone['large_center'] = np.logical_and(in_zone['large_center'], np.logical_not(in_zone['small_center']))
        in_zone['external_ring'] = np.logical_and(np.logical_not(in_zone['small_center']), np.logical_not(in_zone['large_center']))
        in_zone['entire_apparatus'] = np.ones((len(x), ), dtype=bool)
    params['videotrack']['in_zone'] = in_zone
    return params


def behavioral_analysis(params):
    params = in_zones(params)
    bin_size_sec = params['beh_analysis']['bin_size_sec']
    bin_number = params['beh_analysis']['bin_number']
    frame_rate = params['video']['fr']
    idx_start = int(params['session']['startingtime'] * frame_rate)
    # Whole session (if you covert the entire session with bin_size_sec and bin_number)
    bin_size_idx = int(bin_size_sec * bin_number * frame_rate)
    params = stats_in_zones(params, idx_start=idx_start, idx_stop=idx_start+bin_size_idx, period_name=f'0-{bin_size_sec * bin_number}sec')
    # One period equal to bin_size_sec
    bin_size_idx = int(bin_size_sec * frame_rate)
    i1 = idx_start
    for i in range(bin_number):
        i2 = i1 + bin_size_idx
        params = stats_in_zones(params, idx_start=i1, idx_stop=i2,
                                period_name=f'{i*bin_size_sec}-{(i+1)*bin_size_sec}sec')
        i1=i2+1
    return params
