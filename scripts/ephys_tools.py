import numpy as np
import matplotlib.pyplot as plt
from tridesclous import DataIO
from typing import Tuple
from scipy.signal import butter, sosfilt


def get_one_ch_data(filepath, total_ch_number, ch_id):
    d = np.memmap(filepath, dtype=np.int16)
    d = np.reshape(d, (int(d.shape[0] / total_ch_number), total_ch_number))
    return d[:, ch_id]

def get_multiple_ch_data(filepath, total_ch_number, id_list):
    d = np.memmap(filepath, dtype=np.int16)
    d = np.reshape(d, (int(d.shape[0] / total_ch_number), total_ch_number))
    return d[:, id_list]

def plot_sig(d, i1, i2):
    fig, ax = plt.subplots(1, 1)
    plt.plot(d[i1:i2])
    plt.show()
    plt.ion()

def analog_2_events(signal):
    th = (np.max(signal[0:30000 * 60]) + np.min(signal[0:30000 * 60])) / 2
    signal = signal > th
    signal = signal.astype(int)
    diff_sig = np.diff(signal)
    onsets = np.where(diff_sig == 1)[0]
    offsets = np.where(diff_sig == -1)[0]
    return {'onsets': onsets, 'offsets': offsets}

def get_video_frames_onsets(params, total_ch_number=27, ch_id=20): #ch_id = 20 if ADC2
    d = get_one_ch_data(filepath=params['paths']['ephys_dat'], total_ch_number=total_ch_number, ch_id=ch_id)
    params['ephys'] = {}
    params['ephys']['ADC1_pulses'] = analog_2_events(d)
    params['video']['frame0_ephys_idx'] = params['ephys']['ADC1_pulses']['onsets'][0]
    params['videotrack']['frame0_ephys_idx'] = params['ephys']['ADC1_pulses']['onsets'][0]
    return params

def get_sq_licks_onsets(params, total_ch_number=27, ch_ids=[20, 21]):
    d_ = get_one_ch_data(filepath=params['paths']['ephys_dat'], total_ch_number=total_ch_number, ch_id=ch_ids[0])
    params['ephys']['ADC2_pulses'] = analog_2_events(d_)
    d_ = get_one_ch_data(filepath=params['paths']['ephys_dat'], total_ch_number=total_ch_number, ch_id=ch_ids[1])
    params['ephys']['ADC3_pulses'] = analog_2_events(d_)
    return params

def check_csv_separator(filepath):
    separators = [',',';']
    with open(filepath) as f:
        line = f.readline()
        for sep in separators:
            if sep in line:
                return sep
    return None

def load_spikes(params):
    separator = check_csv_separator(params['paths']['spikesorting'])

    if separator is None:
        print('Program failed to load spikes because csv separator is not defined')
    else:
        idx, cluster_id = np.genfromtxt(params['paths']['spikesorting'], dtype=int, skip_header=1, unpack=True,
                                        delimiter=separator)
        params['ephys']['units'] = {}
        dataio_ = DataIO(params['paths']['tdc'])
        for cid in set(cluster_id):
            params['ephys']['units'][cid] = {'idx': idx[cluster_id == cid], 'wfs': get_avg_wvf(dataio_, cid)}

    # params['ephys']['units'] = {cid: idx[cluster_id == cid] for cid in set(cluster_id)}

    return params


def synchronise_streams(params):
    units = params['ephys']['units']
    cam_pulses = params['ephys']['ADC1_pulses']['onsets']
    cam_pulses=np.hstack((cam_pulses,cam_pulses[-1]+np.mean(np.diff(cam_pulses))))
    params['ephys']['units_framed'] = {}
    for cid, timestamps in units.items():
        params['ephys']['units_framed'][cid] = np.histogram(timestamps['idx'], cam_pulses)[0]
    return params


def get_avg_wvf(data: DataIO, unit_id: int) -> Tuple[np.ndarray, np.ndarray,
                                                     np.ndarray, np.ndarray]:
    """
    Get the average and standard deviation waveform of a cell

    Parameters
    ----------

    Returns
    -------
    wvf: numpy ndarray
        Average waveform
    wvf_sd:numpy ndarray
        Standard deviation of the waveform

    """
    spikes_ix = data.get_spikes()
    n_bef = 45
    n_after = 60
    unit_spikes_ix = spikes_ix['index'][spikes_ix['cluster_label'] == unit_id]
    wvf = data.get_some_waveforms(seg_num=0, peak_sample_indexes=unit_spikes_ix,
                                  n_left=-n_bef, n_right=n_after)
    # best_ch = get_max_channel(data, unit_id)
    # b_wvf = wvf[..., best_ch]

    m = wvf[:, :5, :].mean(1)
    m2 = np.repeat(m, wvf.shape[1], 1)
    m3 = m2.reshape((wvf.shape[0], wvf.shape[2], wvf.shape[1])).transpose((0, 2, 1))
    wvf_c = wvf - m3
    wvf_avg = wvf_c.mean(0)
    wvf_sd = np.std(wvf_c, 0)
    rnd_ix = np.random.randint(0, wvf.shape[0] - 1, 100)
    wvf_ch = wvf_c[rnd_ix, ...]
    dt = 1000 / data.sample_rate
    t = np.arange(-n_bef * dt, n_after * dt, dt)
    wvf_dict = {'avg': wvf_avg, 'sd': wvf_sd, 'samples': wvf_ch, 'time': t}
    return wvf_dict


def get_probe_geometry(params):
    # channel_groups = {
    #     0: {
    #         "channels": [
    #             11,
    #             10,
    #             9,
    #             6,
    #             2,
    #             5,
    #             3,
    #             4,
    #             12,
    #             13,
    #             14,
    #             8,
    #             15,
    #             0,
    #             7,
    #             1
    #         ],
    #         "geometry": {
    #             11: [
    #                 0.0,
    #                 0.0
    #             ],
    #             10: [
    #                 0.0,
    #                 25.0
    #             ],
    #             9: [
    #                 0.0,
    #                 50.0
    #             ],
    #             6: [
    #                 0.0,
    #                 75.0
    #             ],
    #             2: [
    #                 0.0,
    #                 100.0
    #             ],
    #             5: [
    #                 0.0,
    #                 125.0
    #             ],
    #             3: [
    #                 0.0,
    #                 150.0
    #             ],
    #             4: [
    #                 0.0,
    #                 175.0
    #             ],
    #             12: [
    #                 22.5,
    #                 12.5
    #             ],
    #             13: [
    #                 22.5,
    #                 37.5
    #             ],
    #             14: [
    #                 22.5,
    #                 62.5
    #             ],
    #             8: [
    #                 22.5,
    #                 87.5
    #             ],
    #             15: [
    #                 22.5,
    #                 112.5
    #             ],
    #             0: [
    #                 22.5,
    #                 137.5
    #             ],
    #             7: [
    #                 22.5,
    #                 162.5
    #             ],
    #             1: [
    #                 22.5,
    #                 187.5
    #             ]
    #         }
    #     }
    # }

    channel_groups = {
        0: {
            "channels": [
                16,
                12,
                8,
                4,
                15,
                11,
                7,
                3,
                14,
                10,
                6,
                2,
                13,
                9,
                5,
                1
            ],
            "geometry": {
                16: [
                    0.0,
                    0.0
                ],
                12: [
                    0.0,
                    150.0
                ],
                8: [
                    0.0,
                    300.0
                ],
                4: [
                    0.0,
                    450.0
                ],
                15: [
                    150.0,
                    0.0
                ],
                11: [
                    150.0,
                    150.0
                ],
                7: [
                    150.0,
                    300.0
                ],
                3: [
                    150.0,
                    450.0
                ],
                14: [
                    300.0,
                    0.0
                ],
                10: [
                    300.0,
                    150.0
                ],
                6: [
                    300.0,
                    300.0
                ],
                2: [
                    300.0,
                    450.0
                ],
                13: [
                    450.0,
                    0.0
                ],
                9: [
                    450.0,
                    150.0
                ],
                5: [
                    450.0,
                    300.0
                ],
                1: [
                    450.0,
                    450.0
                ]
            }
        }
    }

    params['ephys']['probe_geometry'] = channel_groups

    return params

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = butter(order, [low, high], analog=False, btype='band', output='sos')
    return sos


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass(lowcut, highcut, fs, order=order)
    y = sosfilt(sos, data)
    return y


def creates_time_vector(filepath, total_ch_number=27):
    d = get_one_ch_data(filepath=filepath, total_ch_number=total_ch_number, ch_id=0)
    t = np.arange(d.shape[0]) / 30000
    return t

def measure_unit_properties(params):
    units = params['ephys']['units']

    for k, v in units.items():
        u = units[k]
        spk_times_sec = u['idx'] / 30000
        spk_diff_sec = np.diff(spk_times_sec)

        window = 20
        weights = np.repeat(1.0, window) / window
        rolling_avg = np.convolve(spk_diff_sec, weights, 'valid')
        fr = 1 / np.mean(rolling_avg)

        params['ephys']['units'][k]['convolved_fr_Hz'] = fr

        wfs = u['wfs']['avg']
        amplitudes = np.max(wfs,axis=0) - np.min(wfs,axis=0)
        ch_amp_max = np.argmax(amplitudes)
        wfs_samples = u['wfs']['samples'][:, :, ch_amp_max]
        min_ = np.argmin(wfs_samples, axis=1)
        max_ = np.argmax(wfs_samples, axis=1)
        peak2valley_time_us = np.mean(np.abs(min_-max_) * 1/30000 * 1000000)

        params['ephys']['units'][k]['peak2valley_time_us'] = peak2valley_time_us


        wfs_tmp = wfs[:,ch_amp_max]
        min_ = np.argmin(wfs_tmp)
        postpeak = wfs_tmp[min_:]
        idx_sup_zero = np.argwhere(postpeak >= 0)[0][0]
        # print(idx_sup_zero)
        hyperpol = postpeak[idx_sup_zero:]
        idx_inf_zero = np.argwhere(hyperpol < 0)[0][0]
        hyperpol = hyperpol[:idx_inf_zero]
        aup = np.sum(hyperpol)
        params['ephys']['units'][k]['aup'] = aup


    return params


if __name__ == '__main__':
    p = r'E:\EphysAnalysis\raw\M5\20210127\d5_2021-01-27_10-01-02_oft\Record Node 119\experiment1\recording1\continuous\Rhythm_FPGA-118.0\continuous.dat'
    d = get_one_ch_data(filepath=p, total_ch_number=24, ch_id=16) #watch out some recordings are only with 24 channels!
    plot_sig(d,30000*200,30000*300)
    #e = analog_2_events(d)
