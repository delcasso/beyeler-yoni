import matplotlib.pyplot as plt

ephys_path = r"Y:\Ephys_in_vivo\01_RAW_DATA\2_In_Nphy\D1-Ephys\Data\M3020\20240310\M3020_2024-03-10_12-06-21_EPM\Record Node 106\experiment1\recording1\continuous\Rhythm_FPGA-105.0\continuous.dat"

import numpy as np


def get_one_ch_data(filepath, total_ch_number, ch_id):
    d = np.memmap(filepath, dtype=np.int16)
    d = np.reshape(d, (int(d.shape[0] / total_ch_number), total_ch_number))
    return d[:, ch_id]








ch1 = get_one_ch_data(filepath=ephys_path, total_ch_number=27, ch_id=1)

t = np.load(r"Y:\Ephys_in_vivo\01_RAW_DATA\2_In_Nphy\D1-Ephys\Data\M3020\20240310\M3020_2024-03-10_12-06-21_EPM\Record Node 106\experiment1\recording1\continuous\Rhythm_FPGA-105.0\synchronized_timestamps.npy")



plt.figure()
plt.plot(ch1[0:30000*60:1000])
plt.show()