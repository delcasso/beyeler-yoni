import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from matplotlib import cm

path_ = r"C:\in-vivo-ephys\data\analysis\20221107_Plots_units_OFT.csv"
# [FR,P2V,AUP, souris, TMP] = np.genfromtxt(path_, delimiter=";", skip_header=1, unpack=True)
d = np.genfromtxt(path_, delimiter=";", skip_header=1)

n=2
col = ['r','g','b']

kmeans = KMeans(n_clusters=n, random_state=0).fit(d[:,0:2])

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

for i in range(n):
    j = np.where(kmeans.labels_ == i)
    print(f'categorie{i} n={j[0].shape} perc={j[0].shape[0]/d.shape[0]}')
    ax.scatter(d[j,2],d[j,0],d[j,1], c=col[i], marker='o')

plt.show()
plt.ion()