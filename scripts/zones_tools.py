import matplotlib.pyplot as plt
from pathlib import Path
import PIL
from roipoly import MultiRoi



class ZoneSelector:

    def __init__(self, apparatus, img0_path):
        self.apparatus = apparatus
        self.img0_pth = img0_path
        self.img0 = PIL.Image.open(self.img0_pth)

        self.fig = None
        self.ax = None
        self.ps = []

        oft_zones = {'names':['whole apparatus',]}
        epm_zones = {'names': ['Open Arm 1', 'Open Arm 2', 'Closed Arm 1', 'Closed Arm 2']}
        self.zones = {'OFT': oft_zones, 'EPM': epm_zones}

        #TODO: remove this is just for test
        self.apparatus = 'EPM'


        self.fig, self.ax = plt.subplots()
        # plt.axis('off')

        # Draw multiple ROIs
        plt.imshow(self.img0)
        multiroi_named = MultiRoi(roi_names=self.zones[self.apparatus]['names'])

        # Draw all ROIs
        plt.imshow(self.img0)

        roi_names = []
        for name, roi in multiroi_named.rois.items():
            roi.display_roi()
            # roi.display_mean(self.img0)
            roi_names.append(name)
        plt.legend(roi_names, bbox_to_anchor=(1.2, 1.05))
        plt.show()

        print('here')



if __name__ == '__main__':

    rawdata_path = Path(r'raw/M8/20210831')
    analysis_path = Path(r'analysis/M8/20210831')
    params = {}
    params['paths'] = {}
    params['paths']['rawdata'] = rawdata_path
    params['paths']['analysis'] = analysis_path

    # zc = ZoneSelector('OFT',r'C:\Users\sebas\Documents\01 AQUINEURO SD\Projets\beyeler-yoni\analysis\M8\20210831\M8_OFT_frame0.png')












