import numpy as np
import pandas as pd

dfA=pd.read_csv("../Time_Behavior.csv", sep=";")
dfB=pd.read_csv("Spike_Sorting_OFT.csv",sep=";")

#print(dfA.columns.tolist())
#print(dfB.columns.tolist())
print(dfA)

dfA["time"]=np.multiply(dfA["time"],1500)
newMat2=[np.arange(len(dfA["time"])*1500)]
newMat2.append(np.empty(len(dfA['time'])*1500))
newMat2[1][:]=np.nan

for i in range(len(dfA["behavior"])):
    newMat2[1][i*1500:(i+1)*1500]=dfA["behavior"][i]

newMat=[]
dfB["index"]=dfB["index"].astype(int)

for i in range(len(dfB["index"])):
    idx = dfB["index"][i]
    newMat.append( newMat2[1][idx])

dfB["behavior"]=newMat
dfB.to_csv("MergeDF.csv")

